package com.bodyfriend.shippingsystem;

import com.bodyfriend.shippingsystem.base.NetConst;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testReal(){
        assertTrue(!NetConst.host.equals(NetConst.HOST_REAL));

        assertTrue(!NetConst.isReal());
    }
}