package com.bodyfriend.shippingsystem.main.delivery_order.net;

import android.os.Parcel;
import android.os.Parcelable;

import com.bodyfriend.shippingsystem.base.BFEnty;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Taeseok on 2018-02-02.
 */

public class getProductList extends BFEnty {
    public getProductList() {
        setUrl("Common/Warehouse/productList.json");
        setParam("productTypeArr", new JSONArray().put("M").put("L").toString());
    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
        public List<resultData> resultData;

        public int total;

        public static class resultData implements Parcelable {
            public String productName;
            public String productCode;
            public String productType;
            public int count;

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.productName);
                dest.writeString(this.productCode);
                dest.writeString(this.productType);
                dest.writeInt(this.count);
            }

            public resultData() {
            }

            protected resultData(Parcel in) {
                this.productName = in.readString();
                this.productCode = in.readString();
                this.productType = in.readString();
                this.count = in.readInt();
            }

            public static final Parcelable.Creator<resultData> CREATOR = new Parcelable.Creator<resultData>() {
                @Override
                public resultData createFromParcel(Parcel source) {
                    return new resultData(source);
                }

                @Override
                public resultData[] newArray(int size) {
                    return new resultData[size];
                }
            };
        }
    }

}
