package com.bodyfriend.shippingsystem.main.delivery_order;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.util.PP;
import com.bodyfriend.shippingsystem.databinding.ActivityDeliveryAddBinding;
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.ProductAdapter;
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliveryResponseData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.ProductItemListData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseApi;
import com.bodyfriend.shippingsystem.main.login.Auth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class DeliveryAddActivity extends BFActivity implements TextWatcher {

    private ActivityDeliveryAddBinding mBinding;
    private ArrayList<ProductItemListData.list> mProductList;
    private ProductAdapter mProductAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_delivery_add);
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mBinding.toolbar.setNavigationOnClickListener(v -> onBackPressed());

        initializeUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.item_add) {
            if (!mProductAdapter.getItems().isEmpty()) {
                Intent intent = getIntent();
                JSONArray jsonArray = new JSONArray();
                for (ProductItemListData.list data : mProductAdapter.getItems()) {
                    JSONObject listData = new JSONObject();
                    try {
                        listData.put("clsCd", data.clsCd);
                        listData.put("tagItemCd", data.tagItemCd);
                        listData.put("clrtnCd", data.clrtnCd);
                        listData.put("partCd", data.partCd);
                        listData.put("mfNatCd", data.mfNatCd);
                        listData.put("gdsStatCd", data.gdsStatCd);
                        listData.put("gdsLvlCd", data.gdsLvlCd);
                        listData.put("qty", data.count);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    jsonArray.put(listData);
                }
                RequestBody repairhistory = RequestBody.create(MediaType.parse("json/plain"), jsonArray.toString());
                DisposableObserver<DeliveryResponseData> disposableObserver = ServiceGenerator.createService(WarehouseApi.class)
                        .addReserveInsData(
                                "O", "W",
                                getResources().getStringArray(R.array.warehouseCode)[mBinding.spinnerWarehouse.getSelectedItemPosition()]
                                , false, Auth.getId(), Auth.d.resultData.ADMIN_NM
                                , PP.carNumber.get(), "  ", repairhistory
                        )

                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<DeliveryResponseData>() {

                            @Override
                            public void onNext(DeliveryResponseData deliveryResponseData) {
                                Log.d("deliveryResponseData.success : " + deliveryResponseData.success);
                                if (deliveryResponseData.success) {
                                    setResult(RESULT_OK, intent);
                                    finish();
                                } else {
                                    if (!isFinishing())
                                        new AlertDialog.Builder(DeliveryAddActivity.this)
                                                .setMessage(deliveryResponseData.msg)
                                                .setTitle(deliveryResponseData.message)
                                                .show();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                            }

                            @Override
                            public void onComplete() {

                            }
                        });

            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product, menu);
        return true;
    }

    public void onAddProduct(View view) {

        if (!TextUtils.isEmpty(mBinding.productName.getText().toString())) {
            ProductItemListData.list selectedItem = null;
            if (mProductList != null)
                for (ProductItemListData.list product : mProductList) {
                    String[] split = mBinding.productName.getText().toString().split(" ");
                    Log.d("split : " + split.length);


//                    String colorOfProduct = "";
//                    if (split.length > 2) {
//                        colorOfProduct = mBinding.productName.getText().toString().substring(split[0].length() + 1, mBinding.productName.getText().toString().length());
//                    } else {
//                        try {
//                            colorOfProduct = split[1];
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    Log.d("split[0] : " + split[0] + "\t colorOfProduct : " + colorOfProduct);
//                    if (split[0].equals(product.itemNmKr) && colorOfProduct.equals(product.clrtnNm)) {
//                        selectedItem = product;
//                        break;
//                    }
                    if (split[split.length - 1].equals(product.itemId)) {
                        selectedItem = product;
                        break;
                    }
                }

            String countString = mBinding.productCount.getText().toString();
            Log.d("countString L : " + countString);
            if (selectedItem != null && !TextUtils.isEmpty(countString)) {
                mBinding.productCount.setText("");
                selectedItem.count = Integer.parseInt(countString);
                selectedItem.gdsStatCd = getResources().getStringArray(R.array.gdsStatCd)[mBinding.spinnerGdsStatCd.getSelectedItemPosition()];
                if (mBinding.spinnerGdsStatCd.getSelectedItemPosition() > 0) {
                    selectedItem.gdsLvlCd = getResources().getStringArray(R.array.gdsLvlCd)[mBinding.spinnerGdsLvlCd.getSelectedItemPosition()];
                } else {
                    selectedItem.gdsLvlCd = "N";
                }
                mBinding.spinnerGdsStatCd.setSelection(0);
                mProductAdapter.addItem(selectedItem);
                hideKeyboard(mBinding.productCount);

            } else {
                Toast.makeText(mContext, "수량을 입력하세요.", Toast.LENGTH_SHORT).show();
            }
        }

//        ProductItemListData.list list = mProductList.get(mBinding.spinnerProduct.getSelectedItemPosition());

    }

    private void initRecyclerView() {
        mBinding.itemList.setHasFixedSize(true);
        LinearLayoutManager mRecentLayoutManager = new LinearLayoutManager(this);
        mBinding.itemList.setLayoutManager(mRecentLayoutManager);
        mProductAdapter = new ProductAdapter();
        mBinding.itemList.setAdapter(mProductAdapter);
    }

    private void initializeUI() {
        initRecyclerView();
        setProductType();
        mBinding.productName.addTextChangedListener(this);
        mBinding.productName.setTextColor(Color.RED);
        mBinding.productName.setThreshold(1);
//        mBinding.spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String info = String.format("%s %s", mProductList.get(position).itemNmKr, mProductList.get(position).clrtnNm);
//                mBinding.productName.setText(info);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        mBinding.spinnerGdsStatCd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    mBinding.spinnerGdsLvlCd.setVisibility(View.VISIBLE);
                } else {
                    mBinding.spinnerGdsLvlCd.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.itemList.addOnItemTouchListener(new RecyclerItemClickListener(this, mBinding.itemList
                , new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }

            @Override
            public void onLongItemClick(View view, int position) {
                new AlertDialog.Builder(mContext).setMessage("선택항목 삭제 하시겠습니까?").setPositiveButton("확인", (dialog, which) -> {
                    dialog.cancel();
                    mProductAdapter.getItems().remove(position);
                    mProductAdapter.notifyDataSetChanged();
                }).setNegativeButton("아니오", (dialog, which) -> {
                    dialog.cancel();
                }).show();
            }
        }));
    }

    private void setProductType() {
        mBinding.spinnerProductType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getProductList();
                mBinding.productName.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getProductList() {
        String code = getResources().getStringArray(R.array.productTypeCode)[mBinding.spinnerProductType.getSelectedItemPosition()];
        DisposableObserver<ProductItemListData> disposableObserver = ServiceGenerator.createService(WarehouseApi.class)
                .getItemList(code, null, null)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ProductItemListData>() {


                    @Override
                    public void onNext(ProductItemListData productItemListData) {
                        if (productItemListData != null) {

                            mProductList = productItemListData.list;

//                            Collections.sort(mProductList, (o1, o2) -> o1.itemNmKr.compareTo(o2.itemNmKr));
                            SpinAdapter adapter =
                                    new SpinAdapter(getApplicationContext(), R.layout.item_delivery_product, mProductList);
//                            mBinding.spinnerProduct.setAdapter(adapter);
                            mBinding.progressBar.setVisibility(View.GONE);

                            mBinding.productName.setAdapter(adapter);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onComplete() {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }
                });


    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public class SpinAdapter extends ArrayAdapter<ProductItemListData.list> {
        private final ArrayList<ProductItemListData.list> values;
        private ArrayList<ProductItemListData.list> suggestions;
        private ArrayList<ProductItemListData.list> tempCustomer;

        SpinAdapter(Context context, int textViewResourceId,
                    ArrayList<ProductItemListData.list> values) {
            super(context, textViewResourceId, values);
            this.values = values;
            suggestions = new ArrayList<>();
            tempCustomer = new ArrayList<>(values);

        }

        @NonNull
        @Override
        public Filter getFilter() {
            return myFilter;
        }

        private Filter myFilter = new Filter() {
            @Override
            public CharSequence convertResultToString(Object resultValue) {
                ProductItemListData.list productInfo = (ProductItemListData.list) resultValue;

                String returnProductInfo = productInfo.itemNmKr.trim() + " " + productInfo.clrtnNm.trim() + " " + productInfo.itemId;
                Log.d("returnProductInfo L : " + returnProductInfo);
                return returnProductInfo;
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    Log.d("constraint != null constraint.toString(): " + constraint.toString());
                    suggestions.clear();
                    for (ProductItemListData.list productInfo : tempCustomer) {
                        if (productInfo.itemNmKr.toLowerCase().contains(constraint.toString().toLowerCase())) {
                            Log.d("constraint.toString(): " + constraint.toString());
                            suggestions.add(productInfo);
                        }
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = suggestions;
                    filterResults.count = suggestions.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                ArrayList<ProductItemListData.list> c = (ArrayList<ProductItemListData.list>) results.values;

                if (results != null && results.count > 0 && !c.isEmpty()) {
                    clear();
                    for (ProductItemListData.list cust : c) {
                        add(cust);
                        notifyDataSetChanged();
                    }
                }
            }
        };

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View vi = convertView;
            if (convertView == null) {
                LayoutInflater inflater = getLayoutInflater();
                vi = inflater.inflate(R.layout.item_delivery_product, null);
                holder = new ViewHolder();
                holder.clrtnNm = (TextView) vi.findViewById(R.id.clrtnNm);
                holder.clsNmKr = (TextView) vi.findViewById(R.id.clsNmKr);
                holder.partNm = (TextView) vi.findViewById(R.id.partNm);
                holder.itemNmKr = (TextView) vi.findViewById(R.id.itemNmKr);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }
            holder.clrtnNm.setText(values.get(position).clrtnNm);
            holder.clsNmKr.setText(values.get(position).clsNmKr);
            holder.partNm.setText(values.get(position).partNm);
            holder.itemNmKr.setText(values.get(position).itemNmKr);
            return vi;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View vi = convertView;
            if (convertView == null) {
                LayoutInflater inflater = getLayoutInflater();
                vi = inflater.inflate(R.layout.item_delivery_product, null);

                holder = new ViewHolder();
                holder.clrtnNm = (TextView) vi.findViewById(R.id.clrtnNm);
                holder.clsNmKr = (TextView) vi.findViewById(R.id.clsNmKr);
                holder.partNm = (TextView) vi.findViewById(R.id.partNm);
                holder.itemNmKr = (TextView) vi.findViewById(R.id.itemNmKr);
                vi.setTag(holder);
            } else {
                holder = (ViewHolder) vi.getTag();
            }
            holder.clrtnNm.setText(values.get(position).clrtnNm);
            holder.clsNmKr.setText(values.get(position).clsNmKr);
            holder.partNm.setText(values.get(position).partNm);
            holder.itemNmKr.setText(values.get(position).itemNmKr);
            return vi;
        }

        public class ViewHolder {
            TextView clrtnNm;
            TextView clsNmKr;
            TextView partNm;
            TextView itemNmKr;
        }
    }

}
