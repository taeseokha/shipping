package com.bodyfriend.shippingsystem.main.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Created by Taeseok on 2017-10-25.
 */

public class WidgetService extends RemoteViewsService
{
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent)
    {
        return(new WidgetDisplay(this.getApplicationContext(), intent));
    }


}
