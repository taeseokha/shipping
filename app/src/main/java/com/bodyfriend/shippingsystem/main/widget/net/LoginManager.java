package com.bodyfriend.shippingsystem.main.widget.net;

import com.bodyfriend.shippingsystem.base.net.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.login.net.login;

import retrofit2.Call;

/**
 * Created by 이주영 on 2017-05-18.
 */

public class LoginManager {



    public Call<login.Data> login(String id, String password, String admin_telno) {
        LoginApi service = ServiceGenerator.createService(LoginApi.class);

        return service.login(id, password, admin_telno);
    }


}
