package com.bodyfriend.shippingsystem.main.shipping_list_finish.net;


import com.bodyfriend.shippingsystem.base.BFEnty;
import com.bodyfriend.shippingsystem.base.NetConst;

import java.util.List;

/**
 * 완료 리스트
 */
public class ShippingDoneList extends BFEnty {

    /**
     * @param s_datetype 날짜타입
     * @param dateStart  날짜시작
     * @param dateEnd    날자끝
     * @param s_area     지역
     * @param s_custname 고객명
     * @param s_addr     주소
     */
    public ShippingDoneList(int s_datetype, String dateStart, String dateEnd, String s_area, String s_custname, String s_addr) {
        setUrl("mobile/api/appShippingDoneList.json");
        setParam("s_datetype", s_datetype, "dateStart", dateStart, "dateEnd", dateEnd, "s_area", s_area, "s_custname"
                , s_custname, "s_addr", s_addr, "s_oneself", "y", "s_producttype", NetConst.s_producttype);
    }

    @Override
    protected void parse(String json) {
        super.parse(json);

//        try {
//            for (Data.ResultData.list item : data.ResultData.list) {
//                try {
//                    Log.d("item.toString() : " + item.toString());
//                    item.DUE_DATE = SDF.mmddE__.format(SDF.yyyymmdd_1.parse(item.IN_DATE));
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public Data data;

    public static class Data {
        public  resultData resultData;
        public String resultMsg;
        public int resultCode;

        public static class resultData {
            public int listCount;
            public List<list> list;
            public list listClass;

            public static class list {
                public  String DUE_DATE_COUNT  ;
                public String PROMISE;
                public String AREA;
                public String PRODUCT_NAME;
                public String DUE_DATE;
                public String DELIVERYMAN1;
                public String PURCHASING_OFFICE;
                public String SHIPPING_SEQ;
                public String SHIPPING_TYPE;
                public String DELIVERYMAN3;
                public String DELIVERYMAN2;
                public String DIV_STATUS;
                public String INS_COMPLETE;
                public String INSADDR;
                public String CUST_NAME;
                public String PRODUCT_TYPE;
                public String CHECK_TYPE;
                public String PHONE_NO;
                public String PROGRESS_NO_NM ;
                public String MUST_FLAG;
                public int QTY;
                public String SHIPPING_PS;
                public String IN_DATE ;
                public String RECEIVE_DATE;
                public boolean isDate;
                public int index;

                @Override
                public String toString() {
                    return "list{" +
                            "PROMISE='" + PROMISE + '\'' +
                            ", AREA='" + AREA + '\'' +
                            ", PRODUCT_NAME='" + PRODUCT_NAME + '\'' +
                            ", DUE_DATE='" + DUE_DATE + '\'' +
                            ", DELIVERYMAN1='" + DELIVERYMAN1 + '\'' +
                            ", PURCHASING_OFFICE='" + PURCHASING_OFFICE + '\'' +
                            ", SHIPPING_SEQ='" + SHIPPING_SEQ + '\'' +
                            ", SHIPPING_TYPE='" + SHIPPING_TYPE + '\'' +
                            ", DELIVERYMAN3='" + DELIVERYMAN3 + '\'' +
                            ", DELIVERYMAN2='" + DELIVERYMAN2 + '\'' +
                            ", DIV_STATUS='" + DIV_STATUS + '\'' +
                            ", INS_COMPLETE='" + INS_COMPLETE + '\'' +
                            ", INSADDR='" + INSADDR + '\'' +
                            ", CUST_NAME='" + CUST_NAME + '\'' +
                            ", PRODUCT_TYPE='" + PRODUCT_TYPE + '\'' +
                            ", CHECK_TYPE='" + CHECK_TYPE + '\'' +
                            ", PHONE_NO='" + PHONE_NO + '\'' +
                            ", MUST_FLAG='" + MUST_FLAG + '\'' +
                            ", QTY=" + QTY +
                            ", SHIPPING_PS='" + SHIPPING_PS + '\'' +
                            ", RECEIVE_DATE='" + RECEIVE_DATE + '\'' +
                            '}';
                }
            }
        }
    }

}

// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "PROMISE": "Y",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "AREA": "충남",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "PRODUCT_NAME": "팬텀(레드)",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "DUE_DATE": "",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "DELIVERYMAN1": "충남영업소",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "PURCHASING_OFFICE": "현대",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "SHIPPING_SEQ": "G201504-0122",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "SHIPPING_TYPE": "",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "DELIVERYMAN3": "",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "DELIVERYMAN2": "",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "DIV_STATUS": "외주",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "INS_COMPLETE": "Y",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "INSADDR": "충남 천안시 서북구 두정동 1371번지 팰리스피아 314호",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "CUST_NAME": "이태경",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "PRODUCT_TYPE": "안마의자",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "CHECK_TYPE": " ",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "PHONE_NO": "010-5454-1177",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "MUST_FLAG": "N",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "QTY": 1,
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "SHIPPING_PS": "test",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "RECEIVE_DATE": "2015-04-01"

