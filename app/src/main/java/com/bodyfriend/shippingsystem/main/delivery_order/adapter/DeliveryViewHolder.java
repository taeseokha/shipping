package com.bodyfriend.shippingsystem.main.delivery_order.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bodyfriend.shippingsystem.databinding.ItemDeliveryOrder2Binding;
import com.bodyfriend.shippingsystem.main.delivery_order.net.OutLoiList;

/**
 * Created by 이주영 on 2016-12-08.
 */

public class DeliveryViewHolder extends RecyclerView.ViewHolder {

    private final ItemDeliveryOrder2Binding mBinding;



    DeliveryViewHolder(ItemDeliveryOrder2Binding itemView) {
        super(itemView.getRoot());
        mBinding = itemView;
    }

    /**
     * @param data
     */
    void setupData(OutLoiList.list data) {
        mBinding.itemDate.setText(data.regDt);
        mBinding.productName.setText(data.loiNo);
        mBinding.qty.setText(String.valueOf(data.modelTotal));
        mBinding.shippingType.setText(data.procStatNm);
        if (data.procStatCd.equals("ST99")) {
            mBinding.delveryOrderBgCheck.setVisibility(View.VISIBLE);
        } else {
            mBinding.delveryOrderBgCheck.setVisibility(View.GONE);
        }

        StringBuffer stringBuffer = new StringBuffer();
        for (OutLoiList.list.models models : data.models) {
            String itemExplen = models.itemExpln.split(",")[0];
            stringBuffer.append("수량 : ").append(models.qty).append("\t 품목 : ").append(itemExplen)
                    .append("\t 상태 : ").append(models.gdsStatNm)
                    .append("\n");
        }
        mBinding.graph1.setText(stringBuffer.toString());
    }
}
