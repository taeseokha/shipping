package com.bodyfriend.shippingsystem.main.widget;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;

import com.bodyfriend.shippingsystem.R;

public class CallActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_call);

        String word = getIntent().getStringExtra(ServiceList.EXTRA_WORD);

        if (word != null) {
//            Log.d("word : " + word);
            Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + word));
            startActivity(callIntent);
            finish();
        }
    }

}
