package com.bodyfriend.shippingsystem.main.benefit;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Taeseok on 2018-03-05.
 */

public interface BenefitApi {


    @FormUrlEncoded
    @POST("ShippingMgt/ShippingMonthlyList/selectMonthlyReportList.json")
    Observable<BenefitData> getMonthlyBenefit(
            @Field("employeeId") String employeeId
            , @Field("dateStart") String dateStart
            , @Field("dateEnd") String admin_id

    );


    @FormUrlEncoded
    @POST("ShippingMgt/ShippingMonthlyList/selectSummaryApiList.json")
    Observable<BenefitSummaryData> getSummaryBenefit(
            @Field("employeeId") String employeeId
            , @Field("dateStart") String dateStart
            , @Field("dateEnd") String admin_id

    );


}
