package com.bodyfriend.shippingsystem.main.shipping_list.sms.net;


import com.bodyfriend.shippingsystem.base.BFEnty;

/**
 * Created by 이주영 on 2016-09-07.
 * 안녕하십니까

 전산실 민경환 과장입니다.

 요청하신 서비스배송 기사 문자 발송 로그 저장 API 공유드립니다

 API 주소 : /mobile/api/insertWorkMsgLog.json
 파라미터 :
 svc_seq=20160907M100066
 admin_id=svcadmin
 admin_nm=민경환
 tel_no=01098797310
 group_cd=SV
 per_cd=104
 msg=메시지내용
 msg_gb=2
 product_type=M

 감사합니다.

 민경환 드림
 */
public class insertWorkMsgLog extends BFEnty {
    public insertWorkMsgLog(String svc_seq, String admin_id, String admin_nm, String tel_no, String group_cd, int per_cd, String msg, int msg_gb, String product_type){
        setUrl("mobile/api/insertWorkMsgLog.json");
        setParam(
                "svc_seq"       , svc_seq
                , "admin_id"    , admin_id
                , "admin_nm"    , admin_nm
                , "tel_no"      , tel_no
                , "group_cd"    , group_cd
                , "per_cd"      , per_cd
                , "msg"         , msg
                , "msg_gb"      , msg_gb
                , "product_type", product_type

        );
    }

//    svc_seq=20160907M100066
//            admin_id=svcadmin
//    admin_nm=민경환
//            tel_no=01098797310
//    group_cd=SV
//            per_cd=104
//    msg=메시지내용
//            msg_gb=2
//    product_type=M
}
