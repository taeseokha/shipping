package com.bodyfriend.shippingsystem.main.shipping_list.net;

import android.location.Location;

import java.util.HashMap;
import java.util.List;

public class ShippingList {
    public resultData resultData;
    public String resultMsg;
    public int resultCode;

    public static class resultData {

        public int listCount;
        public List<list> list;

        public static class list {

            public int index; // 이건 아답터에 표기될 순서
            public String DUE_DATE;
            public String DUE_DATE_COUNT;
            public boolean isDate; // 이건 아답터에 표기될 날짜( 날짜이면 아래의 값들은 없다.)
            public String CUST_UPDATE;
            public String DIV_DATE;
            public String PROMISE;
            public String PROMISE_TIME;
            public String HPHONE_NO;
            public String AREA;
            public String PRODUCT_NAME;
            public String FREEGIFT_ONE;
            public String FREEGIFT_TWO;
            public String TEL_NO;
            public String PURCHASING_OFFICE;
            public String SHIPPING_SEQ;
            public String SERVICE_MONTH;
            public String SHIPPING_TYPE;
            public String INSADDR;
            public String CUST_NAME;
            public String SYMPTOM;
            public String DELIVERYMAN;
            public String MUST_FLAG;
            public int QTY;
            public String SHIPPING_PS;
            public String PROMISE_FAIL_PS;
            public String INS_COMPLETE;
            public String INS_COMPLATE;
            public String PROGRESS_NO;
            public String EXPIRE_FLAG;
            public String RELOCATION_CHECK;
            public boolean HAS_4_MSG;
            public String COMPANY_CHECK;
            public String RECEIVE_DATE;

            public Location location;

            public void setLocation(Location location) {
                this.location = location;
            }

            @Override
            public String toString() {
                return "list{" +
                        "index=" + index +
                        ", DUE_DATE='" + DUE_DATE + '\'' +
                        ", DUE_DATE_COUNT='" + DUE_DATE_COUNT + '\'' +
                        ", isDate=" + isDate +
                        ", CUST_UPDATE='" + CUST_UPDATE + '\'' +
                        ", DIV_DATE='" + DIV_DATE + '\'' +
                        ", PROMISE='" + PROMISE + '\'' +
                        ", PROMISE_TIME='" + PROMISE_TIME + '\'' +
                        ", HPHONE_NO='" + HPHONE_NO + '\'' +
                        ", AREA='" + AREA + '\'' +
                        ", PRODUCT_NAME='" + PRODUCT_NAME + '\'' +
                        ", FREEGIFT_ONE='" + FREEGIFT_ONE + '\'' +
                        ", TEL_NO='" + TEL_NO + '\'' +
                        ", PURCHASING_OFFICE='" + PURCHASING_OFFICE + '\'' +
                        ", SHIPPING_SEQ='" + SHIPPING_SEQ + '\'' +
                        ", SERVICE_MONTH='" + SERVICE_MONTH + '\'' +
                        ", SHIPPING_TYPE='" + SHIPPING_TYPE + '\'' +
                        ", INSADDR='" + INSADDR + '\'' +
                        ", CUST_NAME='" + CUST_NAME + '\'' +
                        ", DELIVERYMAN='" + DELIVERYMAN + '\'' +
                        ", MUST_FLAG='" + MUST_FLAG + '\'' +
                        ", QTY=" + QTY +
                        ", SHIPPING_PS='" + SHIPPING_PS + '\'' +
                        ", PROMISE_FAIL_PS='" + PROMISE_FAIL_PS + '\'' +
                        ", INS_COMPLETE='" + INS_COMPLETE + '\'' +
                        ", INS_COMPLATE='" + INS_COMPLATE + '\'' +
                        ", PROGRESS_NO='" + PROGRESS_NO + '\'' +
                        ", EXPIRE_FLAG='" + EXPIRE_FLAG + '\'' +
                        ", RELOCATION_CHECK='" + RELOCATION_CHECK + '\'' +
                        ", HAS_4_MSG=" + HAS_4_MSG +
                        ", COMPANY_CHECK='" + COMPANY_CHECK + '\'' +
                        ", location=" + location +
                        '}';
            }

            private HashMap<String, Integer> deliveryItems = new HashMap<>();
            public HashMap<String, Integer> getDeliveryItems() {
                return deliveryItems;
            }

            public void setDeliveryItems(HashMap<String, Integer> deliveryItems) {
                this.deliveryItems = deliveryItems;
            }
        }

        @Override
        public String toString() {
            return "resultData{" +
                    "listCount=" + listCount +
                    ", list=" + list +
                    '}';
        }
    }


//	03-23 09:35:52.397: W/(23107):                 "PRODUCT_NAME": "뉴크루즈파워(블랙)",
//	03-23 09:35:52.397: W/(23107):                 "FREEGIFT_ONE": "",
//	03-23 09:35:52.397: W/(23107):                 "PROMISE_FAIL_CD": "",
//	03-23 09:35:52.397: W/(23107):                 "PURCHASING_OFFICE": "옥션",
//	03-23 09:35:52.397: W/(23107):                 "INS_COMPLETE": "",
//	03-23 09:35:52.409: W/(23107):                 "DELIVERYMAN": "박성훈",
//	03-23 09:35:52.409: W/(23107):                 "MUST_FLAG": "N",
//	03-23 09:35:52.409: W/(23107):                 "QTY": "1",
//	03-23 09:35:52.409: W/(23107):                 "HPHONE_NO": "010-4666-4920",
//	03-23 09:35:52.409: W/(23107):                 "BACK_SN": "",
//	03-23 09:35:52.409: W/(23107):                 "PROMISE": "Y",
//	03-23 09:35:52.409: W/(23107):                 "AREA": "경기",
//	03-23 09:35:52.409: W/(23107):                 "PROMISE_TIME": "",
//	03-23 09:35:52.409: W/(23107):                 "PROGRESS_NO": "1",
//	03-23 09:35:52.409: W/(23107):                 "DUE_DATE": "",
//	03-23 09:35:52.409: W/(23107):                 "TEL_NO": "010-4666-4920",
//	03-23 09:35:52.409: W/(23107):                 "SHIPPING_SEQ": "20160322M00001",
//	03-23 09:35:52.410: W/(23107):                 "DIV_DATE": "2016-03-21",
//	03-23 09:35:52.410: W/(23107):                 "SHIPPING_TYPE": "배송",
//	03-23 09:35:52.410: W/(23107):                 "SERVICE_MONTH": "12",
//	03-23 09:35:52.410: W/(23107):                 "INSADDR": "경기 안산시 단원구 와동 108-13 203호",
//	03-23 09:35:52.410: W/(23107):                 "EXPIRE_FLAG": "RED",
//	03-23 09:35:52.410: W/(23107):                 "CUST_NAME": "김미숙",
//	03-23 09:35:52.410: W/(23107):                 "PROMISE_FAIL_PS": "",
//	03-23 09:35:52.410: W/(23107):                 "SHIPPING_PS": ""
//	03-23 09:35:52.410: W/(23107):             },
//	03-23 09:35:52.410: W/(23107):             {
//	03-23 09:35:52.410: W/(23107):                 "PRODUCT_NAME": "프레지던트(연카키)",
//	03-23 09:35:52.410: W/(23107):                 "FREEGIFT_ONE": "bodykdb",
//	03-23 09:35:52.410: W/(23107):                 "PROMISE_FAIL_CD": "",
//	03-23 09:35:52.410: W/(23107):                 "PURCHASING_OFFICE": "롯데",
//	03-23 09:35:52.410: W/(23107):                 "INS_COMPLETE": "",
//	03-23 09:35:52.410: W/(23107):                 "DELIVERYMAN": "박성훈",
//	03-23 09:35:52.410: W/(23107):                 "MUST_FLAG": " ",
//	03-23 09:35:52.410: W/(23107):                 "QTY": "1",
//	03-23 09:35:52.410: W/(23107):                 "HPHONE_NO": "010-3729-8501",
//	03-23 09:35:52.410: W/(23107):                 "BACK_SN": "",
//	03-23 09:35:52.410: W/(23107):                 "PROMISE": " ",
//	03-23 09:35:52.410: W/(23107):                 "AREA": "경기",
//	03-23 09:35:52.410: W/(23107):                 "PROMISE_TIME": "",
//	03-23 09:35:52.411: W/(23107):                 "PROGRESS_NO": "1",
//	03-23 09:35:52.411: W/(23107):                 "DUE_DATE": "",
//	03-23 09:35:52.411: W/(23107):                 "TEL_NO": "010-3729-8501",
//	03-23 09:35:52.411: W/(23107):                 "SHIPPING_SEQ": "20160314M00206",
//	03-23 09:35:52.411: W/(23107):                 "DIV_DATE": "2016-03-21",
//	03-23 09:35:52.411: W/(23107):                 "SHIPPING_TYPE": "이전요청",
//	03-23 09:35:52.411: W/(23107):                 "SERVICE_MONTH": "39",
//	03-23 09:35:52.411: W/(23107):                 "INSADDR": "경기 안양시 만안구 안양6동 521- 25호 2층",
//	03-23 09:35:52.411: W/(23107):                 "EXPIRE_FLAG": "RED",
//	03-23 09:35:52.411: W/(23107):                 "CUST_NAME": "조양익",
//	03-23 09:35:52.411: W/(23107):                 "PROMISE_FAIL_PS": "",
//	03-23 09:35:52.411: W/(23107):                 "SHIPPING_PS": ""

}
