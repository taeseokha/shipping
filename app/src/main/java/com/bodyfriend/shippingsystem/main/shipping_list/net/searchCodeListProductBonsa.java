package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 본사/외주
 */
public class searchCodeListProductBonsa extends searchCodeList {
	public searchCodeListProductBonsa() {
		super();
		setParam("code", 9300);
	}

	public Data data;

	public static class Data {
		public List<resultData> resultData;
		public String resultMsg;
		public int resultCode;

		public class resultData {
			public long INS_DT;
			public String DET_CD;
			public String DESCRIPTION;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String UPD_ID;
			public long UPD_DT;
			public String GROUP_CD;
			public String INS_ID;
			public String SORT_SEQ;
			public String REF_2;
			public String REF_3;
			public String REF_1;
			public String COMM_CD;
		}
	}
	
//	05-04 09:28:00.946: W/_NETLOG_IN(10111):         "DIV_STATUS": "O",

//	05-04 10:18:26.873: W/_NETLOG_IN(18469):     "ResultData": [
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):         {
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "INS_DT": 1429090719310,
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "DET_CD": "I",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "DESCRIPTION": "",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "DET_CD_NM": "본사",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "COMM_CD_NM": "본사\/외주",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "UPD_ID": "adminsp",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "UPD_DT": 1429090719310,
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "GROUP_CD": "SP",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "INS_ID": "adminsp",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "SORT_SEQ": "1",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "REF_2": "",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "REF_3": "",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "REF_1": "",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "COMM_CD": "9300"
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):         },
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):         {
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "INS_DT": 1429090730410,
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "DET_CD": "O",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "DESCRIPTION": "",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "DET_CD_NM": "외주",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "COMM_CD_NM": "본사\/외주",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "UPD_ID": "adminsp",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "UPD_DT": 1429090740240,
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "GROUP_CD": "SP",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "INS_ID": "adminsp",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "SORT_SEQ": "2",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "REF_2": "",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "REF_3": "",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "REF_1": "",
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):             "COMM_CD": "9300"
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):         }
//	05-04 10:18:26.873: W/_NETLOG_IN(18469):     ]

}
