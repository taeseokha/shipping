package com.bodyfriend.shippingsystem.main.benefit;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bodyfriend.shippingsystem.R;

import java.util.ArrayList;

class BenefitAdapter extends RecyclerView.Adapter<BenefitListViewHolder> {
    private ArrayList<BenefitSummaryData2.ItemBenefit> mBenefitList;
    private Context mContext;

    @Override
    public BenefitListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        if (mContext != null) {
            return new BenefitListViewHolder(DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()), R.layout.item_benefit,
                    parent, false));
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(BenefitListViewHolder holder, int position) {
        BenefitSummaryData2.ItemBenefit itemBenefit = mBenefitList.get(position);
        holder.mBinding.setViewData(itemBenefit);
    }

    @Override
    public int getItemCount() {
        return (mBenefitList == null) ? 0 : mBenefitList.size();
    }

    public void clear() {
        if (mBenefitList != null) {
            mBenefitList.clear();
        }
        notifyDataSetChanged();
    }

    public void setBenefitList(ArrayList<BenefitSummaryData2.ItemBenefit> movie) {
        mBenefitList = movie;
        notifyDataSetChanged();
    }
}
