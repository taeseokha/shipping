package com.bodyfriend.shippingsystem.main.delivery_order.custom_view;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.WhFailAdapter;
import com.bodyfriend.shippingsystem.main.delivery_order.net.createInstruction;

/**
 * Created by 이주영 on 2017-01-03.
 */

public class WhFailView extends LinearLayout {
    ViewPager viewPager;
    createInstruction.Data resultData;
    View mContent;

    public WhFailView(Context context, createInstruction.Data resultData) {
        super(context);
        mContent = setupLayout(context);
        this.resultData = resultData;

        setupViewPager();
        setupTabToobar();

    }

    /**
     * 인디케이터 하단의 도트 추가.
     */
    private void setupTabToobar() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
        tabLayout.setupWithViewPager(viewPager);
    }

    /**
     * viewPager 초기화
     */
    private void setupViewPager() {
        viewPager = (ViewPager) mContent.findViewById(R.id.viewPager);
//        viewPager.setAdapter(new WhFailAdapter(getContext(), ResultData.ResultData));
    }

    /**
     * 레이아웃 초기화
     */
    private View setupLayout(Context context) {
        View content = LayoutInflater.from(context).inflate(R.layout.delivery_order_wh_fail_view, null);
        addView(content);
        return content;
    }
}
