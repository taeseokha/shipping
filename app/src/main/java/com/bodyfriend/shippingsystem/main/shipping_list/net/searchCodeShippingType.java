package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * ShippingType
 */
public class searchCodeShippingType extends searchCodeList {

	public searchCodeShippingType() {
		super();
//		setEnableProgress();
		setParam("code", 9110);
	}

	public Data data;

	public static class Data {
		public List<resultData> resultData;
		public int resultCode;
		public String resultMsg;

		public class resultData {
			public long INS_DT;
			public String DET_CD;
			public String DESCRIPTION;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String UPD_ID;
			public long UPD_DT;
			public String GROUP_CD;
			public String INS_ID;
			public String SORT_SEQ;
			public String REF_2;
			public String REF_3;
			public String REF_1;
			public String COMM_CD;
		}
	}

//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "INS_DT": 1432798269437,
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "DET_CD": "0",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "DESCRIPTION": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "DET_CD_NM": "설치",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "COMM_CD_NM": "배송단계",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "UPD_ID": "adminsp",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "UPD_DT": 1432809240953,
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "GROUP_CD": "SP",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "INS_ID": "adminsp",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "SORT_SEQ": "1",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "REF_2": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "REF_3": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "REF_1": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "COMM_CD": "9110"
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):         },
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):         {
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "INS_DT": 1432798275993,
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "DET_CD": "1",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "DESCRIPTION": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "DET_CD_NM": "회수",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "COMM_CD_NM": "배송단계",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "UPD_ID": "adminsp",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "UPD_DT": 1432809247017,
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "GROUP_CD": "SP",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "INS_ID": "adminsp",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "SORT_SEQ": "2",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "REF_2": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "REF_3": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "REF_1": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "COMM_CD": "9110"
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):         },
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):         {
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "INS_DT": 1432803337363,
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "DET_CD": "2",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "DESCRIPTION": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "DET_CD_NM": "이동",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "COMM_CD_NM": "배송단계",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "UPD_ID": "adminsp",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "UPD_DT": 1432809251340,
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "GROUP_CD": "SP",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "INS_ID": "adminsp",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "SORT_SEQ": "3",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "REF_2": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "REF_3": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "REF_1": "",
//	05-28 20:07:18.226: W/_NETLOG_IN(6019):             "COMM_CD": "9110"


}
