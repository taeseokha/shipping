package com.bodyfriend.shippingsystem.main.benefit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;

import com.bodyfriend.shippingsystem.base.MonthYearPickerDialog;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.login.Auth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class BenefitViewModel extends BaseObservable implements DatePickerDialog.OnDateSetListener {

    private static BenefitAdapter mAdapter;
    private final Context mContext;
    private final Calendar startCalendar;
    public final ObservableField<String> dateStart = new ObservableField<>();
    public final ObservableField<String> totalBenefit = new ObservableField<>();
    public final ObservableBoolean mBenefitTotal = new ObservableBoolean();

    public final ObservableInt isEmptData = new ObservableInt();
    private final BenefitViewModelCallback mCallback;

    public interface BenefitViewModelCallback {
        void onSummaryBenefit(BenefitSummaryData2 benefitData);

        void onEmptyBenefit();
    }

    BenefitViewModel(Context context, BenefitViewModelCallback callback) {
        this.mContext = context;
        startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, 1);
        String startDay = SDF.yyyymm3.format(startCalendar.getTime());

        int actualMaximum = startCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        Log.d("actualMaximum : " + actualMaximum);

        @SuppressLint("DefaultLocale") String endDay = startDay + String.format("-%02d", actualMaximum);
        @SuppressLint("DefaultLocale") String startSearchDay = startDay + String.format("-%02d", 1);

        getSummaryBenefit(Auth.getId(), startSearchDay, endDay);
        mCallback = callback;

        getBenefit(Auth.getId(), startSearchDay, endDay);

        dateStart.set(startSearchDay + " ~ " + endDay);
    }


    private void getBenefit(String employeeId, String dateStart, String dateEnd) {

        DisposableObserver<BenefitData> benefitDataDisposableObserver = ServiceGenerator.createService(BenefitApi.class)
                .getMonthlyBenefit(employeeId, dateStart, dateEnd)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<BenefitData>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    public void onNext(BenefitData benefitData) {

                        if (benefitData.resultData != null && !benefitData.resultData.isEmpty()) {
                            StringBuffer stringBuffer = new StringBuffer();
                            for (BenefitData.resultData data : benefitData.resultData) {
                                stringBuffer.append(String.format("%,d 원", data.GRAND_TOTAL));
                            }
                            totalBenefit.set(stringBuffer.toString());

                        } else {
                            totalBenefit.set("검색 기간내 수당이 없습니다.");
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }


    private void getSummaryBenefit(String employeeId, String dateStart, String dateEnd) {
        isEmptData.set(View.VISIBLE);
        isEmptData.notifyChange();
        DisposableObserver<BenefitSummaryData> benefitDataDisposableObserver = ServiceGenerator.createService(BenefitApi.class)
                .getSummaryBenefit( employeeId, dateStart, dateEnd)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<BenefitSummaryData>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    public void onNext(BenefitSummaryData benefitData) {
                        isEmptData.set(View.GONE);
                        isEmptData.notifyChange();
                        if (benefitData.resultData != null && !benefitData.resultData.isEmpty()) {
                            setDayPerData(benefitData);
                            mBenefitTotal.set(true);
                        } else {
                            mCallback.onEmptyBenefit();
                            mBenefitTotal.set(false);
                        }
                        mBenefitTotal.notifyChange();
                    }

                    @Override
                    public void onError(Throwable e) {
                        isEmptData.set(View.GONE);
                        isEmptData.notifyChange();
                    }

                    @Override
                    public void onComplete() {
                        isEmptData.set(View.GONE);
                        isEmptData.notifyChange();
                    }
                });
    }


    @BindingAdapter("shippingItem")
    public static void bindItem(RecyclerView recyclerView, ObservableArrayList<BenefitSummaryData2.ItemBenefit> movie) {

        mAdapter = (BenefitAdapter) recyclerView.getAdapter();
        if (mAdapter != null && movie != null) {

            if (movie.size() == 0) {
                mAdapter.clear();
            } else {
                mAdapter.setBenefitList(movie);
            }
        }
    }

    @SuppressLint("DefaultLocale")
    private void setDayPerData(BenefitSummaryData benefitData) {
        HashMap<String, List<BenefitSummaryData.ResultDatum>> hashMap = new HashMap<>();
        List<BenefitSummaryData.ResultDatum> list = benefitData.resultData;


        for (BenefitSummaryData.ResultDatum resultData : list) {
//            Log.d("ResultData : " + ResultData.toString());
            List<BenefitSummaryData.ResultDatum> list2 = hashMap.get(resultData.iNDATE);
            if (list2 == null) {
                list2 = new ArrayList<>();
            }
            list2.add(resultData);
            hashMap.put(resultData.iNDATE, list2);
        }

        String[] keySet = hashMap.keySet().toArray(new String[hashMap.keySet().size()]);
        Arrays.sort(keySet);
        ArrayList<BenefitSummaryData2.ItemBenefit> itemBenefits = new ArrayList<>();
        for (String key : keySet) {
            BenefitSummaryData2.ItemBenefit itemBenefit = new BenefitSummaryData2.ItemBenefit();

            itemBenefit.day = key.substring(key.length() - 2, key.length()) + " 일";
            List<BenefitSummaryData.ResultDatum> list3 = hashMap.get(key);
            int total = 0;
            StringBuilder benefitInfo = new StringBuilder("\n");

            for (BenefitSummaryData.ResultDatum resultDatum : list3) {
                if (!resultDatum.tYPE.equals("성과급수당")) {
                    total += resultDatum.tOTAL;
                    benefitInfo.append(resultDatum.tYPE).append(" : ")
                            .append(String.format("%,d 원", resultDatum.tOTAL))
                            .append("( ")
                            .append(resultDatum.dAILYCOUNT)
                            .append(" )")
                            .append("\n");
                }
            }
            itemBenefit.total = String.format("%,d원", total);
            itemBenefit.benefitSummaryInfo = benefitInfo.toString();

            itemBenefits.add(itemBenefit);
        }

        BenefitSummaryData2 benefitSummaryData2 = new BenefitSummaryData2();
        benefitSummaryData2.listHashMap = itemBenefits;
        mCallback.onSummaryBenefit(benefitSummaryData2);

    }


    public void onBenefitSearch(View view) {
        if (mAdapter != null) {
            mAdapter.clear();
        }
        totalBenefit.set("");

        String startDay = SDF.yyyymm3.format(startCalendar.getTime());

        int actualMaximum = startCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        @SuppressLint("DefaultLocale") String endDay = startDay + String.format("-%02d", actualMaximum);
        Log.d("endDay : " + endDay);

        getSummaryBenefit(Auth.getId(), SDF.yyyymmdd_1.format(startCalendar.getTime()), endDay);
        getBenefit(Auth.getId(), SDF.yyyymmdd_1.format(startCalendar.getTime()), endDay);
    }

    public void showMonthYearPickerDialog(View view) {
        MonthYearPickerDialog pd = new MonthYearPickerDialog();
        pd.setListener(this);
        pd.show(((Activity) mContext).getFragmentManager(), "MonthYearPickerDialog");
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        startCalendar.set(year, month - 1, 1);
        String startDay = SDF.yyyymm3.format(startCalendar.getTime());
        Log.d("startDay : " + startDay);
        int actualMaximum = startCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        @SuppressLint("DefaultLocale") String endDay = startDay + String.format("-%02d", actualMaximum);
        @SuppressLint("DefaultLocale") String startSearchDay = startDay + String.format("-%02d", 1);

        dateStart.set(startSearchDay + " ~ " + endDay);

    }
}
