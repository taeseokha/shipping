package com.bodyfriend.shippingsystem.main.shipping_list;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appRequestService;

/**
 * 서비스 등록
 */
public class AppRequestServiceActivity extends BFActivity {

	// m_producttype (제품구분)
	// m_productsn (S/N)
	// m_custname (고객명)
	// m_tel (전화)
	// m_phone (휴대전화)
	// m_address (주소)
	// m_productname (제품명)
	// m_serviceps (서비스접수 비고)
	public static class EXTRA {
		public static final String m_producttype = "m_producttype";
		public static final String m_custname = "m_custname";
		public static final String m_tel = "m_tel";
		public static final String m_phone = "m_phone";
		public static final String m_address = "m_address";
		public static final String m_productname = "m_productname";
		public static final String m_serviceps = "m_serviceps";
		public static final String m_purchaseoffice = "m_purchaseoffice";
		public static final String m_servicemonth = "m_servicemonth";
		
	}

	private String m_producttype;// (제품구분)
	// private String m_productsn;// (S/N)
	private String m_custname;// (고객명)
	private String m_tel;// (전화)
	private String m_phone;// (휴대전화)
	private String m_address;// (주소)
	private String m_productname; // (제품명)
	private String m_purchaseoffice;// 구매처
	private String m_servicemonth; // 서비스기간

	// private String m_serviceps;// (서비스접수 비고)

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		setContentView(R.layout.popup_app_request_service);
	}

	@Override
	protected void onParseExtra() {
		super.onParseExtra();

		Intent i = getIntent();
		m_producttype = i.getStringExtra(EXTRA.m_producttype);
		m_custname = i.getStringExtra(EXTRA.m_custname);
		m_tel = i.getStringExtra(EXTRA.m_tel);
		m_phone = i.getStringExtra(EXTRA.m_phone);
		m_address = i.getStringExtra(EXTRA.m_address);
		m_productname = i.getStringExtra(EXTRA.m_productname);
		m_servicemonth = i.getStringExtra(EXTRA.m_servicemonth);
		m_purchaseoffice = i.getStringExtra(EXTRA.m_purchaseoffice);
	}

	@Override
	protected void onLoadOnce() {
		super.onLoadOnce();

		setFinish(R.id.close);
		setOnClickListener(R.id.ok, onOkClickListener);

		setText(R.id.m_custname, m_custname);
		setText(R.id.m_tel, m_tel);
		setText(R.id.m_phone, m_phone);
		setText(R.id.m_address, m_address);
		setText(R.id.m_productname, m_productname);
		
		setText(R.id.m_servicemonth, m_servicemonth);
		setText(R.id.m_purchaseoffice, m_purchaseoffice);

	}
	
	@Override
	public boolean check() {
		if(isEmpty(R.id.m_custname)){
//			return false;
		}
		if(isEmpty(R.id.m_tel)){
//			return false;
		}
		if(isEmpty(R.id.m_phone)){
//			toast("주소가 입력되지 않았습니다.");
//			return false;
		}
		if(isEmpty(R.id.m_address)){
//			toast("주소가 입력되지 않았습니다.");
//			return false;
		}
		if(isEmpty(R.id.m_productname)){
			toast("제품명이 입력되지 않았습니다.");
			return false;
		}
		if(isEmpty(R.id.m_productsn)){
			toast("S/N을 입력해주세요.");
			return false;
		}
		if(isEmpty(R.id.m_serviceps)){
//			toast("S/N을 입력해주세요.");
//			return false;
		}
		
		return true;
	}

	private OnClickListener onOkClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(check() == false){
				return;
			}
			String m_productsn = text(R.id.m_productsn);
			String m_serviceps = text(R.id.m_serviceps);
			showProgress();
			
//			m_servicemonth = i.getStringExtra(EXTRA.m_servicemonth);
//			m_purchaseoffice = i.getStringExtra(EXTRA.m_purchaseoffice);
//			Net.async(new appRequestService(m_producttype, m_productsn, m_custname, m_tel, m_phone, m_address, m_productname, m_serviceps, m_servicemonth, m_purchaseoffice)).setOnNetResponse(onNetResponse);
		}
	};

	private Net.OnNetResponse<appRequestService> onNetResponse = new Net.OnNetResponse<appRequestService>() {

		private android.content.DialogInterface.OnClickListener positiveListener = new android.content.DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		};

		@Override
		public void onResponse(appRequestService response) {
			hideProgress();
			
			showDialog(response.data.resultMsg, "확인", positiveListener);
		}

		@Override
		public void onErrorResponse(VolleyError error) {
			hideProgress();
		}
	};
	
	@Override
	public void onBackPressed() {
		finish();
	}
}
