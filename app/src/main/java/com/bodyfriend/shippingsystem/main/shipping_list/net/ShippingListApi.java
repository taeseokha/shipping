package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.delivery_order.net.BfData;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ShippingListApi {


    @FormUrlEncoded
    @POST("ShippingMgt/ShippingTeamMgt/insertKakaoTalk.json")
    Observable<BfData> insertKaKaoTalk(
            @Field("shipping_seq") String shipping_seq
            ,@Field("sess_id") String deliveryManId
    );
}
