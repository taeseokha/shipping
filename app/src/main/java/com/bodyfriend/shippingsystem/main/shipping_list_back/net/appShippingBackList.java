package com.bodyfriend.shippingsystem.main.shipping_list_back.net;

import com.bodyfriend.shippingsystem.base.BFEnty;
import com.bodyfriend.shippingsystem.base.NetConst;

import java.util.List;

/**
 * 완료 리스트
 */
public class appShippingBackList extends BFEnty {

    /**
     * @param s_datetype 날짜타입
     * @param dateStart  날짜시작
     * @param dateEnd    날자끝
     * @param s_area     지역
     * @param s_custname 고객명
     * @param s_addr     주소
     */
    public appShippingBackList(int s_datetype, String dateStart, String dateEnd, String s_area, String s_custname, String s_addr) {
        setUrl("mobile/api/appShippingBackList.json");
        setParam("s_datetype", s_datetype, "dateStart", dateStart, "dateEnd", dateEnd, "s_area", s_area, "s_custname", s_custname, "s_addr", s_addr, "s_oneself", "y", "s_producttype", NetConst.s_producttype);
    }

    public Data data;

    public static class Data {
        public resultData resultData;
        public String resultMsg;
        public int resultCode;

        public class resultData {
            public int listCount;
            public List<list> list;

            public class list {
                public String PROMISE;
                public String AREA;
                public String PRODUCT_NAME;
                public String DUE_DATE;
                public String DELIVERYMAN1;
                public String PURCHASING_OFFICE;
                public String SHIPPING_SEQ;
                public String SHIPPING_TYPE;
                public String DELIVERYMAN3;
                public String DELIVERYMAN2;
                public String DIV_STATUS;
                public String INS_COMPLETE;
                public String INSADDR;
                public String CUST_NAME;
                public String PRODUCT_TYPE;
                public String CHECK_TYPE;
                public String PHONE_NO;
                public String MUST_FLAG;
                public int QTY;
                public String SHIPPING_PS;
                public String BACK_DATE;
            }
        }
    }

}

// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "PROMISE": "Y",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "AREA": "충남",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "PRODUCT_NAME": "팬텀(레드)",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "DUE_DATE": "",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "DELIVERYMAN1": "충남영업소",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "PURCHASING_OFFICE": "현대",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "SHIPPING_SEQ": "G201504-0122",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "SHIPPING_TYPE": "",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "DELIVERYMAN3": "",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "DELIVERYMAN2": "",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "DIV_STATUS": "외주",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "INS_COMPLETE": "Y",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "INSADDR": "충남 천안시 서북구 두정동 1371번지 팰리스피아 314호",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "CUST_NAME": "이태경",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "PRODUCT_TYPE": "안마의자",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "CHECK_TYPE": " ",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "PHONE_NO": "010-5454-1177",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "MUST_FLAG": "N",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "QTY": 1,
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "SHIPPING_PS": "test",
// 05-04 20:51:54.006: W/_NETLOG_IN(17456): "RECEIVE_DATE": "2015-04-01"

