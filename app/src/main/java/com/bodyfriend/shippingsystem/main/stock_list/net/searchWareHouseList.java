package com.bodyfriend.shippingsystem.main.stock_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.util.List;

/**
 * Created by 이주영 on 2016-10-25.
 * <p>
 * <p>
 * 창고리스트(SELECT BOX 세팅) : "/stats/admin/searchWareHouseList.json"
 * 파라미터 : 없음
 * 리턴값 : map
 * (
 * map.WH_SEQ : 창고SEQ
 * , map.WH_NAME : 창고명
 * , map.AREA : 지역
 * )
 */

public class searchWareHouseList extends BFEnty {
    public searchWareHouseList() {
        setUrl("stats/admin/searchWareHouseList.json");
    }

    public Data data;

    public static class Data {
        public List<resultData> resultData;

        public class resultData {
            public long REG_DATE;
            public String AREA;
            public String WH_NAME;
            public String ADDR;
            public String WH_SEQ;
        }
    }
}
