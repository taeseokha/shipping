package com.bodyfriend.shippingsystem.main.map.net;

import com.bodyfriend.shippingsystem.main.map.vo.Geocorder;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by ts.ha on 2017-05-12.
 */

public interface NaverGeocoderApi {

    @GET("geocode?")
    Call<Geocorder> getGeocoder(@Query("query") String query);


}
