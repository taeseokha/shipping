package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 약속 불가 사유
 * 
 * @author 이주영
 * 
 */
public class searchCodeListPromise extends searchCodeList {
	public searchCodeListPromise() {
		super();
//		setEnableProgress();
		setParam("code", 9400);
	}

	public Data data;

	public static class Data {
		public List<resultData> resultData;
		public int resultCode;
		public String resultMsg;

		public class resultData {
			public long INS_DT;
			public String DET_CD;
			public String DESCRIPTION;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String UPD_ID;
			public long UPD_DT;
			public String GROUP_CD;
			public String INS_ID;
			public String SORT_SEQ;
			public String REF_2;
			public String REF_3;
			public String REF_1;
			public String COMM_CD;
		}
	}
	
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "INS_DT": 1431566979867,
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "DET_CD": "04",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "DESCRIPTION": "",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "DET_CD_NM": "지정일요청",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "COMM_CD_NM": "약속불가",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "UPD_ID": "adminsp",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "UPD_DT": 1431566979867,
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "GROUP_CD": "SP",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "INS_ID": "adminsp",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "SORT_SEQ": "4",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "REF_2": "",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "REF_3": "",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "REF_1": "",
//	05-14 10:40:31.612: W/_NETLOG_IN(13554):             "COMM_CD": "9400"


}
