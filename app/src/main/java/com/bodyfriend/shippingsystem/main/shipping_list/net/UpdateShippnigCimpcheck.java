package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

public class UpdateShippnigCimpcheck extends BFEnty {

    public UpdateShippnigCimpcheck(String shipping_seq, String m_compcheck, String m_compcheck1) {
        setUrl("ShippingMgt/ShippingTeamMgt/updateShippingCompcheck.json");
        setParam("shipping_seq", shipping_seq, "m_compcheck", m_compcheck, "m_compcheck1", m_compcheck1);
    }
}
