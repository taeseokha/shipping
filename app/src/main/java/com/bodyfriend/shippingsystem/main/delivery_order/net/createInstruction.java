package com.bodyfriend.shippingsystem.main.delivery_order.net;


import com.bodyfriend.shippingsystem.base.BFEnty;
import com.bodyfriend.shippingsystem.main.login.Auth;

import java.util.ArrayList;

/**
 * Created by 이주영 on 2016-12-27.
 */

public class createInstruction extends BFEnty {

    public createInstruction(ArrayList<createInstructionItem> items) {
        setUrl("common/warehouse/createinstruction.json");
        setParam("deliveryManId", Auth.d.resultData.ADMIN_ID);
        setParam("deliveryManName", Auth.d.resultData.ADMIN_NM);

        ArrayList<String> shippingSeq = new ArrayList<>();
        ArrayList<String> shippingType = new ArrayList<>();
        ArrayList<String> whsSeq = new ArrayList<>();
        ArrayList<String> carNo = new ArrayList<>();
        ArrayList<String> productCode = new ArrayList<>();
        ArrayList<String> qty = new ArrayList<>();
        ArrayList<String> rmrk = new ArrayList<>();
        ArrayList<String> giftCode = new ArrayList<>();
        ArrayList<String> isReserve = new ArrayList<>();

        for (createInstructionItem item : items) {
            shippingSeq.add(item.shippingSeq);
            shippingType.add(item.shippingType);
            whsSeq.add(item.whsSeq);
            carNo.add(item.carNo);
            productCode.add(item.productCode);
            qty.add(item.qty);
            rmrk.add(item.rmrk);
            giftCode.add(item.giftCode);
            isReserve.add(item.IS_RESERVE);
        }

        setParam("shippingSeq", shippingSeq);
        setParam("shippingType", shippingType);
        setParam("whsSeq", whsSeq);
        setParam("carNo", carNo);
        setParam("productCode", productCode);
        setParam("qty", qty);
        setParam("rmrk", rmrk);
        setParam("giftCode", giftCode);
        setParam("isReserve", isReserve);

    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
        public resultData resultData;

        public class resultData {
            public String cause;
            public String hr;
            public String failDetail;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "resultCode=" + resultCode +
                    ", resultMsg='" + resultMsg + '\'' +
                    ", ResultData=" + resultData +
                    '}';
        }
    }
}

