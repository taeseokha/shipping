/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bodyfriend.shippingsystem.main.main;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.BFDialog;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.base.util.PP;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.benefit.BenefitActivity;
import com.bodyfriend.shippingsystem.main.crean_car.CleanCarActivity;
import com.bodyfriend.shippingsystem.main.delivery_order.DeliveryOrderFragment2;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.main.managers.VersionCheckManager;
import com.bodyfriend.shippingsystem.main.main.net.appInsertCarAssignHis;
import com.bodyfriend.shippingsystem.main.main.net.logout;
import com.bodyfriend.shippingsystem.main.main.net.searchCodeListDirector;
import com.bodyfriend.shippingsystem.main.map.ShippingMapView;
import com.bodyfriend.shippingsystem.main.notice.NoticeFragment;
import com.bodyfriend.shippingsystem.main.product.ProductDiscriptionFragment;
import com.bodyfriend.shippingsystem.main.refuel.RefuelActivity;
import com.bodyfriend.shippingsystem.main.refuel.RefuelListActivity;
import com.bodyfriend.shippingsystem.main.schedule.ScheduleFragment1;
import com.bodyfriend.shippingsystem.main.schedule.list.ScheduleListFragment;
import com.bodyfriend.shippingsystem.main.shipping_list.SelectCarActivity;
import com.bodyfriend.shippingsystem.main.shipping_list.ShippingListFragment2;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appCarList;
import com.bodyfriend.shippingsystem.main.shipping_list_back.ShippingBackListFragment2;
import com.bodyfriend.shippingsystem.main.shipping_list_finish.ShippingFinishListFragment;
import com.bodyfriend.shippingsystem.main.stock_list.StockListFragment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;

/**
 * @author 이주영 메인 액티비티
 */
public class MainActivity extends BFActivity {

    public interface MainActivityControll {
        void changeTitle(int resId);

        void changeFragment(Fragment fragment);

        View getView(int resId);
    }

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private View mLeftDrawer;

    private final int REQUEST_CHANGE_CAR = 1003;

    private boolean isReqFirstLogin = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        createLeftMenu();
        createBottomMenu();
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        Log.e("MainActivity::onLoadOnce");
//        checkPermissions();

        initLeftMenuEvent();

        setOnClickListener(R.id.title_layout_leftmenu, new OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer();
            }
        });

        MainFragment mainFragment = new MainFragment();
        mainFragment.setMainActivityControll(mainActivityControll);
        showFragment(mainFragment);
        findViewById(R.id.bottom_menu_home).setSelected(true);
        setOnClickListener(R.id.face_layout, onClickFaceListener);
        setOnClickListener(R.id.clean_car, onCleanCarClickListener);
    }

    /**
     * 접속 URL이 테스트이면 타이틀에 접속 URL 주소를 표기한다.
     */
    private void checkTestTitleUrl() {
        boolean isTest = !NetConst.isReal();

        try {
            if (findViewById(R.id.test_title) == null) return;
            if (findViewById(R.id.real_title) == null) return;

            TextView testTitle = (TextView) findViewById(R.id.test_title);
            View realTitle = findViewById(R.id.real_title);

            if (testTitle != null) testTitle.setVisibility(isTest ? View.VISIBLE : View.GONE);
            if (realTitle != null) realTitle.setVisibility(isTest ? View.GONE : View.VISIBLE);

            if (isTest && testTitle != null) setText(R.id.test_title, NetConst.host);
        } catch (Exception e) {
            e.printStackTrace();
        }


//        Log.d("NetConst.host -> " + NetConst.host);
//        Log.d("R.id.test_title :: getText -> " + text(R.id.test_title));
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        Log.e("MainActivity::onLoad");

        updateUserData();
        Net.async(new searchCodeListDirector(), onDirectorNetResponse);
    }


    private void initLeftMenuEvent() {
        setOnClickListener(R.id.schedule, onLeftMenuClickListener); // 배송 스케쥴
        setOnClickListener(R.id.shipping_list, onLeftMenuClickListener); // 배송 리스트
        setOnClickListener(R.id.back_list, onLeftMenuClickListener); // 배송 회수리스트
        setOnClickListener(R.id.complete_list, onLeftMenuClickListener); // 배송 완료리스트
        setOnClickListener(R.id.products, onLeftMenuClickListener); // 제품설명
        setOnClickListener(R.id.notice, onLeftMenuClickListener); // 공지사항
        setOnClickListener(R.id.stock_list, onLeftMenuClickListener); // 공지사항
        setOnClickListener(R.id.instruction_list, onLeftMenuClickListener); // 공지사항
        setOnClickListener(R.id.questionnaire, onLeftMenuClickListener); // 공지사항
        setOnClickListener(R.id.benefitMenu, onLeftMenuClickListener); // 공지사항
        setOnClickListener(R.id.refuelMenu, onLeftMenuClickListener); // 공지사항
        setOnClickListener(R.id.refuelListMenu, onLeftMenuClickListener); // 공지사항
        setOnClickListener(R.id.logout, new OnClickListener() {
            @Override
            public void onClick(View v) {
                PP.MSG_ID.set("");
                PP.IS_KAKAO.set(false);
                logout();
            }
        }); // 로그아웃
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @SuppressLint("CommitTransaction")
    private boolean checkBack() {
        Log.d("checkBack ~~~~~~~~~~~~~");
        if (mLeftDrawer.isShown()) {

            mDrawerLayout.closeDrawer(mLeftDrawer);
            return true;
        }

        if (mLastFragment instanceof ScheduleListFragment) {
            ScheduleFragment1 scheduleFragment = new ScheduleFragment1();
            scheduleFragment.setMainActivityControll(mainActivityControll);
            mainActivityControll.changeFragment(scheduleFragment);
            return true;
        }
//        if (mLastFragment instanceof ShippingFinishListFragment) {
//            ShippingFinishListFragment scheduleFragment = new ShippingFinishListFragment();
//            scheduleFragment.setMainActivityControll(mainActivityControll);
//            mainActivityControll.changeFragment(scheduleFragment);
//            return true;
//        }

        if (!(mLastFragment instanceof MainFragment)) {
            MainFragment mainFragment = new MainFragment();
            mainFragment.setMainActivityControll(mainActivityControll);
            mainActivityControll.changeFragment(mainFragment);
            return true;
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        if (!checkBack()) {
            super.onBackPressed();
        }
    }

    private DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {

        private Net.OnNetResponse<logout> onLogoutNetListener = new Net.OnNetResponse<logout>() {

            @Override
            public void onResponse(logout response) {

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }
        };

        @Override
        public void onClick(DialogInterface dialog, int which) {
            mDrawerLayout.closeDrawer(mLeftDrawer);
            Net.async(new logout(), onLogoutNetListener);
            Auth.logout();
            Auth.startLogin(mContext);
        }
    };

    private void logout() {
        showDialog("로그아웃 하시겠습니까?", "확인", positiveListener, "취소", null);
    }


    /**
     * 화면 기사명과 소속회사를 업데이트한다.<br>
     * 갤럭시s4에서 로그인시 옵저버를 통해서 하는 업데이트가 정상적으로 되지 않는다고 한다.
     */
    private void updateUserData() {
        try {
//            Log.d("Auth.d.ResultData.ADMIN_NM : " + Auth.d.ResultData.ADMIN_NM);
//            Log.d("Auth.d.ResultData.CAR_NUMBER : " + Auth.d.ResultData.CAR_NUMBER);
//            Log.d("Auth.d.ResultData.COMPANY_NM : " + Auth.d.ResultData.COMPANY_NM);
            setText(R.id.admin_nm, Auth.d.resultData.ADMIN_NM);
            setText(R.id.car_nm, PP.carNumber.get());
            setText(R.id.company_nm, String.format("[%s]", Auth.d.resultData.COMPANY_NM));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Net.OnNetResponse<searchCodeListDirector> onDirectorNetResponse = new Net.OnNetResponse<searchCodeListDirector>() {

        @Override
        public void onResponse(searchCodeListDirector response) {

            List<searchCodeListDirector.Data.resultData> datas = null;
            Log.d("onDirectorNetResponse data.DET_CD_NM: ");
            try {
                datas = response.data.resultData;
            } catch (NullPointerException e) {
                e.printStackTrace();
            }


            searchCodeListDirector.Data.resultData data;
            int size = datas.size();
            size = size > 3 ? 3 : size;
            int resId = 0;
            TextView det_cd_nm, comm_cd_nm, description;
            View view;
            for (int i = 0; i < size; i++) {
                data = datas.get(i);
                if (i == 0) {
                    resId = R.id.item_1;
                } else if (i == 1) {
                    resId = R.id.item_2;
                } else if (i == 2) {
                    resId = R.id.item_3;
                }
                view = findViewById(resId);
                view.setVisibility(View.VISIBLE);

                view.setTag(data);
                view.setOnClickListener(onClickDirectorListener); // 담당자 리스너
                det_cd_nm = (TextView) view.findViewById(R.id.det_cd_nm);
                comm_cd_nm = (TextView) view.findViewById(R.id.comm_cd_nm);
                description = (TextView) view.findViewById(R.id.description);

                det_cd_nm.setText(data.DET_CD_NM);
                comm_cd_nm.setText(String.format("[%s]", data.COMM_CD_NM));
                description.setText(data.DESCRIPTION);
//                Log.d("data.DET_CD_NM: " + data.DET_CD_NM);
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }

        public OnClickListener onClickDirectorListener = v -> {
            final searchCodeListDirector.Data.resultData data = (searchCodeListDirector.Data.resultData) v.getTag();
            if (data == null) return;

            showDialog(String.format("%s 님과 통화시겠습니까?", data.DET_CD_NM), "통화", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(String.format("tel:%s", data.DESCRIPTION)));
                    mContext.startActivity(intent);
                }
            }, "취소", null);
        };
    };

    private OnClickListener onLeftMenuClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final int id = v.getId();
            Fragment fragment = null;
            String tag = null;

            switch (id) {
                case R.id.schedule:
                    fragment = new ScheduleFragment1();
                    ((ScheduleFragment1) fragment).setMainActivityControll(mainActivityControll);
                    break;
                case R.id.shipping_list:
                    fragment = new ShippingListFragment2();
                    ((ShippingListFragment2) fragment).setMainActivityControll(mainActivityControll);
                    break;
                case R.id.back_list:
                    fragment = new ShippingBackListFragment2();
                    ((ShippingBackListFragment2) fragment).setMainActivityControll(mainActivityControll);
                    break;
                case R.id.complete_list:
                    fragment = new ShippingFinishListFragment();
                    ((ShippingFinishListFragment) fragment).setMainActivityControll(mainActivityControll);
                    break;
                case R.id.products:
                    fragment = new ProductDiscriptionFragment();
                    ((ProductDiscriptionFragment) fragment).setMainActivityControll(mainActivityControll);
                    break;
                case R.id.notice:
                    fragment = new NoticeFragment();
                    ((NoticeFragment) fragment).setMainActivityControll(mainActivityControll);
                    break;
                case R.id.stock_list:
                    fragment = new StockListFragment();
                    ((StockListFragment) fragment).setMainActivityControll(mainActivityControll);
                    break;
                case R.id.instruction_list:
                    fragment = new DeliveryOrderFragment2();
                    ((DeliveryOrderFragment2) fragment).setMainActivityControll(mainActivityControll);
                    /*fragment = new DeliveryOrderFragment();
                    ((DeliveryOrderFragment) fragment).setMainActivityControll(mainActivityControll);*/
                    break;

                case R.id.questionnaire:
                    Uri uri = Uri.parse("https://goo.gl/forms/5RUZzqlEe2JL1RZ73");
                    Intent it = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(it);
                    break;
                case R.id.benefitMenu:
                    Intent intent = new Intent(MainActivity.this, BenefitActivity.class);
                    startActivity(intent);
                    break;
                case R.id.refuelMenu:
                    Intent intent2 = new Intent(MainActivity.this, RefuelActivity.class);
                    startActivity(intent2);
                    break;
                case R.id.refuelListMenu:
                    Intent intent3 = new Intent(MainActivity.this, RefuelListActivity.class);
                    startActivity(intent3);
                    break;
            }

            if (fragment != null) {
                showFragment(fragment);
                closeDrawer();
            }
        }
    };

    /**
     * 좌측 메뉴 생성
     */
    private void createLeftMenu() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mLeftDrawer = findViewById(R.id.left_drawer);
        mLeftDrawer.setOnClickListener(onLeftMenuClickListener);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_launcher, R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private ArrayList<View> bottomMenus;

    /**
     * 하단 메뉴 생성
     */
    private void createBottomMenu() {
        bottomMenus = new ArrayList<>();
        bottomMenus.add(findViewById(R.id.bottom_menu_notice));
        bottomMenus.add(findViewById(R.id.bottom_menu_done_list));
        bottomMenus.add(findViewById(R.id.bottom_menu_home));
        bottomMenus.add(findViewById(R.id.bottom_menu_shipping_list));

        bottomMenus.add(findViewById(R.id.bottom_menu_delivery_order));

        for (View view : bottomMenus) {
            view.setOnClickListener(onClickBottomListener);
        }
    }

    private DeliveryOrderFragment2 mDeliveryOrderFragment;
    private OnClickListener onClickBottomListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.bottom_menu_notice: // 공지사항
                    NoticeFragment noticeFragment = new NoticeFragment();
                    noticeFragment.setMainActivityControll(mainActivityControll);
                    showFragment(noticeFragment);
                    break;
                case R.id.bottom_menu_done_list: // 완료리스트
                    Log.d("bottom_menu_done_list~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    ShippingFinishListFragment shippingFinishListFragment = new ShippingFinishListFragment();
                    shippingFinishListFragment.setMainActivityControll(mainActivityControll);
                    showFragment(shippingFinishListFragment);
                    break;
                case R.id.bottom_menu_home:
                    MainFragment mainFragment = new MainFragment();
                    mainFragment.setMainActivityControll(mainActivityControll);
                    showFragment(mainFragment);
                    break;
                case R.id.bottom_menu_shipping_list: // 배송리스트
                    ShippingListFragment2 shippingListFragment2 = new ShippingListFragment2();
                    shippingListFragment2.setMainActivityControll(mainActivityControll);
                    showFragment(shippingListFragment2);
                    break;
                case R.id.bottom_menu_delivery_order: // 출고지시서
                    mDeliveryOrderFragment = new DeliveryOrderFragment2();
                    mDeliveryOrderFragment.setMainActivityControll(mainActivityControll);
                    showFragment(mDeliveryOrderFragment);
                    break;
            }
        }

    };

    private void unSelectBottomMenu() {
        for (View view : bottomMenus) {
            if (view.isSelected()) view.setSelected(false);
        }
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(mLeftDrawer);
    }

    private void closeDrawer() {
        mDrawerLayout.closeDrawer(mLeftDrawer);
    }

    private Fragment mLastFragment;

    protected void showFragment(Fragment fragment) {
        mLastFragment = fragment;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commitAllowingStateLoss();

        if (fragment instanceof ShippingListFragment2) {
            unSelectBottomMenu();
            findViewById(R.id.bottom_menu_shipping_list).setSelected(true);
        } else if (fragment instanceof ShippingFinishListFragment) {

            Log.d("bottom_menu_done_list~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//            ShippingFinishListFragment shippingFinishListFragment = new ShippingFinishListFragment();
//            shippingFinishListFragment.setMainActivityControll(mainActivityControll);
//            showFragment(shippingFinishListFragment);
            unSelectBottomMenu();
            findViewById(R.id.bottom_menu_done_list).setSelected(true);
        } else if (fragment instanceof MainFragment) {
            unSelectBottomMenu();
            findViewById(R.id.bottom_menu_home).setSelected(true);
        } else if (fragment instanceof NoticeFragment) { // 공지
            unSelectBottomMenu();
            findViewById(R.id.bottom_menu_notice).setSelected(true);
        } else if (fragment instanceof DeliveryOrderFragment2) { // 출고지시서
            unSelectBottomMenu();
            findViewById(R.id.bottom_menu_delivery_order).setSelected(true);
        }
    }

    private MainActivityControll mainActivityControll = new MainActivityControll() {
        @Override
        public void changeTitle(int resId) {
            FrameLayout titleLayout = (FrameLayout) findViewById(R.id.title_layout);
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            titleLayout.removeAllViews();
            titleLayout.addView(layoutInflater.inflate(resId, null));

            View leftMenu = findViewById(R.id.title_layout_leftmenu);
            if (leftMenu != null) {
                leftMenu.setOnClickListener(v -> openDrawer());
            }

            View search = findViewById(R.id.title_layout_search);
            if (search != null) {
                search.setOnClickListener(v -> OH.c().notifyObservers(OH.TYPE.SEARCH));
            }


            Switch showProductListSw = (Switch) findViewById(R.id.showProductListSw);
            if (showProductListSw != null) {
                showProductListSw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        OH.TYPE oh = OH.TYPE.SHOW_PRODUCT_LIST;
                        oh.obj = isChecked;
                        OH.c().notifyObservers(oh);
                    }
                });
            }


            View map = findViewById(R.id.title_layout_map);
            if (map != null) {
                map.setOnClickListener(v -> startActivity(new Intent(mContext, ShippingMapView.class)));
            }

            checkTestTitleUrl();

        }

        @Override
        public void changeFragment(Fragment fragment) {
            showFragment(fragment);
        }

        @Override
        public View getView(int resId) {
            return findViewById(resId);
        }
    };

    public MainActivityControll getMainActivityControll() {
        return mainActivityControll;
    }


    private OnClickListener onClickFaceListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            MainFragment mainFragment = new MainFragment();
            mainFragment.setMainActivityControll(mainActivityControll);
            showFragment(mainFragment);
            closeDrawer();
        }
    };

    @Override
    public void update(Observable observable, Object data) {
        super.update(observable, data);

        if (OH.c() != observable)
            return;

        if (data instanceof OH.TYPE) {
            OH.TYPE type = (OH.TYPE) data;
            switch (type) {
                case EXCEPTION:
                    toast(type.obj);
                    break;
                case SESSION_OUT:
                    Auth.startLogin(mContext);
                    break;
                case LOGIN:
                    VersionCheckManager.newInstance().reqVersionCheck(MainActivity.this);
                    break;
                default:
                    break;
            }
        }
    }

    private OnClickListener onCleanCarClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                if (PP.carNumber.get().isEmpty()) {
                    BFDialog.newInstance(mContext).showSimpleDialog("등록된 차량번호가 없습니다.\n관리자에게 문의하시기 바랍니다.");
                } else {
                    startActivity(new Intent(mContext, CleanCarActivity.class));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        // 신규 업데이트해야할 버전이 있는지 확인한다.
        VersionCheckManager.newInstance().reqVersionCheck(MainActivity.this);
        Log.d("MainActivity onResume ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        // 차량 등록 팝업을 띄울 유무를 체크한다.
        appIsFirstLogin();
    }

    /**
     * 메인화면에 앱이 들어왔을때 체크하며,
     * 결과값이 Y이면
     * 차량 등록 팝업을 띄운다.
     */
    private void appIsFirstLogin() {
        Log.d("MainActivity LOGIN_DATE : " + PP.LOGIN_DATE.getString());
        Log.e("loginComplete " + SDF.yyyymmdd_1.format(new Date()));


        if (PP.LOGIN_DATE.getString().equals(SDF.yyyymmdd_1.format(new Date()))) {
            isReqFirstLogin = false;
            return;
        } else if (Auth.isLogin()) {
            PP.LOGIN_DATE.set(SDF.yyyymmdd_1.format(new Date()));

            Intent intent = new Intent(mContext, SelectCarActivity.class);
            startActivityForResult(intent, REQUEST_CHANGE_CAR);
        }
        isReqFirstLogin = true;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onActivityResult requestCode : " + requestCode);
        Log.d("onActivityResult resultCode : " + resultCode);
        if (requestCode == REQUEST_CHANGE_CAR) {
            // appIsFirstLogin 메소드의 락을 푼다.
            if (resultCode == MainActivity.RESULT_OK) {
                appCarList.Data.resultData resultData = data.getParcelableExtra(SelectCarActivity.EXTRA.APPCARLIST_DATA_RESULTDATA);

                Log.d("ResultData : " + resultData.toString());
                appInsertCarAssignHis(resultData.CAR_SEQ, resultData.CAR_NUMBER);
                PP.carNumber.set(resultData.CAR_NUMBER);
            } else {
                // 종료
                exit();
                isReqFirstLogin = false;
            }
        } /*else if (requestCode == DeliveryOrderFragment.ADD_PRODUCT_ITEM) {
            ArrayList<getProductList.Data.ResultData> product = data.getParcelableArrayListExtra("Product");
//            mDeliveryOrderFragment.setFactoryInstructions(product);
            Log.d("product ~~~: " + product.size());
            Log.d("product ~~~: " + product.get(0).productName);

        }*/
    }

    /**
     * 선택된 배차차량 송신
     */
    private void appInsertCarAssignHis(String car_seq, String car_number) {
        Log.d("MainActivity : appInsertCarAssignHis");
        Log.d("car_seq -> " + car_seq);
        Log.d("car_number -> " + car_number);
        if (car_seq == null) {
            isReqFirstLogin = false;
            return;
        }
        if (car_number == null) {
            isReqFirstLogin = false;
            return;
        }
        if (car_seq.isEmpty()) {
            isReqFirstLogin = false;
            return;
        }
        if (car_number.isEmpty()) {
            isReqFirstLogin = false;
            return;
        }

        Net.async(new appInsertCarAssignHis(car_seq, car_number)).setOnNetResponse(new Net.OnNetResponse<appInsertCarAssignHis>() {
            @Override
            public void onErrorResponse(VolleyError error) {
                isReqFirstLogin = false;
            }

            @Override
            public void onResponse(appInsertCarAssignHis response) {
                BFDialog.newInstance(mContext).showSimpleDialog(response.data.resultMsg);
                isReqFirstLogin = false;
            }
        });
    }
}