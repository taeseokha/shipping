package com.bodyfriend.shippingsystem.main.notice;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFFragment2;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.image.AUIL;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.main.main.MainActivity;
import com.bodyfriend.shippingsystem.main.main.net.NoticeLog;
import com.bodyfriend.shippingsystem.main.main.net.appNoticeList;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 이주영 on 2016-08-01.
 */
public class NoticeFragment extends BFFragment2 {
    protected ListView mList;
    private NoticeListAdapter mListAdapter;
    private MainActivity.MainActivityControll mainActivityControll;

    public void setMainActivityControll(MainActivity.MainActivityControll mainActivityControll) {
        this.mainActivityControll = mainActivityControll;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notice, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        mainActivityControll.changeTitle(R.layout.title_layout_notice);

        mList = (ListView) getView().findViewById(R.id.list_view);
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        Net.async(new appNoticeList()).setOnNetResponse(onNetResponse);

    }

    private Net.OnNetResponse<appNoticeList> onNetResponse = new Net.OnNetResponse<appNoticeList>() {

        @Override
        public void onResponse(appNoticeList response) {
            List<appNoticeList.Data.resultData.list> list = response.data.resultData.list;
            Collections.sort(list, (o1, o2) -> o2.RG_DT.compareTo(o1.RG_DT));
            if (mListAdapter == null) {
                mListAdapter = new NoticeListAdapter(getContext(), list);
                mList.setAdapter(mListAdapter);

            } else {
                mListAdapter.setItemList(list);
                mListAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    public class NoticeListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private ArrayList<appNoticeList.Data.resultData.list> mItemList;
        @SuppressLint("UseSparseArrays")
        private HashMap<Integer, View> viewHashMap = new HashMap<>();

        public void setItemList(List<appNoticeList.Data.resultData.list> list) {
            this.mItemList = (ArrayList<appNoticeList.Data.resultData.list>) list;
        }

        public NoticeListAdapter(Context context, List<appNoticeList.Data.resultData.list> list) {
            super();
            this.mItemList = (ArrayList<appNoticeList.Data.resultData.list>) list;
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            int cnt = mItemList.size();
            return cnt;
        }

        @Override
        public Object getItem(int position) {
            return mItemList.get(position);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;
            final appNoticeList.Data.resultData.list item = mItemList.get(position);
            // 캐시된 뷰가 없을 경우 새로 생성하고 뷰홀더를 생성한다
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.notice_list_item2, parent, false);

                viewHolder = new ViewHolder(convertView, item);
            }
            // 캐시된 뷰가 있을 경우 저장된 뷰홀더를 사용한다
            else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.text1.setText(item.NOTICE_TITLE);
            viewHolder.text2.setText(item.RG_DT);
            viewHolder.text3.setText(item.NOTICE_CONTENTS);
            if (!TextUtils.isEmpty(item.PICTURE_ONE)) {
                Log.d("item.PICTURE_ONE : " + item.PICTURE_ONE);
                Log.d("item.PICTURE_ONE : " + NetConst.host + item.PICTURE_ONE.replace("/svc/", ""));
//                ImageLoader.getInstance().displayImage(NetConst.host + item.PICTURE_ONE.replace("\\/svc", ""),
//                        viewHolder.noticImage, AUIL.options);
                setImage(viewHolder.noticImage, NetConst.host + item.PICTURE_ONE.replace("/svc/", ""));
            }

            if (item.NOTICE_NEW) {
                Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ico_notice_n, null);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                viewHolder.text1.setCompoundDrawables(null, null, drawable, null);
            } else {
                viewHolder.text1.setCompoundDrawables(null, null, null, null);
            }
            viewHashMap.put(position, convertView);

            return convertView;
        }

        public class ViewHolder {
            private final ImageView noticImage;
            public TextView text1; // 제목
            public TextView text2; // 등록일자
            public TextView text3; // 등록일자
            private appNoticeList.Data.resultData.list mItem;

            public ViewHolder(View convertView, final appNoticeList.Data.resultData.list item) {
                mItem = item;
                text1 = (TextView) convertView.findViewById(R.id.text1);
                text2 = (TextView) convertView.findViewById(R.id.text2);
                text3 = (TextView) convertView.findViewById(R.id.text3);
                noticImage = (ImageView) convertView.findViewById(R.id.imageViewNotic);

                convertView.setTag(this);

                convertView.setOnClickListener(onClickListener);
                noticImage.setOnClickListener(onLinkImage);
            }

            View.OnClickListener onLinkImage = new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    String url = NetConst.host + mItem.PICTURE_ONE.replace("/svc/", "");
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);

                }
            };

            private View.OnClickListener onClickListener = new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    ViewHolder holder = (ViewHolder) v.getTag();
                    boolean isVisible = holder.text3.getVisibility() == View.VISIBLE;
                    if (!isVisible) {
                        for (int i : viewHashMap.keySet()) {
                            ViewHolder holder1 = (ViewHolder) viewHashMap.get(i).getTag();
                            holder1.text3.setVisibility(View.GONE);
                            holder1.noticImage.setVisibility(View.GONE);
                            viewHashMap.get(i).findViewById(R.id.arrow).setRotation(0);
                            text1.setCompoundDrawables(null, null, null, null);
                        }
                    }
                    holder.text3.setVisibility(isVisible ? View.GONE : View.VISIBLE);
                    holder.noticImage.setVisibility(isVisible ? View.GONE : View.VISIBLE);
                    v.findViewById(R.id.arrow).setRotation(isVisible ? 0 : 180);
                    Net.async(new NoticeLog(mItem.NOTICE_SEQ)).setOnNetResponse(new Net.OnNetResponse<NoticeLog>() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }

                        @Override
                        public void onResponse(NoticeLog response) {
//                            Log.d("response.data.resultMsg : " + response.data.resultMsg);
                            holder.text1.setCompoundDrawables(null, null, null, null);
                        }
                    });
                }
            };
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }
}
