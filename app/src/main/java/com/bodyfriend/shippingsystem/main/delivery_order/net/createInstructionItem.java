package com.bodyfriend.shippingsystem.main.delivery_order.net;

/**
 * Created by 이주영 on 2016-12-27.
 */

public class createInstructionItem {
    public String shippingSeq; // 배송Seq
    public String shippingType;// 배송구분 (고객배송, 맞교체, 전시장설치 외) *
    public String whsSeq;// 창고번호 *
    public String carNo;// 차량번호 *
    public String productCode;// 제품코드(ERP)
    public String qty;// 제품수량
    public String rmrk;// 비고 (사은품)
    public String giftCode;// 사은품코드
    public String instructionKey; // 지시서 번호
    public String PRODUCT_TYPE; // 프로덕트 타입
    public String IS_RESERVE; // 여유 수량 신청 여부
}
