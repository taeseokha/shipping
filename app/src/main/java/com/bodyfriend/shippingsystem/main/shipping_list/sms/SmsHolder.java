package com.bodyfriend.shippingsystem.main.shipping_list.sms;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bodyfriend.shippingsystem.R;

/**
 * Created by 이주영 on 2017-02-27.
 */
public class SmsHolder {
    private final String TAG = SmsHolder.class.getSimpleName();
    private SmsModel smsModel;

    private View contentView;

    private TextView phoneNum;
    private TextView message;
    private TextView userName;

    public SmsHolder(View contentView) {
        this.contentView = contentView;

        phoneNum = (TextView) contentView.findViewById(R.id.phone_num);
        message = (TextView) contentView.findViewById(R.id.message);
        message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (smsModel != null) {
                    smsModel.MSG = message.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        userName = (TextView) contentView.findViewById(R.id.user_name);
    }

    public void setupData(SmsModel smsData) {
        Log.d(TAG, ":: setupData smsData -> " + smsData.toString());

        this.smsModel = smsData;

        phoneNum.setText(smsData.HPHONE_NO);
        message.setText(smsData.MSG);
        userName.setText(smsData.CUST_NAME);
    }
}
