package com.bodyfriend.shippingsystem.main.shipping_list.sms;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.main.shipping_list.db.SmsDBHelper;

import java.util.ArrayList;

/**
 * Created by 이주영 on 2016-09-01.
 */
public class ShippingAlarmReceiver extends BroadcastReceiver {

    private SmsDBHelper mSmsDBHelper;

    @Override
    public void onReceive(Context context, Intent intent) {
        mSmsDBHelper = new SmsDBHelper(context);

        Log.d(":: onReceive");
        boolean isAllMessage = false;
        if (intent.getAction() != null) {
            isAllMessage = intent.getAction().equals("IS_ALL_MESSAGE_ALARM");
        }
        Log.d("isAllMessage => " + isAllMessage);
        if (isAllMessage) {
            regAllMessageNotification(context, intent);
        } else {
            regNotification(context, intent);
        }

        acquireCpuWakeLock(context);
    }

    /**
     * 오늘 보내야 모든 메세지를 보내는 노티피케이션을 등록한다.
     */
    private void regAllMessageNotification(Context context, Intent intent) {
        Log.d(":: regAllMessageNotification");
        Bundle bundle = intent.getExtras();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        intent = new Intent(context, ShippingTodayNotificationReceiver.class);
        intent.putExtras(bundle);
        intent.putExtra("IS_ALL_MESSAGE_ALARM", true);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setTicker("바디프랜드(배송앱)");
        builder.setContentTitle("방문 당일 문자 발송(ALL)");
        builder.setWhen(System.currentTimeMillis());
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);
        builder.setContentText("방문 당일 문자 발송(ALL)");

        if (Build.VERSION.SDK_INT < 16) {
            notificationManager.notify(9999, builder.getNotification());
        } else {
            notificationManager.notify(9999, builder.build());
        }

    }

    private void acquireCpuWakeLock(Context context) {
        PowerManager.WakeLock cpuWakeLock;

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        cpuWakeLock = pm.newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                        PowerManager.ACQUIRE_CAUSES_WAKEUP |
                        PowerManager.ON_AFTER_RELEASE, "hi");

        cpuWakeLock.acquire();

        if (cpuWakeLock != null) {
            cpuWakeLock.release();
        }
    }


    /**
     * 노티 등록
     */
    private void regNotification(Context context, Intent intent) {
        String action = intent.getAction();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        intent = new Intent(context, ShippingNotificationReceiver.class);
        intent.setAction(action);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher));
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setTicker("바디프랜드(배송앱)");
        builder.setContentTitle("방문 당일 문자 발송");
        builder.setWhen(System.currentTimeMillis());
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);

        try {
            ArrayList<SmsModel> smsModels = mSmsDBHelper.select(Integer.parseInt(action));
            builder.setContentText(smsModels.get(0).MSG);
            String tmp = smsModels.get(0).HPHONE_NO.replaceAll("-", "");
            tmp = tmp.replaceAll("0", "");
            int id = Integer.parseInt(tmp);

            if (Build.VERSION.SDK_INT < 16) {
                notificationManager.notify(id, builder.getNotification());
            } else {
                notificationManager.notify(id, builder.build());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
