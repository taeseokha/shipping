package com.bodyfriend.shippingsystem.main.shipping_list_finish.detail;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.TextUtil;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.shipping_list_finish.ZoomActivity;
import com.bodyfriend.shippingsystem.main.shipping_list.ShippingMasterDetailActivity;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingMasterDetail;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeAdapter;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeJoli;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListArea;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListDeliveryGb;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListFreegift;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListProductBonsa;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListProductGb;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListProductName;
import com.bodyfriend.shippingsystem.main.shipping_list.net.selectDeliveryManList;

/**
 * Created by 이주영 on 2016-08-05.
 */
public class ShippingDoneMasterDetailActivity extends ShippingMasterDetailActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shipping_done_master_detail2);
    }

    @Override
    protected void onLoadOnce() {
        setFinish(R.id.title_layout_leftmenu);
        setText(R.id.title, "배송완료");

        setOnClickListener(R.id.btn1, onBtn1ClickListener);
        setOnClickListener(R.id.btn2, onBtn2ClickListener);

        findViewById(R.id.btn1).performClick();

        setOnClickListener(R.id.installData, onInstallDataClickListener);
    }

    @Override
    protected void onLoad() {
        showProgress();
        Net.async(new ShippingMasterDetail(shipping_seq)).setOnNetResponse(onShippingDetailNetResponse);
    }

    @Override
    protected void onUpdateUI() {
    }

    private String mSignDataImage;

    protected Net.OnNetResponse<ShippingMasterDetail> onShippingDetailNetResponse = new Net.OnNetResponse<ShippingMasterDetail>() {

        @Override
        public void onResponse(ShippingMasterDetail response) {
            hideProgress();

            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);

                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            ShippingMasterDetail.Data.resultData r = mResultData = response.data.resultData;

            Log.d("r : " + r.toString());
            String path_sign[] = r.imagesign.split("/");
            path_sign[path_sign.length - 1] = String.format("confirmation_%s_%s.jpg", r.PRODUCT_TYPE, r.SHIPPING_SEQ);
            mSignDataImage = "";
            for (String str : path_sign) {
                if (!str.isEmpty())
                    mSignDataImage = mSignDataImage + "/" + str;
            }

            setText(R.id.body_num, r.BODY_NO);
            setText(R.id.receive_date, r.RECEIVE_DATE);
            setText(R.id.cust_name, r.CUST_NAME);
            // setText(R.id.deliveryman1, r.DELIVERYMAN1);
            setText(R.id.m_shippingps, r.SHIPPING_PS);
            setText(R.id.m_productsn, r.PRODUCT_SN);

            setText(R.id.freegift_one, r.FREEGIFT_ONE);
            setText(R.id.hphone_no, r.HPHONE_NO);
            setText(R.id.insaddr, r.INSADDR);
            setText(R.id.product_name, r.PRODUCT_NAME);
            setText(R.id.purchasing_office, r.PURCHASING_OFFICE);
            setText(R.id.qty, r.QTY);
            setText(R.id.receive_date, r.RECEIVE_DATE);
            setText(R.id.service_month, r.SERVICE_MONTH);
            setText(R.id.tel_no, r.TEL_NO);


            if (!TextUtils.isEmpty(r.imageone.trim())) {
                setImage(R.id.imageone, NetConst.host + r.imageone.substring(1));
            }
            if (!TextUtils.isEmpty(r.imagetwo.trim())) {
                setImage(R.id.imagetwo, NetConst.host + r.imagetwo.substring(1));
            }
            if (!TextUtils.isEmpty(r.imagethree.trim())) {
                setImage(R.id.imagethree, NetConst.host + r.imagethree.substring(1));
            }
            if (!TextUtils.isEmpty(r.imagefour.trim())) {
                setImage(R.id.imagefour, NetConst.host + r.imagefour.substring(1));
            }
            if (!TextUtils.isEmpty(r.imagefive.trim())) {
                setImage(R.id.imagefive, NetConst.host + r.imagefive.substring(1));
            }
            if (!TextUtils.isEmpty(r.imagesix.trim())) {
                setImage(R.id.imagesix, NetConst.host + r.imagesix.substring(1));
            }

            if (r.INS_COMPLETE_NM != null) {
                setText(R.id.ins_complete_nm, r.INS_COMPLETE_NM);
            }
            Log.d("r.PROGRESS_NO_NM : " + r.PROGRESS_NO_NM);
            if (!TextUtils.isEmpty(r.PROGRESS_NO_NM)) {

                findViewById(R.id.form_회수_설치_2).setVisibility(View.VISIBLE);
                setText(R.id.agree_name, r.AGREE_NAME);

                setText(R.id.insaddr2, r.INSADDR2); // 설치주소
                setText(R.id.insaddr2, r.INSADDR2); // 설치주소
                setText(R.id.tel_no2, r.TEL_NO); // 설치자 전화번호
                setText(R.id.hphone_no2, r.HPHONE_NO); // 설치자 핸드폰번호
            }


            //양중
            if (!TextUtils.isEmpty(r.socket_image_one)) {
                if (!TextUtils.isEmpty(r.socket_image_one.trim())) {
                    setImage(R.id.stairsOne, NetConst.host + r.socket_image_one.substring(1));
                }
                if (!TextUtils.isEmpty(r.socket_image_two.trim())) {
                    setImage(R.id.stairsTwo, NetConst.host + r.socket_image_two.substring(1));
                }
                if (!TextUtils.isEmpty(r.socket_image_three.trim())) {
                    setImage(R.id.stairsThree, NetConst.host + r.socket_image_three.substring(1));
                }
            } else {
                setVisibility(R.id.stairsLayout, false);
            }
            // 내림서비스
            if (!TextUtils.isEmpty(r.service_image_one)) {
                if (!TextUtils.isEmpty(r.service_image_one.trim())) {
                    setImage(R.id.disembarkmentOne, NetConst.host + r.service_image_one.substring(1));
                }

                if (!TextUtils.isEmpty(r.service_image_two.trim())) {
                    setImage(R.id.disembarkmentTwo, NetConst.host + r.service_image_two.substring(1));
                }
            } else {
                setVisibility(R.id.disembarkmentLayout, false);
            }
            Log.d("ladder_image_one : " + r.ladder_image_one);
            //사다리차
            if (!TextUtils.isEmpty(r.ladder_image_one)) {
                if (!TextUtils.isEmpty(r.ladder_image_one.trim())) {
                    setImage(R.id.ladderOne, NetConst.host + r.ladder_image_one.substring(1));
                }
                if (!TextUtils.isEmpty(r.ladder_image_two.trim())) {
                    setImage(R.id.ladderTwo, NetConst.host + r.ladder_image_two.substring(1));
                }
                if (!TextUtils.isEmpty(r.ladder_image_three.trim())) {
                    setImage(R.id.ladderThree, NetConst.host + r.ladder_image_three.substring(1));
                }
            } else {
                setVisibility(R.id.ladderLayout, false);
            }


            setText(R.id.installationDate, r.IN_DATE);

            // if (r.INS_COMPLETE != null)
            // setChecked(r.INS_COMPLETE.equals("Y") ? R.id.m_completedtY : R.id.m_completedtN, true);
            if (r.PROMISE != null) {
                if (r.PROMISE.equals("Y")) {
                    setChecked(R.id.promiseY, true);
                } else if (r.PROMISE.equals("N")) {
                    setChecked(R.id.promiseN, true);
                } else {

                }
            }

            Net.async(new searchCodeListDeliveryGb()).setOnNetResponse(onDeliveryGbNetResponse);
            Net.async(new searchCodeListArea()).setOnNetResponse(onLocationGbNetResponse);
            Net.async(new searchCodeListProductBonsa()).setOnNetResponse(onBonsaGbNetResponse);
            Net.async(new searchCodeListProductGb()).setOnNetResponse(onProductGbNetResponse);
            Net.async(new searchCodeListProductName()).setOnNetResponse(onProductNameNetResponse);
            Net.async(new selectDeliveryManList(r.DIV_STATUS)).setOnNetResponse(onManNetResponse);
            Net.async(new searchCodeListFreegift()).setOnNetResponse(onFreegiftNetResponse);

            if (NetConst.PRODUCT_TYPE_W.equals(NetConst.s_producttype)) { // 정수기이면
                Net.async(new searchCodeAdapter()).setOnNetResponse(onAdapterNetResponse);
                Net.async(new searchCodeJoli()).setOnNetResponse(onJoliNetResponse);
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    private View.OnClickListener onBtn1ClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            setVisibility(R.id.shipping_done_master_detail_body1, true);
            setVisibility(R.id.shipping_done_master_detail_body2, false);
            setSelected(R.id.btn1, true);
            setSelected(R.id.btn2, false);
            setTextColor(R.id.btn1, getResources().getColor(R.color.white));
            setTextColor(R.id.btn2, Color.parseColor("#393a3a"));
        }
    };
    private View.OnClickListener onBtn2ClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            setVisibility(R.id.shipping_done_master_detail_body1, false);
            setVisibility(R.id.shipping_done_master_detail_body2, true);
            setSelected(R.id.btn1, false);
            setSelected(R.id.btn2, true);
            setTextColor(R.id.btn1, Color.parseColor("#393a3a"));
            setTextColor(R.id.btn2, getResources().getColor(R.color.white));
        }
    };

    private Net.OnNetResponse<searchCodeListFreegift> onFreegiftNetResponse = new Net.OnNetResponse<searchCodeListFreegift>() {

        @Override
        public void onResponse(searchCodeListFreegift response) {

            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            for (searchCodeListFreegift.Data.resultData d : response.data.resultData) {
                if (mResultData.FREEGIFT_CHECK_ONE.equals(d.DET_CD)) {
                    setText(R.id.m_freegiftcheck1, d.DET_CD_NM);
                }
                if (mResultData.FREEGIFT_CHECK_TWO.equals(d.DET_CD)) {
                    setText(R.id.m_freegiftcheck2, d.DET_CD_NM);
                }
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {

        }
    };

    private View.OnClickListener onInstallDataClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, ZoomActivity.class);
            intent.putExtra(ZoomActivity.EXTRA.IMAGE_URI, NetConst.host + mSignDataImage);
            startActivity(intent);
        }
    };

    private Net.OnNetResponse<searchCodeAdapter> onAdapterNetResponse = new Net.OnNetResponse<searchCodeAdapter>() {

        @Override
        public void onResponse(searchCodeAdapter response) {
            hideProgress();

            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            setVisibility(R.id.adapter_layout, true);
            setVisibility(R.id.adapter_layout_line, true);

            for (searchCodeAdapter.Data.resultData d : response.data.resultData) {
                if (d.DET_CD.equals(mResultData.WATER_ADAPTOR)) {
                    setText(R.id.water_adaptor, d.DET_CD_NM);
                    break;
                }
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    private Net.OnNetResponse<searchCodeJoli> onJoliNetResponse = new Net.OnNetResponse<searchCodeJoli>() {

        @Override
        public void onResponse(searchCodeJoli response) {
            hideProgress();

            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            setVisibility(R.id.adapter_layout, true);
            setVisibility(R.id.adapter_layout_line, true);

            for (searchCodeJoli.Data.resultData d : response.data.resultData) {
                if (d.DET_CD.equals(mResultData.WATER_COOKING)) {
                    setText(R.id.water_cooking, d.DET_CD_NM);
                    break;
                }
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

}
