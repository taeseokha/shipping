package com.bodyfriend.shippingsystem.main.refuel.net;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Taeseok on 2018-08-14
 * 구글 시트 업데이트 진행
 */
public interface Sheet {

    //    https://script.google.com/macros/s/AKfycby3QwcSIsLpb_vY1Yi5Ctri5tO9agtDqzfJ3jd9UEeSb4BuUfM/exec
    @FormUrlEncoded
    @POST("AKfycbx_5J-8rL29PM1_2heE-fyYLMOPAkHf02uJh_qEH1hTPNCas8GA/exec?action=insert")
    Observable<Response<Void>> updateGas(
            @Field("date") String format,
            @Field("name") String name,
            @Field("carNumber") String carNumber,
            @Field("gasLiter") String gasLiter,
            @Field("gasPrice") String gasPrice,
            @Field("dashboard") String dashboard,
            @Field("dieselExhaustFluid") String dieselExhaustFluid,
            @Field("ectPrice") String ectPrice,
            @Field("carChang") String carChang,
            @Field("ownerCard") String ownerCard,
            @Field("dieselExhaustFluidLiter") String dieselExhaustFluidLiter
    );

    @FormUrlEncoded
    @POST("AKfycbx_5J-8rL29PM1_2heE-fyYLMOPAkHf02uJh_qEH1hTPNCas8GA/exec?action=readAll")
    Observable<RefuelData> getGasList(
            @Field("carNumber") String carNo
    );
}
