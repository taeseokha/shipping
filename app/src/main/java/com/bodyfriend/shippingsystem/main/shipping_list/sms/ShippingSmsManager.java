package com.bodyfriend.shippingsystem.main.shipping_list.sms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.util.PhoneNumberMatcher;
import com.bodyfriend.shippingsystem.main.shipping_list.db.SmsDBHelper;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by 이주영 on 2016-09-01.
 */
public class ShippingSmsManager extends SmsManager {
    private static ShippingSmsManager instance;

    private SmsDBHelper mSmsDBHelper;

    private ShippingSmsManager(Context context) {
        super(context);
        mSmsDBHelper = new SmsDBHelper(context);
    }

    public static ShippingSmsManager getInstance(Context context) {
        if (instance == null)
            instance = new ShippingSmsManager(context);
        return instance;
    }


    /**
     * 예약문자 발송
     *
     * @param item
     * @param visitTime
     */
    public void sendSmsReservation(ShippingList.resultData.list item, String visitTime) {

        String text = String.format(mContext.getString(R.string.shipping_sms)
                , item.CUST_NAME
                , item.DELIVERYMAN
                , visitTime
        );

        startSmsActivity(createBundle(item, text, 2));
    }

    /**
     * @param item
     */
    public void sendSmsNine(ShippingList.resultData.list item, String visitTime, Calendar cal) {
        String msg = String.format(mContext.getString(R.string.shipping_sms)
                , item.CUST_NAME
                , item.DELIVERYMAN
                , visitTime
        );

        requestSmsAlarm(createBundle(item, msg, 3), cal);
        // 노티피케이션을 선택하면 보내야할 메세지를 모두 보내는 기능의 알람을 등록한다.
        regAllMessageAlarm(cal);
    }

    private ArrayList<SmsModel> createBundle(ShippingList.resultData.list item, String text, int MSG_GB) {
        SmsModel smsModel = new SmsModel();
        smsModel.HPHONE_NO = item.HPHONE_NO;
        smsModel.MSG = text;
        smsModel.CUST_NAME = item.CUST_NAME;
        smsModel.MSG_GB = MSG_GB;
        smsModel.SHIPPING_SEQ = item.SHIPPING_SEQ;

        ArrayList<SmsModel> smsModels = new ArrayList<>();
        smsModels.add(smsModel);
        return smsModels;
    }

    public void startSmsActivity(ArrayList<SmsModel> smsModels) {
        Intent intent = new Intent(mContext, SmsActivity.class);
        intent.putParcelableArrayListExtra(SmsActivity.SMS_MODELS, smsModels);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mContext.startActivity(intent);
    }



    /**
     * 노티피케이션을 선택하면 보내야할 메세지를 모두 보내는 기능의 알람을 등록한다.
     */
    private void regAllMessageAlarm(Calendar cal) {
        Log.d(":: regAllMessageAlarm");
        Intent intent = new Intent(mContext, ShippingAlarmReceiver.class);
        intent.setAction("IS_ALL_MESSAGE_ALARM");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        final String requestCode = sdf.format(cal.getTime());

        Log.d("requestCode => " + requestCode);

        requestSmsAlarm(Integer.parseInt(requestCode), null, intent, cal.getTimeInMillis());
    }

    private void requestSmsAlarm(ArrayList<SmsModel> models, Calendar cal) {
        Intent intent = new Intent(mContext, ShippingAlarmReceiver.class);

        String tmp = models.get(0).HPHONE_NO.replaceAll("[^\\d]", "").replace("0", "");
//        tmp = tmp.replaceAll("010", "");

        int notiId = PhoneNumberMatcher.phoneNumber2NotiId(tmp);
//        Log.d("tmp => " + tmp);
        requestSmsAlarm(notiId, models, intent, cal.getTimeInMillis());
    }

    private void requestSmsAlarm(int requestCode, ArrayList<SmsModel> models, Intent intent, long triggerAtMillis) {
        intent.setAction(requestCode + "");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);

        if (models != null) {
            mSmsDBHelper.insert(requestCode, models);
        }

        am.setRepeating(AlarmManager.RTC_WAKEUP, triggerAtMillis, 0, pendingIntent);
    }

    /**
     * 폰번호로 등록됨 알람을 취소한다.
     *
     * @param phoneNumber
     */
    public void cancelSmsAlarm(String phoneNumber) {
        Intent intent = new Intent(mContext, ShippingAlarmReceiver.class);
        if (!TextUtils.isEmpty(phoneNumber)) {
            String tmp = phoneNumber.replaceAll("-", "");
            int notiId = PhoneNumberMatcher.phoneNumber2NotiId(tmp);
            cancelSmsAlarm(notiId, intent);
        }

    }


    private void cancelSmsAlarm(int requestCode, Intent intent) {
        mSmsDBHelper.delete(requestCode);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }
}