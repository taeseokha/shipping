package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 약관 데이터
 */
public class searchCodeProvision extends searchCodeList {

	public searchCodeProvision() {
		super();
		setParam("code", 9800, "s_producttype", NetConst.s_producttype);

	}

	public searchCodeProvision(String productType) {
		super();
		setParam("code", 9800, "s_producttype", productType);
	}

	@Override
	protected void parse(String json) {
		super.parse(json);
	}

	public Data data;

	public static class Data {
		public List<resultData> resultData;
		public int resultCode;
		public String resultMsg;

		public class resultData {
			public long INS_DT;
			public String DET_CD;
			public String DESCRIPTION;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String UPD_ID;
			public long UPD_DT;
			public String GROUP_CD;
			public String INS_ID;
			public String SORT_SEQ;
			public String REF_2;
			public String REF_3;
			public String REF_1;
			public String COMM_CD;
		}
	}
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): {
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "INS_DT": 1432097156750,
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DET_CD": "M01",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DESCRIPTION": "외관이 이상이 없는지 확인한다.",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DET_CD_NM": "안마의자01",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "COMM_CD_NM": "동의항목",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "UPD_ID": "adminsp",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "UPD_DT": 1432097246327,
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "GROUP_CD": "SP",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "INS_ID": "adminsp",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "SORT_SEQ": "1",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_2": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_3": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_1": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "COMM_CD": "9800"
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): },
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): {
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "INS_DT": 1432097170150,
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DET_CD": "M02",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DESCRIPTION": "전원스위치 ON시 전원표시 LED에 불이 들어오는지 확인한다.",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DET_CD_NM": "안마의자02",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "COMM_CD_NM": "동의항목",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "UPD_ID": "adminsp",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "UPD_DT": 1432097251943,
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "GROUP_CD": "SP",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "INS_ID": "adminsp",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "SORT_SEQ": "2",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_2": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_3": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_1": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "COMM_CD": "9800"
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): },
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): {
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "INS_DT": 1432097183387,
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DET_CD": "M03",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DESCRIPTION": "라크라이닝 상하 조절이 원할한지 확인한다.",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DET_CD_NM": "안마의자03",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "COMM_CD_NM": "동의항목",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "UPD_ID": "adminsp",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "UPD_DT": 1432097257077,
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "GROUP_CD": "SP",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "INS_ID": "adminsp",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "SORT_SEQ": "3",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_2": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_3": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_1": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "COMM_CD": "9800"
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): },
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): {
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "INS_DT": 1432097194383,
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DET_CD": "M04",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DESCRIPTION": "AIR동작에 이상이 없는지 확인한다.",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DET_CD_NM": "안마의자04",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "COMM_CD_NM": "동의항목",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "UPD_ID": "adminsp",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "UPD_DT": 1432097261590,
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "GROUP_CD": "SP",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "INS_ID": "adminsp",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "SORT_SEQ": "4",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_2": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_3": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_1": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "COMM_CD": "9800"
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): },
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): {
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "INS_DT": 1432097212697,
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DET_CD": "M05",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DESCRIPTION": "각 기능별 이상이 없는지 확인한다.",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "DET_CD_NM": "안마의자05",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "COMM_CD_NM": "동의항목",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "UPD_ID": "adminsp",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "UPD_DT": 1432097266250,
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "GROUP_CD": "SP",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "INS_ID": "adminsp",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "SORT_SEQ": "5",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_2": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_3": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "REF_1": "",
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): "COMM_CD": "9800"
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): }
	// 05-20 16:31:57.241: W/_NETLOG_IN(24458): ]

}
