package com.bodyfriend.shippingsystem.main.login.custom;

/**
 * Created by gergun on 13/05/15.
 */
public interface OnBlurCompleteListener {

    void onBlurComplete();
}
