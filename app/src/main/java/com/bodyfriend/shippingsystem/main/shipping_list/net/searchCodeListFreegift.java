package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 사은품
 */
public class searchCodeListFreegift extends searchCodeList {

    public searchCodeListFreegift() {
        super();
        setParam("code", 9600);
        showLog(false);
    }

    @Override
    protected void parse(String json) {
        super.parse(json);
    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
        public List<resultData> resultData;

        public class resultData {
            public long INS_DT;
            public String DET_CD;
            public String DESCRIPTION;
            public String DET_CD_NM;
            public String COMM_CD_NM;
            public String UPD_ID;
            public long UPD_DT;
            public String GROUP_CD;
            public String INS_ID;
            public String SORT_SEQ;
            public String REF_2;
            public String REF_3;
            public String REF_1;
            public String COMM_CD;
        }
    }

//	05-15 15:44:30.281: W/_NETLOG_IN(27727):     "ResultData": [
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):         {
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "INS_DT": 1431671440690,
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "DET_CD": "N",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "DESCRIPTION": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "DET_CD_NM": "미증정",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "COMM_CD_NM": "사은품증정여부",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "UPD_ID": "adminsp",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "UPD_DT": 1431671440690,
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "GROUP_CD": "SP",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "INS_ID": "adminsp",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "SORT_SEQ": "1",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "REF_2": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "REF_3": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "REF_1": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "COMM_CD": "9600"
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):         },
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):         {
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "INS_DT": 1431671511170,
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "DET_CD": "X",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "DESCRIPTION": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "DET_CD_NM": "없음",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "COMM_CD_NM": "사은품증정여부",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "UPD_ID": "adminsp",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "UPD_DT": 1431671511170,
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "GROUP_CD": "SP",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "INS_ID": "adminsp",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "SORT_SEQ": "3",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "REF_2": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "REF_3": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "REF_1": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "COMM_CD": "9600"
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):         },
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):         {
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "INS_DT": 1431671465410,
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "DET_CD": "Y",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "DESCRIPTION": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "DET_CD_NM": "증정",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "COMM_CD_NM": "사은품증정여부",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "UPD_ID": "adminsp",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "UPD_DT": 1431671465410,
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "GROUP_CD": "SP",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "INS_ID": "adminsp",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "SORT_SEQ": "2",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "REF_2": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "REF_3": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "REF_1": "",
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):             "COMM_CD": "9600"
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):         }
//	05-15 15:44:30.281: W/_NETLOG_IN(27727):     ]


}
