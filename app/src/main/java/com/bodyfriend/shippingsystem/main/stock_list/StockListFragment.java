package com.bodyfriend.shippingsystem.main.stock_list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFFragment2;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.main.main.MainActivity;
import com.bodyfriend.shippingsystem.main.notice.NoticeFragment;
import com.bodyfriend.shippingsystem.main.stock_list.net.searchWareHouseList;
import com.bodyfriend.shippingsystem.main.stock_list.net.selectStockList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 이주영 on 2016-10-25.
 */

public class StockListFragment extends BFFragment2 {
    protected ListView mList;
    private StockListFragment.ListAdapter mListAdapter;
    private MainActivity.MainActivityControll mainActivityControll;
    private Spinner spinner1;
    private Spinner spinner2;

    public void setMainActivityControll(MainActivity.MainActivityControll mainActivityControll) {
        this.mainActivityControll = mainActivityControll;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stock_list, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        mainActivityControll.changeTitle(R.layout.title_layout_stock_list);

        mList = (ListView) getView().findViewById(R.id.list_view);

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        Net.async(new searchWareHouseList()).setOnNetResponse(onNetWareHouseResponse);
    }

    public AdapterView.OnItemSelectedListener onSpinnerSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String whSeq = mSearchWareHouseList.data.resultData.get(spinner1.getSelectedItemPosition()).WH_SEQ;
            String productType;

            if (spinner2.getSelectedItemPosition() == 0) {
                productType = "M";
            } else {
                productType = "L";
            }

            Net.async(new selectStockList(whSeq, productType)).setOnNetResponse(onNetStockListResponse);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private searchWareHouseList mSearchWareHouseList;
    private Net.OnNetResponse<searchWareHouseList> onNetWareHouseResponse = new Net.OnNetResponse<searchWareHouseList>() {

        @Override
        public void onErrorResponse(VolleyError error) {

        }

        @Override
        public void onResponse(searchWareHouseList response) {
            mSearchWareHouseList = response;

            spinner1.setOnItemSelectedListener(onSpinnerSelectedListener);
            spinner2.setOnItemSelectedListener(onSpinnerSelectedListener);

            ArrayList<String> strings = new ArrayList<>();
            for (searchWareHouseList.Data.resultData data : response.data.resultData) {
                strings.add(data.WH_NAME);
            }

            setSpinner(R.id.spinner1, strings);
        }
    };

    private Net.OnNetResponse<selectStockList> onNetStockListResponse = new Net.OnNetResponse<selectStockList>() {
        @Override
        public void onErrorResponse(VolleyError error) {

        }

        @Override
        public void onResponse(selectStockList response) {
            if (mListAdapter == null) {
                mListAdapter = new StockListFragment.ListAdapter(getContext(), response.data.resultData.list);
                mList.setAdapter(mListAdapter);
            } else {
                mListAdapter.setItemList(response.data.resultData.list);
                mListAdapter.notifyDataSetChanged();
            }
        }
    };


    public class ListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private ArrayList<selectStockList.Data.resultData.list> mItemList;
        private HashMap<Integer, View> viewHashMap;

        public void setItemList(List<selectStockList.Data.resultData.list> list) {
            this.mItemList = (ArrayList<selectStockList.Data.resultData.list>) list;
        }

        public ListAdapter(Context context, List<selectStockList.Data.resultData.list> list) {
            super();
            this.mItemList = (ArrayList<selectStockList.Data.resultData.list>) list;
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            viewHashMap = new HashMap<>();
        }

        @Override
        public int getCount() {
            int cnt = mItemList.size();
            return cnt;
        }

        @Override
        public Object getItem(int position) {
            return mItemList.get(position);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            StockListFragment.ListAdapter.ViewHolder viewHolder;
            final selectStockList.Data.resultData.list item = mItemList.get(position);
            // 캐시된 뷰가 없을 경우 새로 생성하고 뷰홀더를 생성한다
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.stock_list_item, parent, false);

                viewHolder = new StockListFragment.ListAdapter.ViewHolder(convertView, item);
            }
            // 캐시된 뷰가 있을 경우 저장된 뷰홀더를 사용한다
            else {
                viewHolder = (StockListFragment.ListAdapter.ViewHolder) convertView.getTag();
            }

            viewHolder.text1.setText(item.NUM);
            viewHolder.text2.setText(item.PRODUCT_NAME.trim());
            viewHolder.text3.setText(item.QTY5);
            viewHolder.text4.setText(item.QTY6);
            viewHolder.text5.setText(item.QTY3);
            viewHolder.text6.setText(item.QTY4);

            viewHashMap.put(position, convertView);

            return convertView;
        }

        public class ViewHolder {
            public TextView text1; // No.
            public TextView text2; // 제품명
            public TextView text3; // 양품재고
            public TextView text4; // 리퍼재고
            public TextView text5; // 양품예약
            public TextView text6; // 리퍼예약
            private selectStockList.Data.resultData.list mItem;

            public ViewHolder(View convertView, final selectStockList.Data.resultData.list item) {
                mItem = item;
                text1 = (TextView) convertView.findViewById(R.id.text1);
                text2 = (TextView) convertView.findViewById(R.id.text2);
                text3 = (TextView) convertView.findViewById(R.id.text3);
                text4 = (TextView) convertView.findViewById(R.id.text4);
                text5 = (TextView) convertView.findViewById(R.id.text5);
                text6 = (TextView) convertView.findViewById(R.id.text6);

                convertView.setTag(this);

                convertView.setOnClickListener(onClickListener);
            }

            private View.OnClickListener onClickListener = new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    NoticeFragment.NoticeListAdapter.ViewHolder holder = (NoticeFragment.NoticeListAdapter.ViewHolder) v.getTag();
                    boolean isVisible = holder.text3.getVisibility() == View.VISIBLE;
                    if (!isVisible) {
                        for (int i : viewHashMap.keySet()) {
                            NoticeFragment.NoticeListAdapter.ViewHolder holder1 = (NoticeFragment.NoticeListAdapter.ViewHolder) viewHashMap.get(i).getTag();
                            holder1.text3.setVisibility(View.GONE);
                            viewHashMap.get(i).findViewById(R.id.arrow).setRotation(0);
                        }
                    }
                    holder.text3.setVisibility(isVisible ? View.GONE : View.VISIBLE);
                    v.findViewById(R.id.arrow).setRotation(isVisible ? 0 : 180);
                }
            };
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }
}
