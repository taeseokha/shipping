package com.bodyfriend.shippingsystem.main.main.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.util.List;

/**
 * 공지리스트`
 */
public class appNoticeList extends BFEnty {

    public appNoticeList() {
        setUrl("mobile/api/appNoticeList.json");
//        setParam("noticeScope_srch");
       /* if (BuildConfig.FLAVOR.equals("outApp")) {
            setParam("noticeScope_srch", "O");
        } else {
            setParam("noticeScope_srch", "I");
        }*/
    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
        public resultData resultData;

        public class resultData {
            public int listCount;
            public List<list> list;

            public class list {
                /**
                 * 제목
                 */
                public String NOTICE_TITLE;
                /**
                 * 등록자 아이디
                 */
                public String RG_ID;
                public String PICTURE_ONE;
                /**
                 * 내용
                 */
                public String NOTICE_CONTENTS;
                /**
                 * 공지번호
                 */
                public int NOTICE_SEQ;
                /**
                 * 신규공지
                 */
                public boolean NOTICE_NEW;
                /**
                 * 등록자 이름
                 */
                public String RG_NM;
                /**
                 * 등록일
                 */
                public String RG_DT;
            }
        }
    }

//	05-14 15:54:31.327: W/_NETLOG_IN(27351):                 "NOTICE_TITLE": "test",
//	05-14 15:54:31.327: W/_NETLOG_IN(27351):                 "RG_ID": "adminsp",
//	05-14 15:54:31.327: W/_NETLOG_IN(27351):                 "NOTICE_CONTENTS": "첫번째 공지사항입니다.\n공지내용 테스트 중입니다.",
//	05-14 15:54:31.327: W/_NETLOG_IN(27351):                 "NOTICE_SEQ": 1,
//	05-14 15:54:31.327: W/_NETLOG_IN(27351):                 "NOTICE_NEW": "Y",
//	05-14 15:54:31.327: W/_NETLOG_IN(27351):                 "RG_NM": "배송관리자",
//	05-14 15:54:31.327: W/_NETLOG_IN(27351):                 "RG_DT": "2015-05-14"

}
