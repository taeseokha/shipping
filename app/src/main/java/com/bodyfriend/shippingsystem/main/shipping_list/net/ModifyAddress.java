package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

/**
 * Created by Taeseok on 2018-03-15.
 */

public class ModifyAddress extends BFEnty {


    /**
     * 주소 변경
     *
     * @param m_shippingseq 배송 번호
     * @param m_address     변경 주소
     */
    public ModifyAddress(String m_shippingseq, String m_address) {

        setUrl("mobile/api/modifyAddress.json");

        setParam("m_shippingseq", m_shippingseq, "m_address", m_address);
    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
    }
}
