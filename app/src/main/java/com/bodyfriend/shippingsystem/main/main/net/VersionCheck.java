package com.bodyfriend.shippingsystem.main.main.net;


import com.bodyfriend.shippingsystem.base.BFEnty;

/**
 * Created by 이주영 on 2016-04-05.
 */
public class VersionCheck extends BFEnty {
    private String url = "mobile/api/versionCheck.json";

    public VersionCheck() {
        setEnableProgress();
        setUrl(url);

        setParam(
                "deviceType", "A"
                , "svType", "ship"
        );
    }

    public Data data;

    public static class Data {
        public resultData resultData;

        public class resultData {
            public String msg;
            public int version;
            public String updateType;
        }

        public int resultCode;
    }
}