package com.bodyfriend.shippingsystem.main.shipping_list.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bodyfriend.shippingsystem.main.shipping_list.sms.SmsModel;

import java.util.ArrayList;

/**
 * Created by 이주영 on 2017-04-21.
 */

public class SmsDBHelper extends DBHelper {
    // 데이터 베이스 버전이당
    private static final int VERSION = 1;

    // 테이블명
    private static final String TABLE_NAME = "SMS";

    public SmsDBHelper(Context context) {
        super(context, "Sms.db", null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                "_index INTEGER PRIMARY KEY AUTOINCREMENT" +
                ", REQ_CODE INTEGER" +
                ", HPHONE_NO TEXT" +
                ", SHIPPING_SEQ TEXT" +
                ", ADMIN_ID TEXT" +
                ", ADMIN_NM TEXT" +
                ", TEL_NO TEXT" +
                ", GROUP_CD TEXT" +
                ", PER_CD INTEGER" +
                ", MSG TEXT" +
                ", MSG_GB INTEGER" +
                ", PRODUCT_TYPE TEXT" +
                ", CUST_NAME TEXT" +
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insert(int REQ_CODE, ArrayList<SmsModel> smsModels) {
        insert(REQ_CODE, smsModels.toArray(new SmsModel[smsModels.size()]));
    }

    /**
     * 데이터베이스에 데이터를 입력한다.
     *
     * @param models
     */
    public void insert(int REQ_CODE, SmsModel... models) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        for (SmsModel model : models) {
            values.put("REQ_CODE", REQ_CODE);
            values.put("HPHONE_NO", model.HPHONE_NO);
            values.put("SHIPPING_SEQ", model.SHIPPING_SEQ);
            values.put("ADMIN_ID", model.ADMIN_ID);
            values.put("ADMIN_NM", model.ADMIN_NM);
            values.put("TEL_NO", model.TEL_NO);
            values.put("GROUP_CD", model.GROUP_CD);
            values.put("PER_CD", model.PER_CD);
            values.put("MSG", model.MSG);
            values.put("MSG_GB", model.MSG_GB);
            values.put("PRODUCT_TYPE", model.PRODUCT_TYPE);
            values.put("CUST_NAME", model.CUST_NAME);
            db.insert(TABLE_NAME, null, values);
        }
    }

    /**
     * 삭제
     */
    public void delete(int REQ_CODE) {
        SQLiteDatabase db = getWritableDatabase();

        db.delete(TABLE_NAME, "REQ_CODE=?", new String[]{String.valueOf(REQ_CODE)});
    }

    public ArrayList<SmsModel> select(int REQ_CODE) {
        ArrayList<SmsModel> models = new ArrayList<>();

        // 읽기가 가능하게 DB 열기
        SQLiteDatabase db = getReadableDatabase();
        String result = "";

        // DB에 있는 데이터를 쉽게 처리하기 위해 Cursor를 사용하여 테이블에 있는 모든 데이터 출력
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE REQ_CODE = " + "'" + REQ_CODE + "'", null);
        while (cursor.moveToNext()) {
            SmsModel model = new SmsModel();
            models.add(model);
            // index
            cursor.getInt(0);

            // REQ_CODE
            cursor.getInt(1);

            // HPHONE_NO
            model.HPHONE_NO = cursor.getString(2);
            // SHIPPING_SEQ TEXT
            model.SHIPPING_SEQ = cursor.getString(3);
            // ADMIN_ID TEXT
            model.ADMIN_ID = cursor.getString(4);
            // ADMIN_NM TEXT
            model.ADMIN_NM = cursor.getString(5);
            // TEL_NO TEXT
            model.TEL_NO = cursor.getString(6);
            // GROUP_CD TEXT
            model.GROUP_CD = cursor.getString(7);
            // PER_CD INTEGER
            model.PER_CD = cursor.getInt(8);
            // MSG TEXT
            model.MSG = cursor.getString(9);
            // MSG_GB
            model.MSG_GB = cursor.getInt(10);
            // PRODUCT_TYPE TEXT
            model.PRODUCT_TYPE = cursor.getString(11);
            // CUST_NAME TEXT
            model.CUST_NAME = cursor.getString(12);

        }

        return models;
    }
}
