package com.bodyfriend.shippingsystem.main.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.main.login.LoginActivity;


/**
 * Implementation of App Widget functionality.
 */
public class ServiceList extends AppWidgetProvider {
    public static final String ACTION_CLICK = "CLICK";

    

    public static String EXTRA_WORD =
            "com.commonsware.android.appwidget.lorem.WORD";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        Log.d("updateAppWidget ");

        CharSequence widgetText = context.getString(R.string.appwidget_text);

        RemoteViews mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.service_list);
        mRemoteViews.setTextViewText(R.id.appwidget_text, widgetText);

        Intent intent = new Intent(context, ServiceList.class);
        intent.setAction(ACTION_CLICK);

        PendingIntent pending = PendingIntent.getBroadcast(context, appWidgetId,
                intent, 0);
        mRemoteViews.setOnClickPendingIntent(R.id.btn_refresh, pending);


        Intent intent2 = new Intent(context, WidgetService.class);
        intent2.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent2.setData(Uri.parse(intent2.toUri(Intent.URI_INTENT_SCHEME)));
        mRemoteViews.setRemoteAdapter(R.id.list_view, intent2);


        Intent clickIntent = new Intent(context, CallActivity.class);
        PendingIntent clickPI = PendingIntent
                .getActivity(context, 0,
                        clickIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        mRemoteViews.setPendingIntentTemplate(R.id.list_view, clickPI);

        appWidgetManager.updateAppWidget(appWidgetId, mRemoteViews);
    }


    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        AppWidgetManager mgr = AppWidgetManager.getInstance(context);

        Log.d("action : ", action);
        if (action != null && action.equals(ACTION_CLICK)) {
            int appWidgetIds[] = mgr.getAppWidgetIds(new ComponentName(context, ServiceList.class));
            mgr.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.list_view);
            return;
        }


        super.onReceive(context, intent);
    }


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            Log.d("appWidgetId : " + appWidgetId);
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
    }

    @Override
    public void onDisabled(Context context) {
    }


}

