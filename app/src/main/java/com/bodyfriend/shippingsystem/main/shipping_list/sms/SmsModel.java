package com.bodyfriend.shippingsystem.main.shipping_list.sms;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 이주영 on 2017-02-27.
 */
public class SmsModel implements Parcelable {
    public String HPHONE_NO;
    public String SHIPPING_SEQ;
    public String ADMIN_ID;
    public String ADMIN_NM;
    public String TEL_NO;
    public String GROUP_CD;
    public int PER_CD;
    public String MSG;
    public int MSG_GB;
    public String PRODUCT_TYPE;
    public String CUST_NAME;

    public SmsModel() {
    }

    protected SmsModel(Parcel in) {
        HPHONE_NO = in.readString();
        SHIPPING_SEQ = in.readString();
        ADMIN_ID = in.readString();
        ADMIN_NM = in.readString();
        TEL_NO = in.readString();
        GROUP_CD = in.readString();
        PER_CD = in.readInt();
        MSG = in.readString();
        MSG_GB = in.readInt();
        PRODUCT_TYPE = in.readString();
        CUST_NAME = in.readString();
    }

    public static final Creator<SmsModel> CREATOR = new Creator<SmsModel>() {
        @Override
        public SmsModel createFromParcel(Parcel in) {
            return new SmsModel(in);
        }

        @Override
        public SmsModel[] newArray(int size) {
            return new SmsModel[size];
        }
    };

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("HPHONE_NO : ");
        builder.append(HPHONE_NO);
        builder.append(", ");

        builder.append("SHIPPING_SEQ : ");
        builder.append(SHIPPING_SEQ);
        builder.append(", ");

        builder.append("ADMIN_ID : ");
        builder.append(ADMIN_ID);
        builder.append(", ");

        builder.append("ADMIN_NM : ");
        builder.append(ADMIN_NM);
        builder.append(", ");

        builder.append("TEL_NO : ");
        builder.append(TEL_NO);
        builder.append(", ");

        builder.append("GROUP_CD : ");
        builder.append(GROUP_CD);
        builder.append(", ");

        builder.append("PER_CD : ");
        builder.append(PER_CD);
        builder.append(", ");

        builder.append("MSG : ");
        builder.append(MSG);
        builder.append(", ");

        builder.append("MSG_GB : ");
        builder.append(MSG_GB);
        builder.append(", ");

        builder.append("PRODUCT_TYPE : ");
        builder.append(PRODUCT_TYPE);
        builder.append(", ");

        builder.append("CUST_NAME : ");
        builder.append(CUST_NAME);
        builder.append(", ");

        return builder.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(HPHONE_NO);
        dest.writeString(SHIPPING_SEQ);
        dest.writeString(ADMIN_ID);
        dest.writeString(ADMIN_NM);
        dest.writeString(TEL_NO);
        dest.writeString(GROUP_CD);
        dest.writeInt(PER_CD);
        dest.writeString(MSG);
        dest.writeInt(MSG_GB);
        dest.writeString(PRODUCT_TYPE);
        dest.writeString(CUST_NAME);
    }
}
