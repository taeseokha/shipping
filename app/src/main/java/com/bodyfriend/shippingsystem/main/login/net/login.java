package com.bodyfriend.shippingsystem.main.login.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

public class login extends BFEnty {

    //    public login(String admin_id, String admin_pwd, String admin_telno) {
//        showLog(true);
//        setUrl("login/appLogin.json");
//        setParam("admin_id", admin_id, "admin_pwd", admin_pwd, "admin_telno", admin_telno);
////        setParam("admin_id", admin_id, "admin_pwd", admin_pwd);
//    }
    public login(String admin_id, String admin_pwd) {
        showLog(true);
        setUrl("login/appLogin.json");
//        setParam("admin_id", admin_id, "admin_pwd", admin_pwd, "admin_telno", admin_telno);
        setParam("admin_id", admin_id, "admin_pwd", admin_pwd);
    }

    public login(String admin_id, String admin_pwd, String appType) {
        showLog(true);
        setUrl("login/appLogin.json");
//        setParam("admin_id", admin_id, "admin_pwd", admin_pwd, "admin_telno", admin_telno);
        setParam("admin_id", admin_id, "admin_pwd", admin_pwd, "appType", appType);
    }

    @Override
    protected void parse(String json) {
        super.parse(json);
    }

    public Data data;

    public static class Data {
        public resultData resultData;
        public String resultMsg;
        public int resultCode;

        public class resultData {
            public String CAR_NUMBER;
            public String TEL_NO;
            public String resultMsg;
            public String SID;
            public int PER_CD;
            public String GROUP_CD;
            public String ADMIN_NM;
            public String COMPANY_NM;
            public String IPHONE_UUID;
            public int resultCode;
            public String loginDate;
            public String PUSH_REGID;
            public String ADMIN_ID;


            @Override
            public String toString() {
                return "ResultData{" +
                        "CAR_NUMBER='" + CAR_NUMBER + '\'' +
                        ", TEL_NO='" + TEL_NO + '\'' +
                        ", resultMsg='" + resultMsg + '\'' +
                        ", SID='" + SID + '\'' +
                        ", PER_CD=" + PER_CD +
                        ", GROUP_CD='" + GROUP_CD + '\'' +
                        ", ADMIN_NM='" + ADMIN_NM + '\'' +
                        ", COMPANY_NM='" + COMPANY_NM + '\'' +
                        ", IPHONE_UUID='" + IPHONE_UUID + '\'' +
                        ", resultCode=" + resultCode +
                        ", loginDate='" + loginDate + '\'' +
                        ", PUSH_REGID='" + PUSH_REGID + '\'' +
                        ", ADMIN_ID='" + ADMIN_ID + '\'' +

                        '}';
            }
        }

        @Override
        public String toString() {
            return "Data{" +
                    "ResultData=" + resultData +
                    ", resultMsg='" + resultMsg + '\'' +
                    ", resultCode=" + resultCode +
                    '}';
        }
    }

}
