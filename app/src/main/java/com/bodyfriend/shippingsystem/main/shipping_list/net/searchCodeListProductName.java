package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 제품명
 */
public class searchCodeListProductName extends searchCodeList {
	public searchCodeListProductName() {
		super();
		setParam("code", 300);
	}

	public Data data;

	public static class Data {
		public List<resultData> resultData;
		public String resultMsg;
		public int resultCode;

		public class resultData {
			public String GROUP_CD;
			public String DET_CD;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String COMM_CD;
		}
	}

//		05-04 09:28:01.191: W/(10111):             "INS_DT": 1419383794750,
//		05-04 09:28:01.191: W/(10111):             "DET_CD": "305",
//		05-04 09:28:01.195: W/(10111):             "DESCRIPTION": "",
//		05-04 09:28:01.195: W/(10111):             "DET_CD_NM": "아이로보",
//		05-04 09:28:01.195: W/(10111):             "COMM_CD_NM": "제품명",
//		05-04 09:28:01.195: W/(10111):             "UPD_ID": "admin",
//		05-04 09:28:01.195: W/(10111):             "UPD_DT": 1419383794750,
//		05-04 09:28:01.196: W/(10111):             "GROUP_CD": "SV",
//		05-04 09:28:01.196: W/(10111):             "INS_ID": "admin",
//		05-04 09:28:01.196: W/(10111):             "SORT_SEQ": "5",
//		05-04 09:28:01.196: W/(10111):             "REF_2": "",
//		05-04 09:28:01.196: W/(10111):             "REF_3": "",
//		05-04 09:28:01.196: W/(10111):             "REF_1": "",
//		05-04 09:28:01.196: W/(10111):             "COMM_CD": "300"

}
