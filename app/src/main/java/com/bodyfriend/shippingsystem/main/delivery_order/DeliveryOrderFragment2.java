package com.bodyfriend.shippingsystem.main.delivery_order;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFFragment2;
import com.bodyfriend.shippingsystem.databinding.FragmentDeliveryOrder2Binding;
import com.bodyfriend.shippingsystem.main.delivery_order.adapter.DeliveryOrderAdapter;
import com.bodyfriend.shippingsystem.main.delivery_order.net.DeliveryResponseData;
import com.bodyfriend.shippingsystem.main.delivery_order.net.OutLoiList;
import com.bodyfriend.shippingsystem.main.delivery_order.net.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.delivery_order.net.WarehouseApi;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.main.MainActivity;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by 이주영 on 2016-12-08.
 * 출고지시서 페이지이다.
 */

public class DeliveryOrderFragment2 extends BFFragment2 {

    public static final String TAG = DeliveryOrderFragment2.class.getSimpleName();
    // 상세화면 갈때 리절트 코드
    private final int RESULTCODE_SHIPDETAIL = 1234;
    private MainActivity.MainActivityControll mMainActivityControll;
    public static int ADD_PRODUCT_ITEM = 29;
    private FragmentDeliveryOrder2Binding mBinding;
    private DeliveryOrderAdapter mRecyclerAdapter;
    private ArrayList<OutLoiList.list> mOutLoiList;


    public void setMainActivityControll(MainActivity.MainActivityControll mainActivityControll) {
        this.mMainActivityControll = mainActivityControll;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_delivery_order2, container, false);
        return mBinding.getRoot();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getOutLoiList();
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();
        setupTitle();
        setupRecyclerView();
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        getOutLoiList();
    }


    /**
     * 리사이클러뷰 설정
     */
    private void setupRecyclerView() {
        mBinding.recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mBinding.recyclerView.setLayoutManager(layoutManager);
        mRecyclerAdapter = new DeliveryOrderAdapter();
        mBinding.recyclerView.setAdapter(mRecyclerAdapter);
        mBinding.recyclerView.setItemAnimator(new CustomItemAnimator());

        mBinding.recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), mBinding.recyclerView
                , new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }

            @Override
            public void onLongItemClick(View view, int position) {
                new AlertDialog.Builder(mContext).setMessage("선택항목 삭제 하시겠습니까?").setPositiveButton("확인", (dialog, which) -> {
                    dialog.cancel();
                    delExportLoi(mOutLoiList.get(position));
                }).setNegativeButton("아니오", (dialog, which) -> {
                    dialog.cancel();
                }).show();
            }
        }));
    }

    private void delExportLoi(OutLoiList.list list) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ServiceGenerator.createService(WarehouseApi.class)
                .delExportLoi(list.loiNo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<DeliveryResponseData>() {


                    @Override
                    public void onNext(DeliveryResponseData deliveryResponseData) {
                        if (deliveryResponseData.success) {
                            mBinding.progressBar.setVisibility(View.GONE);
                            getOutLoiList();
                        } else {
                            new AlertDialog.Builder(getContext())//
                                    .setMessage(deliveryResponseData.msg)
                                    .setTitle(deliveryResponseData.message)
                                    .show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onComplete() {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }
                });

    }

    /**
     * 출고지시서 리트스 요청
     */
    @SuppressLint("CheckResult")
    private void getOutLoiList() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ServiceGenerator.createService(WarehouseApi.class)
                .getOutLoiList(Auth.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<OutLoiList>() {
                    @Override
                    public void onNext(OutLoiList outLoiList) {
                        if (mOutLoiList != null) {
                            mOutLoiList.clear();
                        }
                        mBinding.progressBar.setVisibility(View.GONE);
                        if (outLoiList.list != null) {
                            mRecyclerAdapter.setData(outLoiList.list);
                            mBinding.emptyView.setVisibility(View.GONE);
                            mOutLoiList = outLoiList.list;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onComplete() {
                        mBinding.progressBar.setVisibility(View.GONE);
                    }
                });
    }


    /**
     * 타이틀 설정
     */
    private void setupTitle() {
        if (mMainActivityControll != null) {
            mMainActivityControll.changeTitle(R.layout.title_layout_delivery_order);
            mMainActivityControll.getView(R.id.date).setOnClickListener(v -> {
                startActivityForResult(new Intent(getContext(), DeliveryAddActivity.class), ADD_PRODUCT_ITEM);
            });
        }
    }

}