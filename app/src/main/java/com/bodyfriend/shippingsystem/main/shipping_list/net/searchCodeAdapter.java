package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.main.net.searchCodeList;

import java.util.List;

/**
 * 아답터
 */
public class searchCodeAdapter extends searchCodeList {

	public searchCodeAdapter() {
		super();
		setParam("code", 9601);

	}

	@Override
	protected void parse(String json) {
		super.parse(json);
	}

	public Data data;

	public static class Data {
		public List<resultData> resultData;
		public int resultCode;
		public String resultMsg;

		public class resultData {
			public long INS_DT;
			public String DET_CD;
			public String DESCRIPTION;
			public String DET_CD_NM;
			public String COMM_CD_NM;
			public String UPD_ID;
			public long UPD_DT;
			public String GROUP_CD;
			public String INS_ID;
			public String SORT_SEQ;
			public String REF_2;
			public String REF_3;
			public String REF_1;
			public String COMM_CD;
		}
	}
//
//	06-19 14:52:02.116: W/_NETLOG_IN(657): >>,들와:,200
//	06-19 14:52:02.116: W/_NETLOG_IN(657): {
//	06-19 14:52:02.116: W/_NETLOG_IN(657):     "ResultData": [
//	06-19 14:52:02.116: W/_NETLOG_IN(657):         {
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "INS_DT": 1434688147380,
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DET_CD": "S01",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DESCRIPTION": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DET_CD_NM": "소",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "COMM_CD_NM": "아답터",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "UPD_ID": "adminsp",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "UPD_DT": 1434688147380,
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "GROUP_CD": "SP",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "INS_ID": "adminsp",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "SORT_SEQ": "1",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_2": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_3": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_1": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "COMM_CD": "9601"
//	06-19 14:52:02.116: W/_NETLOG_IN(657):         },
//	06-19 14:52:02.116: W/_NETLOG_IN(657):         {
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "INS_DT": 1434688155580,
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DET_CD": "S02",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DESCRIPTION": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DET_CD_NM": "중",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "COMM_CD_NM": "아답터",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "UPD_ID": "adminsp",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "UPD_DT": 1434688155580,
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "GROUP_CD": "SP",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "INS_ID": "adminsp",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "SORT_SEQ": "2",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_2": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_3": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_1": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "COMM_CD": "9601"
//	06-19 14:52:02.116: W/_NETLOG_IN(657):         },
//	06-19 14:52:02.116: W/_NETLOG_IN(657):         {
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "INS_DT": 1434688162677,
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DET_CD": "S03",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DESCRIPTION": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DET_CD_NM": "대",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "COMM_CD_NM": "아답터",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "UPD_ID": "adminsp",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "UPD_DT": 1434688162677,
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "GROUP_CD": "SP",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "INS_ID": "adminsp",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "SORT_SEQ": "3",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_2": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_3": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_1": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "COMM_CD": "9601"
//	06-19 14:52:02.116: W/_NETLOG_IN(657):         },
//	06-19 14:52:02.116: W/_NETLOG_IN(657):         {
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "INS_DT": 1434688189430,
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DET_CD": "S04",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DESCRIPTION": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DET_CD_NM": "다다",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "COMM_CD_NM": "아답터",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "UPD_ID": "adminsp",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "UPD_DT": 1434688189430,
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "GROUP_CD": "SP",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "INS_ID": "adminsp",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "SORT_SEQ": "4",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_2": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_3": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_1": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "COMM_CD": "9601"
//	06-19 14:52:02.116: W/_NETLOG_IN(657):         },
//	06-19 14:52:02.116: W/_NETLOG_IN(657):         {
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "INS_DT": 1434688200037,
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DET_CD": "S05",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DESCRIPTION": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "DET_CD_NM": "특대",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "COMM_CD_NM": "아답터",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "UPD_ID": "adminsp",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "UPD_DT": 1434688200037,
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "GROUP_CD": "SP",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "INS_ID": "adminsp",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "SORT_SEQ": "5",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_2": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_3": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "REF_1": "",
//	06-19 14:52:02.116: W/_NETLOG_IN(657):             "COMM_CD": "9601"
//	06-19 14:52:02.116: W/_NETLOG_IN(657):         }
//	06-19 14:52:02.116: W/_NETLOG_IN(657):     ]
//	06-19 14:52:02.116: W/_NETLOG_IN(657): }:at (NetRequest.java:202)

}
