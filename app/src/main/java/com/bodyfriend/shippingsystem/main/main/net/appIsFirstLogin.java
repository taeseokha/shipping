package com.bodyfriend.shippingsystem.main.main.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

/**
 * Created by 이주영 on 2017-03-23.
 * <p>
 * 메인화면에 앱이 들어왔을때 체크하며,
 * 결과값이 Y이면
 * 차량 등록 팝업을 띄운다.
 */
public class appIsFirstLogin extends BFEnty {

    public appIsFirstLogin() {
        setUrl("mobile/api/appIsFirstLogin.json");
    }

    public Data data;

    public static class Data {
        public String resultCode;
    }
}
