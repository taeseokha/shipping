package com.bodyfriend.shippingsystem.main.login.net;

import com.bodyfriend.shippingsystem.main.login.data.AuthenticationData;
import com.bodyfriend.shippingsystem.main.login.data.AuthenticationRequest;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface Authentication {


    @POST("auth/sendMessage")
    @Headers("Content-Type: application/json")
    Observable<AuthenticationData> getAuthenticationCode(

            @Body AuthenticationRequest body
    );

    @POST("auth/checkMsgid")
    @Headers("Content-Type: application/json")
    Observable<AuthenticationData> checkMsgid(
            @Body MsgIdRequest body
    );

    // 인증 번호
    @POST("auth/checkAuthCode")
    @Headers("Content-Type: application/json")
    Observable<AuthenticationData> checkAuthCode(
            @Body MsgIdRequest body
    );


}
