package com.bodyfriend.shippingsystem.main.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.base.util.PP;
import com.bodyfriend.shippingsystem.base.util.SU;
import com.bodyfriend.shippingsystem.main.login.net.login.Data;

public class Auth {

    private static SharedPreferences pre;

    public static void CREATE(final Context context) {
        pre = context.getSharedPreferences("auth", Context.MODE_PRIVATE);
    }

    public static boolean checkLogin(Context context) {
        if (isLogin()) {
            return true;
        }

        startLogin(context);
        return false;
    }


    public static boolean isLogin() {
        final LoginAuth loginAuth = new LoginAuth();
        final SharedPreferences p = pre;

        loginAuth.ADMIN_ID = p.getString("ADMIN_ID", "");

        if (Auth.d == null) {
            return false;
        }
        try {
            return !SU.isEmpty(Auth.d.resultData.SID);
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void logout() {
        pre.edit().clear().apply();
        Auth.d = null;
        PP.LOGIN_DATE.set("");
        // OH.c().notifyObservers(TYPE.EXIT);
    }

    public static void login(final Data data) {
        saveP(data);
        OH.c().notifyObservers(OH.TYPE.LOGIN);
    }

    public static void saveIdPw(final String id, final String pw) {
        final Editor edit = pre.edit();
        edit.putString("ID", id);
        edit.putString("PW", pw);
        edit.apply();
    }

    public static String getSid() {
        final SharedPreferences p = pre;

        return p.getString("SID", "");
    }

    public static String getId() {
        final SharedPreferences p = pre;

        return p.getString("ID", "");
    }

    public static String getPw() {
        final SharedPreferences p = pre;

        return p.getString("PW", "");
    }

    public static Data d;

    private static void saveP(final Data d) {
        Auth.d = d;
        final Editor edit = pre.edit();
        Log.d("d.ResultData.PER_CD : " + d.resultData.PER_CD);
        Log.d("d.ResultData.ADMIN_ID : " + d.resultData.ADMIN_ID);
        Log.d("  Auth.d  : " +   Auth.d.toString());
        edit.putString("SID", d.resultData.SID);
        edit.putInt("PER_CD", d.resultData.PER_CD);
        edit.putString("GROUP_CD", d.resultData.GROUP_CD);
        edit.putString("COMPANY_NM", d.resultData.COMPANY_NM);
        edit.putString("ADMIN_NM", d.resultData.ADMIN_NM);
        edit.putString("loginDate", d.resultData.loginDate);
        edit.putString("ADMIN_ID", d.resultData.ADMIN_ID);
        edit.apply();
    }

    public static boolean checkSession(final int resultCode) {
        if (resultCode == 309) {
            d = null;
            return true;
        } else {
            return false;
        }
    }

    public static void startLogin(final Context mContext) {
        if (!checkLoginTryTime())
            return;
        final Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mContext.startActivity(intent);

    }

    private static long runTimeLogin = 0;

    private static boolean checkLoginTryTime() { // 10초안에 로그인화면 요청은 무시한다.
        final long curTime = System.currentTimeMillis();
        final long gapTime = curTime - runTimeLogin;
        if (gapTime < 10000) {
            return false;
        } else {
            runTimeLogin = curTime;
            return true;
        }
    }
}
