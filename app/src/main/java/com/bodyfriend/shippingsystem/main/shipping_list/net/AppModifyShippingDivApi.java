package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.main.delivery_order.net.BfData;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AppModifyShippingDivApi {
    @FormUrlEncoded
    @POST("mobile/api/appUpdateGiftAddr.json")
    Observable<BfData> updateGiftAddr(
            @Field("m_freegiftAddr") String addr
           , @Field("BODY_NO") String mBodyNo
    );
}
