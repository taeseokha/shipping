package com.bodyfriend.shippingsystem.main.shipping_list.sms;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.bodyfriend.shippingsystem.BuildConfig;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.main.shipping_list.sms.net.insertWorkMsgLog;

import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * 다중 sms 송신 액티비티
 * <p>
 * must have <uses-permission android:name="android.permission.SEND_SMS" /> in AndroidManifest.xml
 */
public class SmsActivity extends BFActivity {
    public static final String SMS_MODELS = "SMS_MODELS";
    private static final int REQUEST_PERVISSION_SMS = 1000;
    private List<SmsModel> smsModels;

    private AlertDialog complteDialog;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        Log.d(":: onCreate");

        // 전송 버튼
        findViewById(R.id.btn_send).setOnClickListener(v -> showSendDialog());
        // 취소 버튼
        findViewById(R.id.btn_cancel).setOnClickListener(v -> cancel());
    }

    @Override
    protected void onParseExtra() {
        super.onParseExtra();
        Log.d(":: onParseExtra");

        // 파셀레이블로된 sms model들을 가져온다.
        smsModels = getIntent().getParcelableArrayListExtra(SMS_MODELS);

        try {
            if (smsModels == null || smsModels.size() == 0)
                throw new NullPointerException(getString(R.string.error_empty_sms_model));
        } catch (Exception e) {
            e.printStackTrace();
            showModelsEmptyMessage();
        }
    }

    @Override
    protected void onLoadOnce() {
        Log.d(":: onLoadOnce");
        super.onLoadOnce();
        setUpViewPager();
    }

    /**
     * 뷰페이져를 셋업한다.
     */
    private void setUpViewPager() {
        Log.d(":: setUpViewPager");
        mViewPager = (ViewPager) findViewById(R.id.smsPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // 페이지의 왼쪽 오른쪽 화살표가 보여져야하는지 체크한다.
                checkPageLeftAndRight();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        SmsAdapter smsAdapter = new SmsAdapter(this, smsModels);
        mViewPager.setAdapter(smsAdapter);
    }

    /**
     * 뷰페이지 양옆의 화살표를 보여주거나 없애자
     */
    private void checkPageLeftAndRight() {
        if (smsModels == null) return;
        if (smsModels.size() == 0) return;

        if (smsModels.size() == 1) {
            setVisibility(R.id.right, false);
            setVisibility(R.id.left, false);
            return;
        }

        // 뷰페이지가 왼쪽끝이면
        if (mViewPager.getCurrentItem() == 0) {
            setVisibility(R.id.right, true);
            setVisibility(R.id.left, false);
        } else if (mViewPager.getCurrentItem() + 1 == smsModels.size()) {
            // 뷰페이저의 위치가 오른쪽 끝이면
            setVisibility(R.id.right, false);
            setVisibility(R.id.left, true);
        } else {
            // 중간이면 둘다 보여줌
            setVisibility(R.id.right, true);
            setVisibility(R.id.left, true);
        }
    }

    /**
     * 모델이 없는것을 노출하는 다이얼로그를 만든다.
     */
    private void showModelsEmptyMessage() {
        Log.d(":: showModelsEmptyMessage");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.empty_message);
        builder.setPositiveButton(R.string.comfirm, (dialog, which) -> finish());
        builder.setCancelable(false);
        builder.create().show();
    }

    /**
     * 정말 메세지 보낼거냐고 묻는 다이얼로그
     */
    @SuppressLint("StringFormatMatches")
    private void showSendDialog() {
        Log.d(":: showSendDialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format(getString(R.string.want_you_send_msg), smsModels.size()));
        builder.setPositiveButton(R.string.comfirm, ((dialog, which) -> sendSms()));
        builder.setNegativeButton(R.string.cancel, null);
        builder.setCancelable(false);
        builder.create().show();
    }

    /**
     * 메세지 송신
     */
    @SuppressLint("StringFormatMatches")
    private void sendSms() {
        Log.d(":: sendSms");

        // 퍼미션이 없으면 퍼미션을 요청합니다.
        int checkPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
        Log.d(":: sendSms checkPermission -> " + checkPermission);
        if (checkPermission == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, REQUEST_PERVISSION_SMS);
            return;
        }

        // 메세지를 보냅니다.
        SmsManager smsManager = new SmsManager(this);
        smsManager.setOnSmsSendResponseListener((complete, fail, total) -> showCompleteMsg(String.format(getString(R.string.sms_completeMsg, total, complete))));
        for (SmsModel s : smsModels) {
            // 현재 접속한 서버가 운영이 아니면 내(이주영) 전화번호로 테스트 메세지를 보낸다.
            if (!BuildConfig.DEBUG) {
                smsManager.sendSms(s.HPHONE_NO, s.MSG);
            } else {
                smsManager.sendSms("0108181314", s.MSG);
            }
        }
    }

    /**
     * 문자 발송 성공 팝업
     */
    private void showCompleteMsg(String str) {
        // 액티비티 종료되었으면 리턴한다.
        if (SmsActivity.this.isFinishing()) return;
        if (complteDialog == null) {
            complteDialog = new AlertDialog.Builder(this).create();
        }
        complteDialog.setMessage(str);
        complteDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.comfirm),
                (dialog, which) -> complete());

        if (!SmsActivity.this.isFinishing()) {
            if (!complteDialog.isShowing()) complteDialog.show();
        }
    }

    /**
     * 송신 완료
     */
    private void complete() {
        complteDialog = null;
        setResult(RESULT_OK);
        insertLog();
        finish();
    }

    /**
     * 송신 성공 로그를 보낸다.
     */
    private void insertLog() {
        for (SmsModel s : smsModels) {
            Net.async(new insertWorkMsgLog(s.SHIPPING_SEQ, s.ADMIN_ID, s.ADMIN_NM, s.TEL_NO, s.GROUP_CD, s.PER_CD, s.MSG, s.MSG_GB, s.PRODUCT_TYPE)).setOnNetResponse(null);
        }
    }

    /**
     * 송신 취소
     */
    private void cancel() {
        Log.d(":: cancel");
        // 취소됨
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            // SMS 이용 퍼미션을 획득에 성공하면 다시 sendSms를 호출한다.
            if (requestCode == REQUEST_PERVISSION_SMS) {
                if (grantResults[0] == RESULT_OK) {
                    sendSms();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
