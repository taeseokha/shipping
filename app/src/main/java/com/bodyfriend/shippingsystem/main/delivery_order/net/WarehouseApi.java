package com.bodyfriend.shippingsystem.main.delivery_order.net;

import org.json.JSONArray;
import org.json.JSONObject;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Taeseok on 2018-03-05.
 */

public interface WarehouseApi {


    @FormUrlEncoded
    @POST("linkTms/selectOutLoiList.do")
    Observable<OutLoiList> getOutLoiList(
            @Field("deliveryManId") String admin_id
    );

    @FormUrlEncoded
    @POST("linkTms/selectItemList.do")
    Observable<ProductItemListData> getItemList(
            @Field("clsCd") String clsCd,
            @Field("itemNmKr") String itemNmKr,
            @Field("itemNmEn") String itemNmEn
    );


    @Multipart
    @POST("linkTms/addExportLoi.do")
    Observable<DeliveryResponseData> addReserveInsData(
            @Query("loiGioTp") String loiGioTp
            , @Query("procSystCd") String procSystCd
            , @Query("whsCd") String whsCd
            , @Query("isMove") boolean isMove
            , @Query("ownrHrId") String ownrHrId
            , @Query("ownrHrNm") String ownrHrNm
            , @Query("vhclNo") String vhclNo
            , @Query("rmrk") String rmrk
            , @Part("listData") RequestBody list
    );

    @FormUrlEncoded
    @POST("linkTms/delExportLoi.do")
    Observable<DeliveryResponseData> delExportLoi(
            @Field("loiNo") String loiNo
    );
}
