package com.bodyfriend.shippingsystem.main.main.net;

import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.base.NetConst;

import java.util.ArrayList;
import java.util.List;

/**
 * 메뉴좌측의 기사정보
 */
public class searchCodeListDirector extends searchCodeList {

    public searchCodeListDirector() {
        super();

        if (NetConst.s_producttype.equals(NetConst.PRODUCT_TYPE_M)) { // 안마의자
            setParam("code", 9900);
        } else if (NetConst.s_producttype.equals(NetConst.PRODUCT_TYPE_W)) { // 웰리스 정수기
            setParam("code", 9901);
        }

    }

    @Override
    protected void parse(String json) {
        super.parse(json);

        List<Data.resultData> resultData = new ArrayList<>();
        for (Data.resultData d : data.resultData) {

            if (d.REF_1 == Auth.d.resultData.PER_CD) resultData.add(d);
        }
        data.resultData = resultData;
    }

    public Data data;

    public static class Data {
        public List<resultData> resultData;

        public class resultData {
            public long INS_DT;
            public String DET_CD;
            public String DESCRIPTION;
            public String DET_CD_NM;
            public String COMM_CD_NM;
            public String UPD_ID;
            public long UPD_DT;
            public String GROUP_CD;
            public String INS_ID;
            public String SORT_SEQ;
            public String REF_2;
            public String REF_3;
            public int REF_1;
            public String COMM_CD;
        }
    }
//	"GROUP_CD": "SP",
//			"INS_ID": "shipadmin",
//			"DET_CD": "10",
//			"DESCRIPTION": "010-5220-1947",
//			"SORT_SEQ": "10",
//			"DET_CD_NM": "오영록",
//			"COMM_CD_NM": "서울,경기담당자",
//			"REF_1": "105",
//			"COMM_CD": "9900",
//			"UPD_ID": "shipadmin"


//	"INS_DT": 1431430339087,
//			"DET_CD": "02",
//			"DESCRIPTION": "010-2127-9536",
//			"DET_CD_NM": "방윤정[본사서울]",
//			"COMM_CD_NM": "담당연락처&#40;안마의자&#41;",
//			"UPD_ID": "shipadmin",
//			"UPD_DT": 1434712243190,
//			"GROUP_CD": "SP",
//			"INS_ID": "shipadmin",
//			"SORT_SEQ": "2",
//			"REF_2": "",
//			"REF_3": "",
//			"REF_1": "",
//			"COMM_CD": "9900"
}