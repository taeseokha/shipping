package com.bodyfriend.shippingsystem.main.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RemoteViews;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.manager.TM;
import com.bodyfriend.shippingsystem.base.net.ServiceGenerator;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.login.net.login;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingList;
import com.bodyfriend.shippingsystem.main.widget.net.ListNetManager;
import com.bodyfriend.shippingsystem.main.widget.net.LoginManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.RemoteViewsService.RemoteViewsFactory;

/**
 * Created by Taeseok on 2017-10-25.
 */

public class WidgetDisplay implements RemoteViewsFactory {

    private int appWidgetId;

    private ArrayList<ShippingList.resultData.list> mResultDataList;
    private Context mContext = null;

    public WidgetDisplay(Context ctxt, Intent intent) {
        this.mContext = ctxt;
        Log.d("WidgetDisplay L : ");
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);

        if (mResultDataList == null)
            widgetLogin(ctxt);
    }


    @Override
    public int getCount() {
        return (mResultDataList == null) ? 0 : mResultDataList.size();
//        return (items.length);
    }

    @Override
    public RemoteViews getViewAt(int position) {
        // Set combine image to the image view using remote view instancel
        if (mResultDataList.isEmpty()) {
            return null;
        }
        ShippingList.resultData.list receiveListItem = mResultDataList.get(position);
        Log.d("remoteViews~~~", receiveListItem.toString());

        RemoteViews remoteView = new RemoteViews(mContext.getPackageName(), R.layout.item_widget_shipping_content);

        remoteView.setTextViewText(R.id.product, receiveListItem.PRODUCT_NAME);
        remoteView.setTextViewText(R.id.time, TextUtils.isEmpty(receiveListItem.PROMISE_TIME) ? "미정" : receiveListItem.PROMISE_TIME);
        remoteView.setTextViewText(R.id.address, receiveListItem.INSADDR);
        remoteView.setTextViewText(R.id.CUST_NAME, receiveListItem.CUST_NAME);

        if (!TextUtils.isEmpty(receiveListItem.SHIPPING_TYPE)) {
            remoteView.setTextViewText(R.id.shipping_type, receiveListItem.SHIPPING_TYPE);
        } else {
            remoteView.setViewVisibility(R.id.shipping_type, View.INVISIBLE);
        }
        if (receiveListItem.MUST_FLAG.equals("Y")) {
            remoteView.setTextViewText(R.id.must, "지정");
        } else {
            remoteView.setViewVisibility(R.id.must, View.INVISIBLE);
        }


//        remoteView.setViewVisibility(R.id.usang, receiveListItem.COSTNCOST.equals("1") ? View.VISIBLE : View.GONE);
//        // 미처리
//        remoteView.setViewVisibility(R.id.michuri, receiveListItem.PROGRESS_NO.equals("3") ? View.VISIBLE : View.GONE);
//        remoteView.setTextViewText(R.id.custName, receiveListItem.USER_NAME);
//
//        // 초도
//        remoteView.setViewVisibility(R.id.chodo, receiveListItem.ETC != null && receiveListItem.ETC.equals("초도") ? View.VISIBLE : View.GONE);
//
//        // 장기
//        remoteView.setViewVisibility(R.id.longTerm, !TextUtils.isEmpty(receiveListItem.LONG_TERM_REQ.trim()) ? View.VISIBLE : View.GONE);
//
//
//        // 민원
//        remoteView.setViewVisibility(R.id.minwon, receiveListItem.IS_COMPLAIN != null && receiveListItem.IS_COMPLAIN.equals("Y") ? View.VISIBLE : View.GONE);
//
//        // 자재
//        remoteView.setViewVisibility(R.id.jajae,
//                (receiveListItem.MAT_REQUEST_CONFIRM != null && receiveListItem.MAT_REQUEST_CONFIRM.equals("2")) ||
//                        (receiveListItem.MAT_REQUEST_CONFIRM_2 != null && receiveListItem.MAT_REQUEST_CONFIRM_2.equals("Y")) ? View.VISIBLE : View.GONE);
//

        Intent i = new Intent();
        Bundle extras = new Bundle();

        extras.putString(ServiceList.EXTRA_WORD, receiveListItem.HPHONE_NO);
        extras.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        i.putExtras(extras);
        remoteView.setOnClickFillInIntent(R.id.widgetLayout, i);


        return remoteView;
    }


    private void getTodayList() {
        ListNetManager mListNetManager = new ListNetManager();
        String date = SDF.yyyymmdd_2.format(Calendar.getInstance().getTime());
        Log.d("date : " + date);
        mListNetManager.appReceiveList(date)
                .enqueue(new Callback<ShippingList>() {
                             @Override
                             public void onResponse(Call<ShippingList> call, Response<ShippingList> response) {
                                 if (response.body().resultData != null) {
                                     mResultDataList = (ArrayList<ShippingList.resultData.list>) response.body().resultData.list;

                                     if (mResultDataList == null || mResultDataList.isEmpty())
                                         return;
                                     Log.d("mResultDataList: ", mResultDataList.size());
//                                 mResultDataList.sort();
                                     Collections.sort(mResultDataList, (o1, o2) -> o1.PROMISE_TIME.compareTo(o2.PROMISE_TIME));

                                 }
                             }

                             @Override
                             public void onFailure(Call<ShippingList> call, Throwable t) {

                             }

                         }
                );
    }


    private void widgetLogin(Context context) {
        if (TextUtils.isEmpty(Auth.getId()))
            return;
        LoginManager mLoginManger = new LoginManager();
        Log.d("getId : " + Auth.getId());
        Log.d("getPw : " + Auth.getPw());

        mLoginManger.login(Auth.getId(), Auth.getPw(), TM.getLine1Number(context))
                .enqueue(new Callback<login.Data>() {
                    @Override
                    public void onResponse(Call<login.Data> call, Response<login.Data> response) {
//                        Log.d("response.body().resultData: " + response.body().resultData.toString());
                        if (response.body().resultData != null) {
                            ServiceGenerator.JSESSIONID = response.body().resultData.SID;
                            getTodayList();
                        }
                    }

                    @Override
                    public void onFailure(Call<login.Data> call, Throwable t) {

                    }
                });
    }

    private String getVersion() {
        String version = "";
        PackageInfo pInfo;
        try {
            pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }


    @Override
    public void onCreate() {
        if (mResultDataList == null)
            widgetLogin(mContext);

    }

    @Override
    public void onDestroy() {
    }

    @Override
    public RemoteViews getLoadingView() {
        return (null);
    }

    @Override
    public int getViewTypeCount() {
        return (1);
    }

    @Override
    public long getItemId(int position) {
        return (position);
    }

    @Override
    public boolean hasStableIds() {
        return (true);
    }

    @Override
    public void onDataSetChanged() {
        widgetLogin(mContext);
    }


}
