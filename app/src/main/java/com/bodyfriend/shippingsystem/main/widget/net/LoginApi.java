package com.bodyfriend.shippingsystem.main.widget.net;


import com.bodyfriend.shippingsystem.main.login.net.login;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ts.ha on 2017-04-24.
 */
public interface LoginApi {

    @FormUrlEncoded
    @POST(NET_API.LOGIN_APP_LOGIN_JSON)
    Call<login.Data> login(
            @Field("admin_id") String admin_id
            , @Field("admin_pwd") String admin_pwd
            , @Field("admin_telno") String admin_telno
    );


}
