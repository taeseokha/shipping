package com.bodyfriend.shippingsystem.main.login.data;

public class AuthenticationCheckRequest {
    private String authCode;
    private String msgId;

    public AuthenticationCheckRequest(String authCode, String msgId) {
        this.msgId = msgId;
        this.authCode = authCode;
    }
}
