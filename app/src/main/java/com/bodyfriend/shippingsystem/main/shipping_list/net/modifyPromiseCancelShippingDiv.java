package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

/**
 * 약속 불가
 */
public class modifyPromiseCancelShippingDiv extends BFEnty {
    public modifyPromiseCancelShippingDiv(String m_shippingseq, String m_promise, String m_promisefailcd, String m_promisefailps) {
        setUrl("mobile/api/appModifyShippingDiv.json");
        setParam("m_shippingseq", m_shippingseq, "m_promise", m_promise, "m_promisetime", "", "m_promisefailcd", m_promisefailcd, "m_promisefailps", m_promisefailps);
    }

    public Data data;

    public static class Data {
        public int resultCode;
        public String resultMsg;
    }

}
