package com.bodyfriend.shippingsystem.main.shipping_list;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;

public class BackSNActivity extends BFActivity {

	public final static int RESULT_CODE = 2015060201;

	public static class RESULT {
		/**
		 * S/N
		 */
		public final static String BACK_SN = "BACK_SN";
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.back_sn_activity);
	}

	@Override
	protected void onLoadOnce() {
		super.onLoadOnce();

		setFinish(R.id.close);
		setOnClickListener(R.id.ok, onOkClickListener);

	}

	@Override
	public boolean check() {

		if (text(R.id.back_sn).length() == 0) {
			toast("회수 S/N을 입력해 주세요.");
			return false;
		}

		return true;
	};

	private OnClickListener onOkClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (check() == false) {
				return;
			}

			Intent data = new Intent();
			data.putExtra(RESULT.BACK_SN, text(R.id.back_sn)); // 비고
			setResult(RESULT_OK, data);
			finish();
		}
	};

	@Override
	public void onBackPressed() {
		finish();
	}
}
