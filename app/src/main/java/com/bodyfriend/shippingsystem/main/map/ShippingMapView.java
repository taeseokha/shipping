package com.bodyfriend.shippingsystem.main.map;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.util.PP;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.map.presenter.ShippingMapPresenter;
import com.bodyfriend.shippingsystem.main.map.presenter.ShippingMapPresenterImpl;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingList;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

public class ShippingMapView extends BFActivity implements View.OnClickListener, ShippingMapPresenter.View {

    public static final String MSG_STRING = "MSG_STRING";
    private static final String TAG = "ShippingMapView";
    // 배송일자
    private static String mDueDate;
    //    private static final String TAG = "ShippingMapActivity";
    // 맵 사용을 위한 퍼미션 요청 코드
    private final int REQUESTCODE_FOR_MAP_PERMISSION = 10;
    // 검색 시작일
    protected Calendar mStartCalendar = Calendar.getInstance();
    // 구글맵
    private GoogleMap mGoogleMap;
    // 프로그레스 다이얼로그
    private ProgressDialog asyncDialog;
    //    private RecyclerView mVisitRecycler;
    private LinearLayout mCustomerInfoActionView;
    private TextView mCustomerAddress;
    private TextView mCustomerName;
    private ShippingMapPresenterImpl mShippingMapPresenter;
    CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            int id = buttonView.getId();
            if (isChecked) {
                if (id == R.id.radioButtonToday) {
                    Calendar mDueDateCalendar = Calendar.getInstance();
                    mDueDate = SDF.yyyymmdd_1.format(mDueDateCalendar.getTime());
                    mShippingMapPresenter.reqShippingDivList(mDueDate);
                    if (mGoogleMap != null)
                        mGoogleMap.clear();
                } else if (id == R.id.radioButtonTomorrow) {
                    Calendar mDueDateCalendar = Calendar.getInstance();
                    mDueDateCalendar.add(Calendar.DATE, +1);
                    mDueDate = SDF.yyyymmdd_1.format(mDueDateCalendar.getTime());
                    mShippingMapPresenter.reqShippingDivList(mDueDate);
                    assert mGoogleMap != null;
                    if (mGoogleMap != null)
                        mGoogleMap.clear();
                } else if (id == R.id.radioButtonDayAfterTomorrow) {
                    Calendar mDueDateCalendar = Calendar.getInstance();
                    mDueDateCalendar.add(Calendar.DATE, +2);
                    mDueDate = SDF.yyyymmdd_1.format(mDueDateCalendar.getTime());
                    mShippingMapPresenter.reqShippingDivList(mDueDate);
                    if (mGoogleMap != null)
                        mGoogleMap.clear();
                }
            }
        }
    };
    private ShippingList.resultData.list deliveryItem;
    private LatLngBounds.Builder mBounds;
    private LocationWrapper locationWrapper;
    private RadioButton mRadioDayAferTomorrow;
    private RadioButton mRadioTomorrow;
    private RadioButton mRadioToday;
    private Location mLastLocation;
    LocationSource.OnLocationChangedListener locationListener = new LocationSource.OnLocationChangedListener() {
        public void onLocationChanged(Location l) {
            mLastLocation = l;
        }
    };
    private CameraUpdate mCameraUpdateFactory;
    private Handler showProgress = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (!ShippingMapView.this.isFinishing() && asyncDialog != null) {
                try {
                    if (msg.getData() != null) {
                        Bundle bundle = msg.getData();
                        asyncDialog.setMessage(bundle.getString(MSG_STRING));
                    }

                    asyncDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return true;
        }
    });
    OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {

        @Override
        public void onMapReady(GoogleMap googleMap) {
            // 구글맵을 맴버변수에 참조시킨다.
            mGoogleMap = googleMap;

            double longitude = Double.parseDouble(TextUtils.isEmpty(PP.LONGITUDE.getString()) ? "0" : PP.LONGITUDE.getString());
            double latitude = Double.parseDouble(TextUtils.isEmpty(PP.LATITUDE.getString()) ? "0" : PP.LATITUDE.getString());

            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 13));
            if (ActivityCompat.checkSelfPermission(ShippingMapView.this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ShippingMapView.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.setOnMarkerClickListener((Marker marker) -> {
                mCustomerInfoActionView.setVisibility(View.VISIBLE);
                deliveryItem = (ShippingList.resultData.list) marker.getTag();
                assert deliveryItem != null;

                mCustomerName.setText(deliveryItem.CUST_NAME);
                mCustomerAddress.setText(deliveryItem.INSADDR);


                final Handler handler = new Handler();
                final long start = SystemClock.uptimeMillis();
                final long duration = 1500;

                final Interpolator interpolator = new BounceInterpolator();

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        long elapsed = SystemClock.uptimeMillis() - start;
                        float t = Math.max(
                                1 - interpolator.getInterpolation((float) elapsed / duration), 0);
                        marker.setAnchor(0.5f, 1.0f + 2 * t);

                        if (t > 0.0) {
                            // Post again 16ms later.
                            handler.postDelayed(this, 16);
                        }
                    }
                });
                return false;
            });

            mGoogleMap.setOnMapClickListener(latLng -> mCustomerInfoActionView.setVisibility(View.GONE));

            Bundle bundle = new Bundle();
            bundle.putString(MSG_STRING, "배송 리스트 요청 중 ...");
            Message message = new Message();
            message.setData(bundle);
            showProgress.sendMessage(message);

            Calendar mDueDateCalendar = Calendar.getInstance();
            mDueDate = SDF.yyyymmdd_1.format(mDueDateCalendar.getTime());

            mShippingMapPresenter.reqShippingDivList(mDueDate);
        }
    };
    private ViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_marker);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarTb);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

//        RecyclerView mVisitRecycler = (RecyclerView) findViewById(R.id.visitRecycler);

        mCustomerInfoActionView = (LinearLayout) findViewById(R.id.customerInfoActionView);
        mCustomerName = (TextView) findViewById(R.id.customerName);
        mCustomerAddress = (TextView) findViewById(R.id.customerAddress);

        mRadioToday = (RadioButton) findViewById(R.id.radioButtonToday);
        mRadioTomorrow = (RadioButton) findViewById(R.id.radioButtonTomorrow);
        mRadioDayAferTomorrow = (RadioButton) findViewById(R.id.radioButtonDayAfterTomorrow);

        mRadioToday.setOnCheckedChangeListener(onCheckedChangeListener);
        mRadioTomorrow.setOnCheckedChangeListener(onCheckedChangeListener);
        mRadioDayAferTomorrow.setOnCheckedChangeListener(onCheckedChangeListener);

        mShippingMapPresenter = new ShippingMapPresenterImpl(ShippingMapView.this);
        mShippingMapPresenter.setView(this);
        deliveryItem = new ShippingList.resultData.list();
        mShippingMapPresenter.setModel(deliveryItem);

        findViewById(R.id.customerInfo).setOnClickListener(this);
        findViewById(R.id.customerCall).setOnClickListener(this);
        findViewById(R.id.customerPromise).setOnClickListener(this);

        mapFragment.getMapAsync(onMapReadyCallback);

//        mStartCalendar.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.DATE) - 5);





        mBounds = new LatLngBounds.Builder();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rout_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.rout_map) {
            startActivity(new Intent(this, RouteMapActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationWrapper = new LocationWrapper(this);
        locationWrapper.setAccuracyFilterEnabled(true, 1000);
        locationWrapper.registerOnLocationChangedListener(locationListener);
        locationWrapper.requestUpdates();

    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();
        // 프로그래스 다이얼로그를 셋업한다.
        setupProgress();
//        mStartCalendar.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH) - 1);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        LatLng BODYFRIEND_POSITION = mGoogleMap.getCameraPosition().target;
//        PP.LONGITUDE.set(Double.valueOf(BODYFRIEND_POSITION.longitude).toString());
//        PP.LATITUDE.set(Double.valueOf(BODYFRIEND_POSITION.latitude).toString());
        asyncDialog.dismiss();

        locationWrapper.cancelUpdates();
        locationWrapper.unregisterOnLocationChangedListener(locationListener);
    }

    /**
     * 프로그래스 다이얼로그를 셋업한다.
     */
    private void setupProgress() {
        asyncDialog = new ProgressDialog(mContext);
        asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    public void hideProgress() {
        super.hideProgress();
        if (asyncDialog != null) {
            runOnUiThread(() -> asyncDialog.hide());
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.customerCall) {
            mShippingMapPresenter.onCall(deliveryItem.HPHONE_NO);
        } else if (id == R.id.customerInfo) {
            mShippingMapPresenter.onDetailView(deliveryItem.SHIPPING_SEQ);
        } else if (id == R.id.customerPromise) {
            if (mLastLocation != null) {
                mShippingMapPresenter.connectNavi(deliveryItem, mLastLocation);
            } else {
                Toast.makeText(mContext, "내위치가 정확하지 않습니다.", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void sendError(VolleyError error) {
        hideProgress();
    }

    @Override
    public void sendResponseShippingDivList(List<ShippingList.resultData.list> data) {
        Log.d(TAG, "sendResponseShippingDivList: ");
        hideProgress();

        // data 에러가 없다면 내이버 geocoder 수행

        mShippingMapPresenter.getGeocoder(data);
    }


    @Override
    public void addMarker(ShippingList.resultData.list deliveryItem) {
        MarkerOptions markerOptions = new MarkerOptions();
        LatLng position = new LatLng(deliveryItem.location.getLongitude(), deliveryItem.location.getLatitude());
        StringBuffer customerInfo = new StringBuffer();
        if (deliveryItem.MUST_FLAG.equals("Y")) {
            customerInfo.append("지정건\n");
        }

        customerInfo.append(deliveryItem.CUST_NAME);
//        customerInfo.append(deliveryItem.SHIPPING_TYPE);
        if (!TextUtils.isEmpty(deliveryItem.PROMISE_TIME)) {
            try {
                customerInfo.append("\n").append(SDF.hhmm_.format(SDF.hhmm.parseDate(deliveryItem.PROMISE_TIME)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            customerInfo.append("\n 미지정");
        }

        IconGenerator iconFactory = new IconGenerator(ShippingMapView.this);
        setStyleMapIcon(deliveryItem, iconFactory);


        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(customerInfo))).
                position(position)
                .anchor(iconFactory.getAnchorU(), iconFactory.
                        getAnchorV());

        Marker marker = mGoogleMap.addMarker(markerOptions);
        marker.setTag(deliveryItem);

        mBounds.include(new
                LatLng(deliveryItem.location.getLongitude(), deliveryItem.location.getLatitude()));

        mCameraUpdateFactory = CameraUpdateFactory.newLatLngBounds(mBounds.build(), 100);
        if (mGoogleMap != null)
            mGoogleMap.moveCamera(mCameraUpdateFactory);
    }

    private void setStyleMapIcon(ShippingList.resultData.list deliveryItem, IconGenerator iconFactory) {
        switch (deliveryItem.SHIPPING_TYPE) {
            case "배송":
                iconFactory.setStyle(IconGenerator.STYLE_DEIVIRY);
                break;
            case "맞교체":
                iconFactory.setStyle(IconGenerator.STYLE_CHAGNGE);
                break;
            case "계약철회":
                iconFactory.setStyle(IconGenerator.STYLE_CANCEL);
                break;
            case "이전요청":
                iconFactory.setStyle(IconGenerator.STYLE_RELOCATION);
                break;
            case "재설치":
                iconFactory.setStyle(IconGenerator.STYLE_AS);
                break;
            default:
                iconFactory.setStyle(IconGenerator.STYLE_DEIVIRY);
                break;
        }
    }
}
