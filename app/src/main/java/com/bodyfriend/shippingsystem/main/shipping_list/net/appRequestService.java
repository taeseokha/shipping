package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

/**
 * 서비스 접수
 * 
 */
public class appRequestService extends BFEnty {

	/**
	 * @param m_producttype
	 *            제품구분
	 * @param m_productsn
	 *            S/N
	 * @param m_custname
	 *            고객명
	 * @param m_tel
	 *            전화
	 * @param m_phone
	 *            휴대전화
	 * @param m_address
	 *            주소
	 * @param m_productname
	 *            제품명
	 * @param m_serviceps
	 *            서비스접수 비고
	 */
	public appRequestService(String m_producttype, String m_productsn, String m_custname, String m_tel, String m_phone, String m_address, String m_productname, String m_serviceps, String m_servicemonth, String m_purchaseoffice) {
		setUrl("mobile/api/appRequestService.json");
		setParam("m_producttype", m_producttype, "m_productsn", m_productsn, "m_custname", m_custname, "m_tel", m_tel, "m_phone", m_phone, "m_address", m_address, "m_productname", m_productname, "m_serviceps", m_serviceps, "m_servicemonth", m_servicemonth, "m_purchaseoffice", m_purchaseoffice);
	}

	public Data data;

	public static class Data {
		public String resultMsg;
		public int resultCode;
	}

//	05-20 15:34:02.499: W/_NETLOG_IN(1322):     "resultMsg": "수정에 성공 하였습니다",
//	05-20 15:34:02.499: W/_NETLOG_IN(1322):     "resultCode": 1

	
	// * 파라미터 :
	// m_producttype (제품구분)
	// m_productsn (S/N)
	// m_custname (고객명)
	// m_tel (전화)
	// m_phone (휴대전화)
	// m_address (주소)
	// m_productname (제품명)
	// m_serviceps (서비스접수 비고)
}
