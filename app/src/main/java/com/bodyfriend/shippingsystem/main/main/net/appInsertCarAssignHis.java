package com.bodyfriend.shippingsystem.main.main.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

/**
 * Created by 이주영 on 2017-03-23.
 */
public class appInsertCarAssignHis extends BFEnty {
    //    car_seq=102&car_number=66거4146
    public appInsertCarAssignHis(String car_seq, String car_number) {
        setUrl("mobile/api/appInsertCarAssignHis.json");
        setParam("car_seq", car_seq);
        setParam("car_number", car_number);
    }

    public Data data;

    public static class Data {
        public String resultCode;
        public String resultMsg;
    }
}
