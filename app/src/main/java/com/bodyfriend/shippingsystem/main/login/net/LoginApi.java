package com.bodyfriend.shippingsystem.main.login.net;

import com.bodyfriend.shippingsystem.main.delivery_order.net.BfData;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginApi {


    @FormUrlEncoded
    @POST("login/appLogin.json")
    Observable<login.Data> login(
            @Field("admin_id") String format,
            @Field("admin_pwd") String name,
            @Field("appType") String carNumber

    );

    @FormUrlEncoded
    @POST("login/appLogin.json")
    Observable<login.Data> login(
            @Field("admin_id") String format,
            @Field("admin_pwd") String name
    );


    @FormUrlEncoded
    @POST("login/reqTempKey.json")
    Observable<BfData> reqTempKey(
            @Field("admin_id") String admin_id,
            @Field("telNo") String telNo
    );



    @FormUrlEncoded
    @POST("login/selectTempKey.json")
    Observable<BfData> authKeyCheck(
            @Field("admin_id") String admin_id,
            @Field("tempKey") String telNo
    );



    @FormUrlEncoded
    @POST("login/updateAdminPwd.json")
    Observable<BfData> updateAdminPwd(
            @Field("admin_id") String admin_id,
            @Field("admin_pwd") String telNo
    );
}
