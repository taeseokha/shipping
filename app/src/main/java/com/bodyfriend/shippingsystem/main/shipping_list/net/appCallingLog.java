package com.bodyfriend.shippingsystem.main.shipping_list.net;


import com.bodyfriend.shippingsystem.base.BFEnty;

public class appCallingLog extends BFEnty {

	public appCallingLog(String bodyNo , String customerName, String callNumber){
		setUrl("mobile/api/appInsertLog.json");
		
		String param = String.format("%s, %s, %s", bodyNo, customerName, callNumber);
		setParam("logtype", "T", "logtable", "TB_LOG", "logtdata", param);
	}
}
