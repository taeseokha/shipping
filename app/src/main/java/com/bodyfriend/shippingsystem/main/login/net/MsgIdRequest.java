package com.bodyfriend.shippingsystem.main.login.net;

public class MsgIdRequest {
    public String msgid;
    public String authCode;

    public MsgIdRequest(String authCode, String msgid) {
        this.msgid = msgid;
        this.authCode = authCode;
    }

    public MsgIdRequest( String msgid) {
        this.msgid = msgid;

    }
}
