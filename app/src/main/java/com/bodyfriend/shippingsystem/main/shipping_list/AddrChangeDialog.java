package com.bodyfriend.shippingsystem.main.shipping_list;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.databinding.AddrChangeDialogBinding;

public class AddrChangeDialog extends Dialog implements View.OnClickListener {


    private final Context mContext;
    private AddrChangeDialogBinding binding;
    private String addr;

    public AddrChangeDialog(@NonNull Context context) {
        super(context);
        mContext = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.8f;
        getWindow().setAttributes(lpWindow);

        binding =
                DataBindingUtil.inflate(
                        LayoutInflater.from(getContext()), R.layout.addr_change_dialog, null, false);
        binding.addrTv.setText(addr);
        setContentView(binding.getRoot());
    }

    @Override
    public void onClick(View v) {

    }


    public void setAddr(String addr) {
        this.addr = addr;

    }
}
