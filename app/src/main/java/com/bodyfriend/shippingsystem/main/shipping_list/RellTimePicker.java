package com.bodyfriend.shippingsystem.main.shipping_list;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.NumberPicker;

import com.bodyfriend.shippingsystem.R;



@SuppressLint("ValidFragment")
public class RellTimePicker extends AppCompatDialogFragment {

    private final String TAG = RellTimePicker.class.getSimpleName();

    public interface OnTimePickListener {
        void onTimePick(int position);
    }

    private Builder mBuilder;
    private OnTimePickListener mOnDatePickListener;
    private NumberPicker mTimePicker;
    private View mRoot;

    public int hourOfDay = 0;


    public RellTimePicker(Builder builder) {
        mBuilder = builder;
    }

    public void show(FragmentManager fragmentManager, OnTimePickListener onDatePickListener) {
        if (onDatePickListener == null)
            throw new NullPointerException("onDatePickListener must be not null");

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.add(this, getTag());
        ft.commitAllowingStateLoss();
        mOnDatePickListener = onDatePickListener;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        setupDialogSetting(dialog);
        setupLayout(dialog);
        setupButtonEvents();
    }

    /**
     * 레이아웃 세팅
     */
    private void setupLayout(Dialog dialog) {
        mRoot = LayoutInflater.from(mBuilder.mContext).inflate(R.layout.dialog_timeinteval, null);
        mTimePicker = (NumberPicker) mRoot.findViewById(R.id.numberPicker);

        mTimePicker.setEnabled(true);
        mTimePicker.setMinValue(0);
        mTimePicker.setMaxValue(24);
        mTimePicker.setDisplayedValues(getResources().getStringArray(R.array.timeInterval));
        dialog.setContentView(mRoot);
        setupTimePicker();
    }


    /**
     * 타임피커 세팅
     */
    @SuppressLint("DefaultLocale")
    private void setupTimePicker() {
        mTimePicker.setOnValueChangedListener((picker, oldVal, newVal) -> RellTimePicker.this.hourOfDay = newVal);

        /*mTimePicker.setOnValueChangedListener((view, hourOfDay, minute) -> {
            Log.d(TAG, String.format("hourOfDay : %s , minute : %s", hourOfDay, minute));
            RellTimePicker.this.hourOfDay = hourOfDay;
            RellTimePicker.this.minute = minute;

            ((TextView) mRoot.findViewById(R.id.timeInterval)).setText(String.format("예정 시각 : %02d시 ~ %02d시", hourOfDay, (hourOfDay + 2)));
        });*/

    }

    /**
     * 다이얼로그 셋팅
     */
    private void setupDialogSetting(Dialog dialog) {
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    /**
     * 확인, 취소 버튼 세팅
     */
    private void setupButtonEvents() {
        mRoot.findViewById(R.id.ok).setOnClickListener(v -> {
            mOnDatePickListener.onTimePick(hourOfDay);
            dismiss();
        });
        mRoot.findViewById(R.id.cancel).setOnClickListener(v -> dismiss());
    }


    /**
     * 빌더 클래스
     */
    public static class Builder {
        Context mContext;
        int hour = -1;
        int minute = -1;
        int TIME_PICKER_INTERVAL = 5;

        public Builder(Context context) {
            this.mContext = context;
        }

        Builder setTime(int hour, int minute) {
            this.hour = hour;
            this.minute = minute / TIME_PICKER_INTERVAL;
            return this;
        }

        public RellTimePicker create() {
            return new RellTimePicker(this);
        }
    }
}
