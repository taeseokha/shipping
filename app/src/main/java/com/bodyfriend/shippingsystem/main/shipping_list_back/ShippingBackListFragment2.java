package com.bodyfriend.shippingsystem.main.shipping_list_back;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.common.BaseFragment;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.CC;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.shipping_list_back.detail.ShippingBackMasterDetailActivity;
import com.bodyfriend.shippingsystem.main.shipping_list.ShippingListFragment2;
import com.bodyfriend.shippingsystem.main.main.MainActivity;
import com.bodyfriend.shippingsystem.main.shipping_list_back.net.appShippingBackList;
import com.bodyfriend.shippingsystem.main.shipping_list.net.searchCodeListArea;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by 이주영 on 2016-08-01.
 */
public class ShippingBackListFragment2 extends ShippingListFragment2 {

    private String mInsAddr;

    private View mSearch;

    public void setMainActivityControll(MainActivity.MainActivityControll mainActivityControll) {
        this.mainActivityControll = mainActivityControll;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mStartCalendar.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH) - 1);
        return inflater.inflate(R.layout.fragment_shpping_finish, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();

        String[] arrSpinner = {"회수일", "분배일", "접수일", "지정일"};
        setSpinner(R.id.s_datetype, arrSpinner);
    }

    private TextWatcher watcherDateStart = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            setOnClickListener(R.id.dateEnd, new BaseFragment.OnDateClickListener((TextView) findViewById(R.id.dateEnd), SDF.mmdd__.format, mEndCalendar, mStartCalendar.getTimeInMillis(), -1));
        }
    };


    @Override
    protected void reqShippingDivList() {

        if (!(this instanceof CC.ILOGIN) && !Auth.isLogin()) {
            return;
        }
        final Spinner spinner = (Spinner) findViewById(R.id.s_datetype);
        final int spinnerPosition = spinner.getSelectedItemPosition();
        int s_datetype = 0;
        switch (spinnerPosition) {
            case 0: // 회수
                s_datetype = 3;
                break;
            case 1: // 배정
                s_datetype = 0;
                break;
            case 2: // 접수
                s_datetype = 1;
                break;
            case 3: // 지정
                s_datetype = 2;
                break;

            default:
                break;
        }

        String dateStart = SDF.yyyymmdd_2.format(mStartCalendar.getTime());
        String dateEnd = SDF.yyyymmdd_2.format(mEndCalendar.getTime());
        Spinner mS_area = (Spinner) findViewById(R.id.s_area);
        String selectedArea = (String) mS_area.getSelectedItem();
        String s_area = null;
        for (searchCodeListArea.Data.resultData d : mSearchCodeListArea.data.resultData) {
            if (d.DET_CD_NM.equals(selectedArea)) {
                s_area = d.DET_CD;
                break;
            }
        }
        String s_custname = text(R.id.s_custname);
        String s_addr = text(R.id.s_addr);

        Net.async(new appShippingBackList(s_datetype, dateStart, dateEnd, s_area, s_custname, s_addr)).setOnNetResponse(onNetResponse);

    }


    private List<appShippingBackList.Data.resultData.list> mData;
    private Net.OnNetResponse<appShippingBackList> onNetResponse = new Net.OnNetResponse<appShippingBackList>() {

        @Override
        public void onResponse(appShippingBackList response) {
            hideProgress();

            if (Auth.checkSession(response.data.resultCode)) {
                Auth.startLogin(mContext);
                return;
            } else if (response.data.resultCode == -1) {
                toast(response.data.resultMsg);
                return;
            }

            mData = response.data.resultData.list;
            ShippingListAdapter listAdapter = new ShippingListAdapter(mContext, response.data.resultData.list);
            mList.setAdapter(listAdapter);

            if (mData.size() > 0) {
                hideSearch();
            } else {
                showSearch();
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }
    };

    public class ShippingListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private ArrayList<appShippingBackList.Data.resultData.list> mItemList;

        public ShippingListAdapter(Context context, List<appShippingBackList.Data.resultData.list> list) {
            this.mItemList = (ArrayList<appShippingBackList.Data.resultData.list>) list;
            this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return mItemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            PersonViewHolder viewHolder;

            // 캐시된 뷰가 없을 경우 새로 생성하고 뷰홀더를 생성한다
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.shipping_back_list_item, parent, false);

                viewHolder = new PersonViewHolder();
                viewHolder.div_date = (TextView) convertView.findViewById(R.id.div_date);
                viewHolder.insaddr = (TextView) convertView.findViewById(R.id.insaddr);
                viewHolder.cust_name = (TextView) convertView.findViewById(R.id.cust_name);
                viewHolder.product_name = (TextView) convertView.findViewById(R.id.product_name);
                viewHolder.phone = (ImageButton) convertView.findViewById(R.id.phone);
                viewHolder.index = (TextView) convertView.findViewById(R.id.index);

                convertView.setTag(viewHolder);
            }
            // 캐시된 뷰가 있을 경우 저장된 뷰홀더를 사용한다
            else {
                viewHolder = (PersonViewHolder) convertView.getTag();
            }

            try {
                viewHolder.div_date.setText(SDF.mmddE.format(SDF.yyyymmdd_1.parse(mItemList.get(position).BACK_DATE)));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            viewHolder.index.setText((position + 1) + "");

            viewHolder.product_name.setText(mItemList.get(position).PRODUCT_NAME);
            viewHolder.cust_name.setText(mItemList.get(position).CUST_NAME);

            viewHolder.insaddr.setText(String.format("주소 : %s", mItemList.get(position).INSADDR));

            viewHolder.phone.setVisibility(mItemList.get(position).PHONE_NO.trim().length() > 0 ? View.VISIBLE : View.GONE);
            viewHolder.phone.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(String.format("tel:%s", mItemList.get(position).PHONE_NO)));
                    v.getContext().startActivity(intent);
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {

                @SuppressLint("NewApi")
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ShippingBackMasterDetailActivity.class);
                    intent.putExtra("shipping_seq", mItemList.get(position).SHIPPING_SEQ);
                    startActivity(intent);
                }
            });

            return convertView;
        }

        public class PersonViewHolder {
            public TextView index;
            public TextView div_date; // 방문예정일
            public TextView insaddr; // 제품명
            public TextView cust_name; // 소비자명
            public TextView product_name;
            public ImageButton phone; // 요청 사항
        }
    }
}