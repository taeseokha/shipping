package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.io.File;


public class modifyShippingDiv extends BFEnty {
	//name="m_shippingseq" G201504-0029
	//name="m_rcvdt" 2015-04-01
	//name="m_custname" 김영숙
	//name="m_telno"
	//name="m_celno" 010-4852-4015
	//name="m_faxno"
	//name="m_addr" 충남 천안시 서북구 천안대로 969 두정역코아루스위트아파트 105동 1003호
	//name="productnm" 팬텀(레드)
	//name="m_purchasingoffice" 현대
	//name="m_qty" 1
	//name="m_service" 39
	//name="m_freegift1" 라이카크로커스
	//name="m_freegift2"
	//name="m_compcheck"
	//name="m_duedt"
	//name="screen1"
	//name="screen2"
	//name="screen3"
	//name="screen4"
	//name="screen5"
	//name="screen6"
	//name="m_promise" Y
	//name="m_shippingps" test
	//name="m_complete" Y
	//name="m_completedt"
	//name="m_productsn"
	
	
//	m_shippingseq
//	m_promise
//	m_shippingps
//	m_complete
//	m_completedt
//	m_productsn
	/**
	 * @param m_shippingseq
	 * @param m_promise
	 * @param m_shippingps
	 * @param m_complete
	 * @param m_completedt
	 * @param m_productsn
	 * @param screen1
	 * @param screen2
	 * @param screen3
	 * @param screen4
	 * @param screen5
	 * @param screen6
	 * @param sign
	 * @param m_freegiftcheck1
	 * @param m_freegiftcheck2
	 * @param m_completenm	설치여부명
	 * @param m_productname	제품명
	 * @param m_productcode	제품코드
	 * @param m_custname	고객명
	 * @param m_confirmps 
	 * @param m_relationshipnm 
	 * @param screen_sign 
	 */
	public modifyShippingDiv(String m_shippingseq, String m_promise, String m_shippingps, String m_complete, String m_completedt, String m_productsn, File screen1, File screen2, File screen3, File screen4, File screen5, File screen6, File sign, String m_freegiftcheck1, String m_freegiftcheck2, String m_completenm, String m_productname, String m_productcode, String m_custname, String m_confirmps, String m_relationship, String m_relationshipnm, String m_backsn, String m_wateradaptor, String m_watercooking, String car_number) {
		setUrl("mobile/api/appModifyShippingDiv.json");

		
		setParam(
				"m_shippingseq", m_shippingseq, // "G201504-0029",
				"m_promise", m_promise, //"Y",
				"m_shippingps", m_shippingps, //"aaa",
				"m_complete", m_complete,
				"m_completedt", m_completedt, //"Y",
				"m_productsn", m_productsn, //"",
//				
//				"m_telno", m_telno , //"",
//				"m_celno", m_celno, //"010-4852-4015",
//				"m_faxno", m_faxno, //"",
//				"m_addr", m_addr, //"충남 천안시 서북구 천안대로 969 두정역코아루스위트아파트 105동 1003호",
//				"productnm", productnm, //"팬텀(레드)",
//				"m_purchasingoffice", m_purchasingoffice, //"현대",
//				"m_qty", m_qty, //"1",
//				"m_service",m_service, //"39",
				"m_freegiftcheck1", m_freegiftcheck1, //"라이카크로커스",
				"m_freegiftcheck2", m_freegiftcheck2, //"",
//				"m_compcheck", m_compcheck, //"",
//				"m_duedt", m_duedt, //"",
//				"m_promise", m_promise, //"Y",
//				"m_shippingps", m_shippingps, //"aaa",
//				"m_completedt", m_completedt, //"Y",
//				"m_productsn", m_productsn, //"",
				
				"screen1", screen1, // file,
				"screen2", screen2, // file,,
				"screen3", screen3, // file,,
				"screen4", screen4, // file,,
				"screen5", screen5, // file,,
				"screen6", screen6, // file,
				"screen_sign", sign // file,
				
				,"m_completenm", m_completenm
				,"m_productname", m_productname
				,"m_productcode", m_productcode
				,"m_custname", m_custname
				,"m_confirmps", m_confirmps
				,"m_relationship", m_relationship
				,"m_relationshipnm", m_relationshipnm
				,"m_backsn", m_backsn				
				,"m_wateradaptor", m_wateradaptor
				,"m_watercooking", m_watercooking
				,"car_number", car_number
				);
		
	}

	public Data data;

	public static class Data {
		public resultData resultData;

		public String resultMsg;
		public int resultCode;
		
		
		public class resultData{
			public int resultCode;
			public String resultMsg;
			public String SID;
			public String PER_CD;
			public String GROUP_CD;
			public String COMPANY_NM;
			public String ADMIN_NM;
			public String loginDate;
			public String ADMIN_ID;
		}
	}

}
