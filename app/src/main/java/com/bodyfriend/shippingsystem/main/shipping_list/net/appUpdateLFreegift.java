package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

/**
 * 사은품 저장
 */
public class appUpdateLFreegift extends BFEnty {

    /**
     * @param shipping_seq 배송seq
     * @param l_freegift   라텍스베개 코드
     */
    public appUpdateLFreegift(String shipping_seq, String l_freegift) {

        setUrl("mobile/api/appUpdateLFreegift.json");
        setParam("shipping_seq", shipping_seq);
        setParam("l_freegift", l_freegift);
    }

    public Data data;

    public static class Data {
        public String resultMsg;
        public int resultCode;
    }
}
