package com.bodyfriend.shippingsystem.main.shipping_list;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.BFDialog;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.databinding.ActivitySelectCarBinding;
import com.bodyfriend.shippingsystem.databinding.SelectDmanListItemBinding;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appCarList;
import com.bodyfriend.shippingsystem.main.shipping_list.net.appCarList.Data.resultData;

import java.util.ArrayList;
import java.util.List;

import util.SoundSearcher;

/**
 * 차량 선택 팝업
 */
public class SelectCarActivity extends BFActivity {

    public static class EXTRA {
        public static final String CAR_NUMBER = "CAR_NUMBER";
        public static final String APPCARLIST_DATA_RESULTDATA = "appCarList.Data.ResultData";

    }

    // 액티비티의 바인딩 객체
    private ActivitySelectCarBinding mBinding;
    // 차량번호 전문을 통해 가져온 모델
    public List<resultData> mData;
    // 차량번호를 보여줄 아답터
    private CarListAdapter mListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("SelectCarActivity :: onCreate");

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_car);
    }

    @Override
    protected void onParseExtra() {
        super.onParseExtra();
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();
        // 리스트에 emptyView를 지정한다
        mBinding.list.setEmptyView(mBinding.emptyView);
        // close를 선택하면 액티비티를 종료한다.
        mBinding.close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // 차량 번호를 입력하면 입력된 번호가 포함된 아이템들만 보여준다.
        mBinding.searchEdit.addTextChangedListener(watcher);
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        // 차량번호 조회 전체조회를 위해 빈값을 넣는다.
        Net.async(new appCarList("")).setOnNetResponse(onNetResponse);
    }


    private Net.OnNetResponse<appCarList> onNetResponse = new Net.OnNetResponse<appCarList>() {

        @Override
        public void onErrorResponse(VolleyError error) {
            hideProgress();
        }

        @Override
        public void onResponse(appCarList response) {
            mData = response.data.resultData;

            // 데이터를 세팅한다.
            setupData();
        }
    };

    /**
     * 데이터를 세팅한다.
     */
    private void setupData() {
        // 유효성체크
        if (mData == null) return;

        // 아답터가 null이면 아답터를 생성한다
        if (mListAdapter == null) {
            mListAdapter = new CarListAdapter(mData);
            mBinding.list.setAdapter(mListAdapter);
        } else { // 아답터가 이미 생성되어 있으면 리스트 아이템을 변경한다.
            mListAdapter.setItemList(mData);
            mListAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 아답터
     */
    public class CarListAdapter extends BaseAdapter {
        private ArrayList<resultData> mItemList;

        public void setItemList(List<resultData> list) {
            this.mItemList = (ArrayList<resultData>) list;
        }

        public CarListAdapter(List<resultData> list) {
            this.mItemList = (ArrayList<resultData>) list;
        }

        @Override
        public int getCount() {
            return mItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return mItemList.get(position);
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final resultData item = mItemList.get(position);
            SelectDmanListItemBinding sBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.select_dman_list_item, null, false);

            ViewHolder holder = new ViewHolder(sBinding, item);

            sBinding.getRoot().setOnClickListener(onItemClickListener);
            sBinding.getRoot().setTag(holder);
            return sBinding.getRoot();
        }

        /**
         * 아이템 선택시 차량번호를 리턴해준다.
         */
        private OnClickListener onItemClickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                ViewHolder h = (ViewHolder) v.getTag();
                // 정말요?
                showCheckPopup(h);
            }

            /**
             * 정말요?
             * @param h
             */
            private void showCheckPopup(final ViewHolder h) {
                BFDialog.newInstance(mContext).showDialog(String.format("%s를 선택하시겠습니까?", h.mItem.CAR_NUMBER), "선택", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent data = new Intent();
                        data.putExtra(EXTRA.CAR_NUMBER, h.mItem.CAR_NUMBER);
                        data.putExtra(EXTRA.APPCARLIST_DATA_RESULTDATA, h.mItem);
                        setResult(Activity.RESULT_OK, data);
                        finish();
                    }
                }, "취소", null);
            }
        };

        /**
         * 뷰홀더
         */
        public class ViewHolder {
            // 모델
            public resultData mItem;
            // 바인딩
            public SelectDmanListItemBinding mSbinding;

            public ViewHolder(SelectDmanListItemBinding binding, final resultData item) {
                mSbinding = binding;
                mItem = item;

                if (!item.ADMIN_INFO.isEmpty()) {
                    // 괄호안에 들어가는 내용에 대해서는 70퍼센트의 크기로 넣는다.
                    String str = String.format("%s(%s)", item.CAR_NUMBER, item.ADMIN_INFO);
                    SpannableString ss = new SpannableString(str);
                    ss.setSpan(new RelativeSizeSpan(0.725f), item.CAR_NUMBER.length(), str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    mSbinding.dmanName.setText(ss, TextView.BufferType.SPANNABLE);
                } else {
                    mSbinding.dmanName.setText(item.CAR_NUMBER);
                }
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            checkSearch();
        }

    };

    /**
     * 검색 창에 텍스트가 입력될때에 리스트를 재구성한다.
     */
    private void checkSearch() {

        String search = text(R.id.searchEdit);

        List<resultData> newDatas;

        if (search.length() == 0) {
            newDatas = mData;
        } else {
            newDatas = new ArrayList<>();
            String value;

            if (mData == null) return;
            for (resultData data : mData) {
                value = data.CAR_NUMBER;
                if (SoundSearcher.matchString(value, search)) {
                    newDatas.add(data);
                }
            }
        }
        if (newDatas != null) {
            mListAdapter.setItemList(newDatas);
            mListAdapter.notifyDataSetChanged();
        }
    }
}
