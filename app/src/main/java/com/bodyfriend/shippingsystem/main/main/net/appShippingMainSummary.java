package com.bodyfriend.shippingsystem.main.main.net;

import com.bodyfriend.shippingsystem.base.BFEnty;

import java.util.List;

/**
 * Created by 이주영 on 2016-08-04.
 */
public class appShippingMainSummary extends BFEnty {
    public appShippingMainSummary(String date) {
//        setUrl("/mobile/api/appShippingMainSummary.json");
        setUrl("mobile/api/appShippingMainSummaryNew.json");
        setParam("date", date);
        setParam("s_producttype", "M");
    }

    public Data data;

    public static class Data {
        public resultData resultData;

        public class resultData {

            public List<remainList> remainList;

            public class remainList {
                public String PRODUCT_NAME;
                public String PROMISE_TIME;
            }

            public int WEEK_CNT_5;
            public int WEEK_CNT_3;
            public int WEEK_CNT_4;
            public int WEEK_CNT_1;
            public int MONTH_CNT;
            public int WEEK_CNT_2;
            public int compCnt;
            public int remainCnt;
            public List<compList> compList;

            public class compList {
                public String PROMISE_TIME;
                public String PRODUCT_NAME;
            }

            public monthCnt monthCnt;

            public class monthCnt {
                public int delivery_of_month;
                public int change_of_month;
                public int cancel_of_month;
                public int relocation_of_month_1;
                public int relocation_of_month_0;
                public int reinstall_of_month;
                public int as_of_month;
                public int faulty_of_month;
                public int etc_of_month;
                public int moterized_bed; // 전동침대
                public int royal_frame;   // 고급형
                public int moterized_bed_etc;   // 전동침대기타
                public int royal_frame_etc;   // 로얄프레임기타

                @Override
                public String toString() {
                    return "monthCnt{" +
                            "delivery_of_month=" + delivery_of_month +
                            ", change_of_month=" + change_of_month +
                            ", cancel_of_month=" + cancel_of_month +
                            ", relocation_of_month_1=" + relocation_of_month_1 +
                            ", relocation_of_month_0=" + relocation_of_month_0 +
                            ", reinstall_of_month=" + reinstall_of_month +
                            ", as_of_month=" + as_of_month +
                            ", faulty_of_month=" + faulty_of_month +
                            ", etc_of_month=" + etc_of_month +
                            '}';
                }
            }

            public todayCnt todayCnt;

            public class todayCnt {
                public int COMPLETED_CNT;
                public int REMAIN_CNT;
            }


//            public List<compList> compList;
//
//            public class compList {
//                public String PROMISE_TIME;
//                public String PRODUCT_NAME;
//            }
//
//            public List<remainList> remainList;
//
//            public class remainList {
//                public String PRODUCT_NAME;
//                public String PROMISE_TIME;
//            }
        }
    }

//    "ResultData": {
//        "remainList": [
//        {
//            "PRODUCT_NAME": "레지나(초코브라운)"
//        },
//        {
//            "PROMISE_TIME": "",
//                "PRODUCT_NAME": "팬텀(레드)"
//        },
//        {
//            "PROMISE_TIME": "1700",
//                "PRODUCT_NAME": "프레지던트플러스(브라운)"
//        },
//        {
//            "PROMISE_TIME": "1800",
//                "PRODUCT_NAME": "파라오(골드)"
//        }
//        ],
//        "WEEK_CNT_5": "0",
//                "WEEK_CNT_3": "0",
//                "WEEK_CNT_4": "0",
//                "WEEK_CNT_1": "28",
//                "MONTH_CNT": "28",
//                "WEEK_CNT_2": "0",
//                "compCnt": 4,
//                "remainCnt": 4,
//                "compList": [
//        {
//            "PROMISE_TIME": "1200",
//                "PRODUCT_NAME": "프레지던트(블랙)"
//        },
//        {
//            "PROMISE_TIME": "1300",
//                "PRODUCT_NAME": "렉스엘(블랙)"
//        },
//        {
//            "PROMISE_TIME": "1400",
//                "PRODUCT_NAME": "레지나(초코브라운)"
//        },
//        {
//            "PROMISE_TIME": "1600",
//                "PRODUCT_NAME": "팬텀블랙에디션(레드)"
//        }
//        ]
//    }
}
