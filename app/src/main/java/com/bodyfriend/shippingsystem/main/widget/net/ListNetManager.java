package com.bodyfriend.shippingsystem.main.widget.net;

import com.bodyfriend.shippingsystem.base.net.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingList;

import retrofit2.Call;

/**
 * Created by 이주영 on 2017-05-23.
 */

public class ListNetManager {


    public Call<ShippingList> appReceiveList(String dateStart) {
        ListApi service = ServiceGenerator.createService(ListApi.class);
        return service.getReceiveList(2, dateStart, dateStart, "N", "M");
    }


}
