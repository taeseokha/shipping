package com.bodyfriend.shippingsystem.main.main;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFFragment2;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.base.util.Util;
import com.bodyfriend.shippingsystem.main.benefit.BenefitApi;
import com.bodyfriend.shippingsystem.main.benefit.BenefitData;
import com.bodyfriend.shippingsystem.main.benefit.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.main.net.appShippingMainSummary;
import com.bodyfriend.shippingsystem.main.schedule.ScheduleFragment1;
import com.bodyfriend.shippingsystem.main.shipping_list.ShippingListFragment2;
import com.bodyfriend.shippingsystem.main.shipping_list_finish.ShippingFinishListFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by 이주영 on 2016-08-01.
 */
public class MainFragment extends BFFragment2 {
    private MainActivity.MainActivityControll mainActivityControll;
    /**
     * 간략 데이터
     */
    private appShippingMainSummary.Data.resultData mAppShippingMainSummary;
    private ArrayList<CalendarItem> calendarItems;
    private View.OnClickListener onCalendarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ScheduleFragment1 scheduleFragment = new ScheduleFragment1();
            scheduleFragment.setMainActivityControll(mainActivityControll);
            mainActivityControll.changeFragment(scheduleFragment);
        }
    };
    private Net.OnNetResponse<appShippingMainSummary> onNetListener = new Net.OnNetResponse<appShippingMainSummary>() {
        @Override
        public void onErrorResponse(VolleyError error) {

        }

        @Override
        public void onResponse(appShippingMainSummary response) {
            mAppShippingMainSummary = response.data.resultData;
            dummyGraphData(mAppShippingMainSummary);
        }
    };
    private View.OnClickListener onDateClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            initCalendarViewStatus();
            CalendarItem item = (CalendarItem) v.getTag();
            item.setStatus(item.STATUS_CLICKED);
        }
    };
    private View.OnClickListener onGraphLayoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment = null;
            Bundle bundle;
            switch (v.getId()) {
                case R.id.graph1_layout: // 남은 배송건
                    fragment = new ShippingListFragment2();
                    ((ShippingListFragment2) fragment).setMainActivityControll(mainActivityControll);
                    break;
                case R.id.graph2_layout: // 배송 완료건
                    fragment = new ShippingFinishListFragment();
                    ((ShippingFinishListFragment) fragment).setMainActivityControll(mainActivityControll);
                    bundle = new Bundle();
                    bundle.putInt(ShippingFinishListFragment.EXTRA.MODE, ShippingFinishListFragment.EXTRA.MODE_TODAY);
                    fragment.setArguments(bundle);
                    break;
                case R.id.graph3_layout: // 당월 완료건
                    fragment = new ShippingFinishListFragment();
                    ((ShippingFinishListFragment) fragment).setMainActivityControll(mainActivityControll);
                    bundle = new Bundle();
                    bundle.putInt(ShippingFinishListFragment.EXTRA.MODE, ShippingFinishListFragment.EXTRA.MODE_THIS_MONTH);
                    fragment.setArguments(bundle);
                    break;
            }

            if (fragment != null) mainActivityControll.changeFragment(fragment);
        }
    };

    public void setMainActivityControll(MainActivity.MainActivityControll mainActivityControll) {
        this.mainActivityControll = mainActivityControll;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();
        if (mainActivityControll == null) {
//            getActivity(). getMainActivityControll
            mainActivityControll = ((MainActivity) getActivity()).getMainActivityControll();
            mainActivityControll.changeTitle(R.layout.title_layout);
        } else {
            mainActivityControll.changeTitle(R.layout.title_layout);
        }

        initScrollView();
        findViewById(R.id.calendar).setOnClickListener(onCalendarClickListener);

        findViewById(R.id.graph1_layout).setOnClickListener(onGraphLayoutClickListener);
        findViewById(R.id.graph2_layout).setOnClickListener(onGraphLayoutClickListener);
        findViewById(R.id.graph3_layout).setOnClickListener(onGraphLayoutClickListener);


    }

    private void initScrollView() {
        LinearLayout scrollLayout = (LinearLayout) findViewById(R.id.scrollLayout);

//        TextView text1 = (TextView) findViewById(R.id.text1);
//        text1.setText(String.format("Today at %s", SDF.hhmm_.now()));

        Calendar calendar = Calendar.getInstance();
        int dayOfMonth = calendar.getMaximum(Calendar.DAY_OF_MONTH);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View itemView;
        CalendarItem item;
        calendarItems = new ArrayList<>();
        for (int i = 1; i <= dayOfMonth; i++) {
            itemView = inflater.inflate(R.layout.item_calendar, null);
            scrollLayout.addView(itemView);
            item = new CalendarItem(itemView);

            calendar.set(Calendar.DAY_OF_MONTH, i);
            item.setData(calendar, i);
            itemView.setTag(item);
            itemView.setOnClickListener(onDateClickListener);
            calendarItems.add(item);
        }
    }

    @SuppressLint("WrongViewCast")
    private void moveToday() {

        findViewById(R.id.horizontalscrollview).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onGlobalLayout() {
                HorizontalScrollView scrollView = (HorizontalScrollView) findViewById(R.id.horizontalscrollview);

                int x = 0;
                for (CalendarItem calendarItem : calendarItems) {
                    if (calendarItem.status == calendarItem.STATUS_TODAY) {
                        calendarItem.setStatus(calendarItem.STATUS_CLICKED);
                        break;
                    }
                    x += calendarItem.parrent.getWidth();
                }

                x = (int) (x - Util.convertDpToPixel(4, getContext()));
                if (x < 0) {
                    x = 0;
                }
                scrollView.smoothScrollTo(x, 0);

                findViewById(R.id.horizontalscrollview).getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void reqAppShippingMainSummary(String date) {
        Net.async(new appShippingMainSummary(date)).setOnNetResponse(onNetListener);
    }

    private void initCalendarViewStatus() {
        for (CalendarItem calendarItem : calendarItems) {
            if (calendarItem.calendar.get(Calendar.DAY_OF_MONTH) == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) {
                calendarItem.setStatus(calendarItem.STATUS_TODAY);
            } else {
                calendarItem.setStatus(calendarItem.STATUS_NORMAL);
            }
        }
    }

    private String dayOfWeek(int idx) {
        switch (idx) {
            case Calendar.MONDAY:
                return "MON";
            case Calendar.TUESDAY:
                return "TUE";
            case Calendar.WEDNESDAY:
                return "WED";
            case Calendar.THURSDAY:
                return "THU";
            case Calendar.FRIDAY:
                return "FRI";
            case Calendar.SATURDAY:
                return "SAT";
            case Calendar.SUNDAY:
                return "SUN";
        }
        return null;
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        initCalendarViewStatus();
        moveToday();
        String endDate = (SDF.yyyymmdd_1.format(Calendar.getInstance().getTime()));
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.set(Calendar.DAY_OF_MONTH, 1);
        String startDate = SDF.yyyymmdd_1.format(startCalendar.getTime());
        String date = SDF.yyyymmdd_1.format(Calendar.getInstance().getTime());
//        Log.d("Auth.getId() : " + Auth.getId());
        if (!TextUtils.isEmpty(Auth.getId()) && Auth.isLogin()) {
            getBenefit(Auth.getId(), startDate, endDate);
        }
    }

    public void getBenefit(String employeeId, String dateStart, String dateEnd) {
        showProgress();
        DisposableObserver<BenefitData> benefitDataDisposableObserver = ServiceGenerator.createService(BenefitApi.class)
                .getMonthlyBenefit(employeeId, dateStart, dateEnd)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<BenefitData>() {
                    @SuppressLint("DefaultLocale")
                    @Override
                    public void onNext(BenefitData benefitData) {

                        if (benefitData.resultData != null && !benefitData.resultData.isEmpty()) {
                            StringBuffer stringBuffer = new StringBuffer();
                            for (BenefitData.resultData data : benefitData.resultData) {
                                stringBuffer.append(String.format("%,d 원", data.GRAND_TOTAL));
                            }
                            try {
                                setText(R.id.mainBenefitText, stringBuffer.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            hideProgress();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });


    }

    @SuppressLint("DefaultLocale")
    private void dummyGraphData(appShippingMainSummary.Data.resultData response) {
        TextView graph_cnt_1;
        LinearLayout graph1;
        TextView graph_cnt_2;
        LinearLayout graph2;
        TextView graph_cnt_3;
        LinearLayout graph3;
        try {
            graph_cnt_1 = (TextView) findViewById(R.id.graph_cnt_1);
            graph1 = (LinearLayout) findViewById(R.id.graph1);
            graph_cnt_2 = (TextView) findViewById(R.id.graph_cnt_2);
            graph2 = (LinearLayout) findViewById(R.id.graph2);
            graph_cnt_3 = (TextView) findViewById(R.id.graph_cnt_3);
            graph3 = (LinearLayout) findViewById(R.id.graph3);
        } catch (NullPointerException e) {
            return;
        }

        graph1.removeAllViews();
        graph2.removeAllViews();
        graph3.removeAllViews();

        for (appShippingMainSummary.Data.resultData.remainList item : response.remainList) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
            graph1.addView(view);
            ((TextView) view.findViewById(R.id.text1)).setText(item.PROMISE_TIME);
            ((TextView) view.findViewById(R.id.text2)).setText(item.PRODUCT_NAME);
        }
        graph_cnt_1.setText(String.valueOf(response.todayCnt.REMAIN_CNT));

        for (appShippingMainSummary.Data.resultData.compList item : response.compList) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
            graph2.addView(view);

            try {
                ((TextView) view.findViewById(R.id.text1)).setText(new StringBuilder().append(item.PROMISE_TIME.substring(0, 2)).append(":").append(item.PROMISE_TIME.substring(2, 4)).toString());
            } catch (Exception e) {
                ((TextView) view.findViewById(R.id.text1)).setText(item.PROMISE_TIME);
            }

            ((TextView) view.findViewById(R.id.text2)).setText(item.PRODUCT_NAME);
        }
        graph_cnt_2.setText(String.valueOf(response.todayCnt.COMPLETED_CNT));

        View view2 = new View(getContext());
//        int tmp = response.MONTH_CNT + 50;

//        DisplayMetrics metrics = new DisplayMetrics();
//        WindowManager mgr = (WindowManager) getContext().getSystemService(getContext().WINDOW_SERVICE);
//        mgr.getDefaultDisplay().getMetrics(metrics);
//
//        Log.d("metrics : " + metrics.densityDpi);


        int tmp = 50;
        graph3.addView(view2, 1, (int) Util.convertDpToPixel(tmp, Objects.requireNonNull(getContext())));


        Log.d("response.monthCnt : " + response.monthCnt.toString());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("전동침대");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.moterized_bed));

        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("전동기타");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.moterized_bed_etc));

        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("고급프레임");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.royal_frame));

        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("고급프레임기타");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.royal_frame_etc));


        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("배송");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.delivery_of_month));

        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("맞교체");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.change_of_month));

        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("계약철회");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.cancel_of_month));

        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("이전회수");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.relocation_of_month_1));

        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("이전설치");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.relocation_of_month_0));

        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("재설치");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.reinstall_of_month));

        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("수리");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.as_of_month));

        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("초도불량");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.faulty_of_month));


        view = LayoutInflater.from(getContext()).inflate(R.layout.item_main_graph, null);
        graph3.addView(view);
        ((TextView) view.findViewById(R.id.text1)).setText("기타");
        ((TextView) view.findViewById(R.id.text2)).setText(String.format("%d건", response.monthCnt.etc_of_month));

        graph_cnt_3.setText(String.format("%d", response.monthCnt.delivery_of_month
                + response.monthCnt.change_of_month
                + response.monthCnt.cancel_of_month
                + response.monthCnt.relocation_of_month_1
                + response.monthCnt.relocation_of_month_0
                + response.monthCnt.as_of_month
                + response.monthCnt.faulty_of_month
                + response.monthCnt.reinstall_of_month
                + response.monthCnt.etc_of_month)
        );
    }

    class CalendarItem {
        public final int STATUS_NORMAL = 0;
        public final int STATUS_TODAY = 1;
        public final int STATUS_CLICKED = 2;
        public int status = STATUS_NORMAL;

        public TextView dayOfWeek;
        public TextView dayOfMonth;
        public Calendar calendar;
        public View parrent;
        private View dot;

        public CalendarItem(View parrent) {
            this.parrent = parrent;
            dayOfWeek = (TextView) parrent.findViewById(R.id.day_of_week);
            dayOfMonth = (TextView) parrent.findViewById(R.id.day_of_month);
            dot = parrent.findViewById(R.id.dot);
        }

        public void setData(Calendar calendar, int dayOfMonth) {
            this.calendar = (Calendar) calendar.clone();

            this.dayOfWeek.setText(dayOfWeek(calendar.get(Calendar.DAY_OF_WEEK)));
            this.dayOfMonth.setText(dayOfMonth + "");
        }

        public void setStatus(int status) {
            this.status = status;
            switch (status) {
                case STATUS_NORMAL:
                    dayOfMonth.setTextColor(Color.parseColor("#393a3a"));
                    dayOfMonth.setBackgroundColor(Color.TRANSPARENT);
                    if (dot.getVisibility() != View.GONE) dot.setVisibility(View.GONE);
                    break;
                case STATUS_TODAY:
                    dayOfMonth.setTextColor(Color.WHITE);
                    dayOfMonth.setBackgroundResource(R.drawable.shape_rect_blue2);
                    if (dot.getVisibility() != View.VISIBLE) dot.setVisibility(View.VISIBLE);
                    break;
                case STATUS_CLICKED:
                    reqAppShippingMainSummary(SDF.yyyymmdd.format(calendar.getTime()));
                    dayOfMonth.setTextColor(Color.WHITE);
                    dayOfMonth.setBackgroundResource(R.drawable.shape_rect_green);
                    if (dot.getVisibility() != View.GONE) dot.setVisibility(View.GONE);
                    break;
            }
        }
    }

}