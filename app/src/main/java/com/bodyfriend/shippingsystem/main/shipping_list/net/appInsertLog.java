package com.bodyfriend.shippingsystem.main.shipping_list.net;

import com.bodyfriend.shippingsystem.base.BFEnty;
import com.bodyfriend.shippingsystem.base.NetConst;

public class appInsertLog extends BFEnty {

	public appInsertLog(String m_shippingseq, String promise, String shipping_ps, String ins_complete, String in_date) {

		url = NetConst.host + "mobile/api/appInsertLog.json";

		String logtdata = String.format("%s, %s, %s, %s, %s", m_shippingseq, promise, shipping_ps, ins_complete, in_date);
		setParam("logtype", "U", "logtable", "TB_SHIPPING_INFO", "logtdata", logtdata);
	}

	public appInsertLog(String m_shippingseq, String promise, String promise_time) {

		url = NetConst.host + "/mobile/api/appInsertLog.json";

		String logtdata = String.format("%s, %s, %s", m_shippingseq, promise, promise_time);
		setParam("logtype", "U", "logtable", "TB_SHIPPING_INFO", "logtdata", logtdata);
	}

	public appInsertLog(String log) {

		url = NetConst.host + "/mobile/api/appInsertLog.json";

		setParam("logtype", "U", "logtable", "TB_SHIPPING_INFO", "logtdata", log);
	}

	// logtype = "U"
	// logtable = "TB_SHIPPING_INFO"
	// logtdata = SHIPPING_SEQ, PROMISE, PROMISE_TIME

	// logtype = "U"
	// logtable = "TB_SHIPPING_INFO"
	// logtdata = PROMISE, SHIPPING_PS, INS_COMPLETE, IN_DATE
}
