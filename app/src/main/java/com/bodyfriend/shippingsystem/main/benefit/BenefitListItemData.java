package com.bodyfriend.shippingsystem.main.benefit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class BenefitListItemData {

    public ArrayList<BenefitData.resultData> resultData;
    public HashMap<TotalPerDay, ArrayList<resultData>> totalPerDayresultDataHashMap;

    public static class TotalPerDay {
        public String IN_DATE;
        public int TOTAL;

        public TotalPerDay(String inDate) {
            IN_DATE = inDate;
        }

        @Override
        public String toString() {
            return "TotalPerDay{" +
                    "IN_DATE='" + IN_DATE + '\'' +
                    ", TOTAL=" + TOTAL +
                    '}';
        }
    }

    public static class resultData {
        public String IN_DATE;
        public String DELIVERYMAN_NM;
        public int TOTAL;
        public int DAILY_COUNT;
        public String TYPE;

        @Override
        public String toString() {
            return "ResultData{" +
                    "IN_DATE='" + IN_DATE + '\'' +
                    ", DELIVERYMAN_NM='" + DELIVERYMAN_NM + '\'' +
                    ", TOTAL=" + TOTAL +
                    ", DAILY_COUNT=" + DAILY_COUNT +
                    ", TYPE='" + TYPE + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof resultData)) return false;
            resultData that = (resultData) o;
            return TOTAL == that.TOTAL &&
                    DAILY_COUNT == that.DAILY_COUNT &&
                    Objects.equals(IN_DATE, that.IN_DATE) &&
                    Objects.equals(DELIVERYMAN_NM, that.DELIVERYMAN_NM) &&
                    Objects.equals(TYPE, that.TYPE);
        }

        @Override
        public int hashCode() {

            return Objects.hash(IN_DATE, DELIVERYMAN_NM, TOTAL, DAILY_COUNT, TYPE);
        }
    }
}
