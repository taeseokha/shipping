package com.bodyfriend.shippingsystem.main.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bodyfriend.shippingsystem.BuildConfig;
import com.bodyfriend.shippingsystem.R;
import com.bodyfriend.shippingsystem.base.BFActivity;
import com.bodyfriend.shippingsystem.base.BaseConst;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.Net;
import com.bodyfriend.shippingsystem.base.util.AES256Util;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.base.util.PP;
import com.bodyfriend.shippingsystem.base.util.VersionManager;
import com.bodyfriend.shippingsystem.main.benefit.ServiceGenerator;
import com.bodyfriend.shippingsystem.main.login.custom.BlurBehind;
import com.bodyfriend.shippingsystem.main.login.custom.OnBlurCompleteListener;
import com.bodyfriend.shippingsystem.main.login.data.AuthenticationData;
import com.bodyfriend.shippingsystem.main.login.data.AuthenticationRequest;
import com.bodyfriend.shippingsystem.main.login.net.Authentication;
import com.bodyfriend.shippingsystem.main.login.net.LoginApi;
import com.bodyfriend.shippingsystem.main.login.net.MsgIdRequest;
import com.bodyfriend.shippingsystem.main.login.net.login;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends BFActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_login2);
        checkPermissions();
        if (!TextUtils.isEmpty(PP.MSG_ID.get())) {
            setVisibility(R.id.textInputLayout4, false);
        }
    }


    private void checkPermissions() {
        checkAndRequestPermissions(111,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CHANGE_WIFI_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.SEND_SMS,
                Manifest.permission.READ_PHONE_STATE
        );
    }

    @Override
    protected void onLoadOnce() {
        super.onLoadOnce();
        initButtonEvents();
        initLoginInfo();
        checkVersionOrAutoLogin();
        initPasswordView();
    }

    /**
     * 비밀번호 에디트텍스트 키보드에서 로그인 버튼누를때 attempLogin호출한다.
     */
    private void initPasswordView() {
        EditText passwordView = (EditText) findViewById(R.id.login_password);
        passwordView.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == R.id.login || id == EditorInfo.IME_NULL || id == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard(textView);
                attempLogin();
                return true;
            }
            return false;
        });
    }

    private void checkVersionOrAutoLogin() {
        Log.d("checkVersionOrAutoLogin ");
        if (!BuildConfig.DEBUG) {
            checkVersion();
        }
        autoLogin();
    }

    private void initLoginInfo() {

        if (PP.saveLoginId.is()) {
            setText(R.id.login_id, Auth.getId());
        }
        if (PP.autoLogin.is()) {
            setText(R.id.login_id, Auth.getId());
            setText(R.id.login_password, Auth.getPw());
        }
    }

    private void initButtonEvents() {
        setChecked(R.id.check1, PP.autoLogin.is());
        setChecked(R.id.check2, PP.saveLoginId.is());
        setOnClickListener(R.id.login, onLoginClickListener);
//        setOnClickListener(R.id.login_form, onClickLoginFormListener);
        setOnCheckedChangeListener(R.id.check1, onAutoLoginCheckedChangeListener);
        setOnCheckedChangeListener(R.id.check2, onSaveLoginIdCheckedChangeListener);

        setOnClickListener(R.id.pwChangBtn, v -> {
            startActivityForResult(new Intent(mContext, TempPwActivity.class), 10003);
        });

        EditText id = (EditText) findViewById(R.id.login_id);
        id.setPrivateImeOptions("defaultInputmode=english;");
    }

    private View.OnClickListener onLoginClickListener = arg0 -> attempLogin();

    private void attempLogin() {
        final String id = text(R.id.login_id);
        final String pw = text(R.id.login_password);
        if ("ip".equals(id) && "".equals(pw)) {
            startIpActivity();
            return;
        }

        if (id.length() == 0) {
            return;
        }
        if (pw.length() == 0) {
            return;
        }

        Calendar calendar = Calendar.getInstance();
        int dayNum = calendar.get(Calendar.DAY_OF_WEEK);
        Log.d("dayNum : " + dayNum);

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        Log.d("hour : " + hour);


        if (dayNum == Calendar.MONDAY) {
            Log.d("hour : " + hour);
            if (BaseConst.START_HOUR <= hour && hour <= BaseConst.END_HOUR) {
                showAlertDialog();
            } else {
                showProgress();
                login(text(R.id.login_id), text(R.id.login_password));
/*
                if (BuildConfig.FLAVOR.equals("outApp")) {
                    login(text(R.id.login_id), text(R.id.login_password), "outsourceApp");
                } else {
                    login(text(R.id.login_id), text(R.id.login_password), "innerApp");
                }
*/

            }
        } else {
            showProgress();
            login(text(R.id.login_id), text(R.id.login_password));
           /* if (BuildConfig.FLAVOR.equals("outApp")) {
                login(text(R.id.login_id), text(R.id.login_password), "outsourceApp");
            } else {
                login(text(R.id.login_id), text(R.id.login_password), "innerApp");
            }*/
        }
    }

    private void login(String id, String password) {
        Disposable subscribe = ServiceGenerator.changeApiBaseUrl(LoginApi.class, NetConst.host)
                .login(id, password)
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    hideProgress();
                    if (data.resultData == null) {
                        toast(data.resultMsg);
                        return;
                    }
                    if (data.resultMsg != null && data.resultCode != 200) {
                        toast(data.resultMsg);
                        return;
                    }

                    if (!BuildConfig.DEBUG) {
                        // 인증 요청
                        if (TextUtils.isEmpty(text(R.id.login_auth_code)) && TextUtils.isEmpty(PP.MSG_ID.get()) && !PP.IS_KAKAO.is())
                            try {
                                if (!BuildConfig.DEBUG) {
                                    getAuthenticationCode(AES256Util.AES_Encode(data.resultData.TEL_NO, NetConst.KEY_SPEC).replaceAll("(\r\n|\r|\n|\n\r)", ""), text(R.id.login_id));
                                } else {
                                    getAuthenticationCode(AES256Util.AES_Encode("01081813184", NetConst.KEY_SPEC).replaceAll("(\r\n|\r|\n|\n\r)", ""), text(R.id.login_id));
                                }
                            } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException e) {
                                e.printStackTrace();
                            }
                        else if (!TextUtils.isEmpty(text(R.id.login_auth_code)) && !TextUtils.isEmpty(PP.MSG_ID.get()) && !PP.IS_KAKAO.is()) {
                            // 카카오 인증 확인
                            checkAuthCode(PP.MSG_ID.get(), text(R.id.login_auth_code));
                        } else if (TextUtils.isEmpty(text(R.id.login_auth_code)) && !TextUtils.isEmpty(PP.MSG_ID.get()) && PP.IS_KAKAO.is()) {
                            // 재로그인
                            checkMsgId(PP.MSG_ID.get());
                        } else {
                            setText(R.id.login_auth_code, "");
                            PP.IS_KAKAO.set(false);
                            PP.MSG_ID.set("");
//                        Toast.makeText(mContext, "카카오 인증 후 처리 부탁드립니다.", Toast.LENGTH_SHORT).show();
                            getAuthenticationCode(AES256Util.AES_Encode(data.resultData.TEL_NO, NetConst.KEY_SPEC).replaceAll("(\r\n|\r|\n|\n\r)", ""), text(R.id.login_id));
                        }
                    }

                    boolean isPerCd = data.resultData.PER_CD == 104 || data.resultData.PER_CD == 105;
                    final boolean isGroupPermission = data.resultData.GROUP_CD.equals("SP"); // 배송기사용

                    if (isPerCd && isGroupPermission) {
                        Auth.saveIdPw(text(R.id.login_id), text(R.id.login_password));
                        Auth.login(data);
                        setText(R.id.login, "로그인");
                        if (BuildConfig.DEBUG) {
                            loginComplete();
                        }
                    } else {
                        toast("사용 권한이 없습니다.");
                    }
                });
    }

    private void login(String id, String password, String type) {
        Disposable subscribe = ServiceGenerator.changeApiBaseUrl(LoginApi.class, NetConst.host)
                .login(id, password, type)
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    hideProgress();
                    if (data.resultData == null) {
                        toast(data.resultMsg);
                        return;
                    }
                    if (data.resultMsg != null && data.resultCode != 200) {
                        toast(data.resultMsg);
                        return;
                    }

                    // 인증 요청
                    if (TextUtils.isEmpty(text(R.id.login_auth_code)) && TextUtils.isEmpty(PP.MSG_ID.get()) && !PP.IS_KAKAO.is())
                        try {
                            if (!BuildConfig.DEBUG) {
                                getAuthenticationCode(AES256Util.AES_Encode(data.resultData.TEL_NO, NetConst.KEY_SPEC).replaceAll("(\r\n|\r|\n|\n\r)", ""), text(R.id.login_id));
                            } else {
                                getAuthenticationCode(AES256Util.AES_Encode("01081813184", NetConst.KEY_SPEC).replaceAll("(\r\n|\r|\n|\n\r)", ""), text(R.id.login_id));
                            }
                        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException e) {
                            e.printStackTrace();
                        }
                    else if (!TextUtils.isEmpty(text(R.id.login_auth_code)) && !TextUtils.isEmpty(PP.MSG_ID.get()) && !PP.IS_KAKAO.is()) {
                        // 카카오 인증 확인
                        checkAuthCode(PP.MSG_ID.get(), text(R.id.login_auth_code));
                    } else if (TextUtils.isEmpty(text(R.id.login_auth_code)) && !TextUtils.isEmpty(PP.MSG_ID.get()) && PP.IS_KAKAO.is()) {
                        // 재로그인
                        checkMsgId(PP.MSG_ID.get());
                    } else {
                        setText(R.id.login_auth_code, "");
                        PP.IS_KAKAO.set(false);
                        PP.MSG_ID.set("");
//                        Toast.makeText(mContext, "카카오 인증 후 처리 부탁드립니다.", Toast.LENGTH_SHORT).show();
                        getAuthenticationCode(AES256Util.AES_Encode("01081813184", NetConst.KEY_SPEC).replaceAll("(\r\n|\r|\n|\n\r)", ""), text(R.id.login_id));
                    }

                    boolean isPerCd = data.resultData.PER_CD == 104 || data.resultData.PER_CD == 105;
                    final boolean isGroupPermission = data.resultData.GROUP_CD.equals("SP"); // 배송기사용

                    if (isPerCd && isGroupPermission) {
                        Auth.saveIdPw(text(R.id.login_id), text(R.id.login_password));
                        Auth.login(data);
                        setText(R.id.login, "로그인");
//                if (BuildConfig.BUILD_TYPE.equals("debug")) {
//                            loginComplete();
//                }
                    } else {
                        toast("사용 권한이 없습니다.");
                    }
                });
    }

    private void showAlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("접속 불가합니다.")
                .setMessage("월요일 오전 5 부터 9시까지 접속일 불가 합니다. ")
                .setPositiveButton("확인", (dialog, which) -> {
                    ActivityCompat.finishAffinity(LoginActivity.this);
                    System.exit(0);
                }).create();
        alertDialog.show();
    }

    private void startIpActivity() {
        BlurBehind.getInstance().execute(LoginActivity.this, new OnBlurCompleteListener() {
            @Override
            public void onBlurComplete() {
                Intent intent = new Intent(mContext, IpActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                startActivity(intent);
            }
        });
    }

    private Net.OnNetResponse<login> onLoginNetResponse = new Net.OnNetResponse<login>() {

        @Override
        public void onResponse(login response) {
            hideProgress();
            Log.d("login result : " + response.toString());

            if (response.data.resultData == null) {
                toast(response.data.resultMsg);
                return;
            }
            if (response.data.resultMsg != null && response.data.resultCode != 200) {
                toast(response.data.resultMsg);
                return;
            }

           /* if (TextUtils.isEmpty(text(R.id.login_auth_code)) && TextUtils.isEmpty(PP.MSG_ID.get()) && !PP.IS_KAKAO.is()) {
                // 인증 요청
                try {
                    if (!BuildConfig.DEBUG) {
                        getAuthenticationCode(AES256Util.AES_Encode(response.data.ResultData.TEL_NO, NetConst.KEY_SPEC).replaceAll("(\r\n|\r|\n|\n\r)", ""), text(R.id.login_id));
                    } else {
                        getAuthenticationCode(AES256Util.AES_Encode("01040518241", NetConst.KEY_SPEC).replaceAll("(\r\n|\r|\n|\n\r)", ""), text(R.id.login_id));
                    }
                } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException e) {
                    e.printStackTrace();
                }
            } else if (!TextUtils.isEmpty(text(R.id.login_auth_code)) && !TextUtils.isEmpty(PP.MSG_ID.get()) && !PP.IS_KAKAO.is()) {
                // 카카오 인증 확인
                checkAuthCode(PP.MSG_ID.get(), text(R.id.login_auth_code));
            } else if (TextUtils.isEmpty(text(R.id.login_auth_code)) && !TextUtils.isEmpty(PP.MSG_ID.get()) && PP.IS_KAKAO.is()) {
                // 재로그인
                checkMsgId(PP.MSG_ID.get());
            } else {
                setText(R.id.login_auth_code, "");
                PP.IS_KAKAO.set(false);
                PP.MSG_ID.set("");
                Toast.makeText(mContext, "카카오 인증 후 처리 부탁드립니다.", Toast.LENGTH_SHORT).show();
            }
*/

            boolean isPerCd = response.data.resultData.PER_CD == 104 || response.data.resultData.PER_CD == 105;
            final boolean isGroupPermission = response.data.resultData.GROUP_CD.equals("SP"); // 배송기사용

            if (isPerCd && isGroupPermission) {
                Auth.saveIdPw(text(R.id.login_id), text(R.id.login_password));
                Auth.login(response.data);
                setText(R.id.login, "로그인");
//                if (BuildConfig.BUILD_TYPE.equals("debug")) {
                loginComplete();
//                }
            } else {
                toast("사용 권한이 없습니다.");
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            error.printStackTrace();
            hideProgress();
        }
    };

    @Override
    public void onBackPressed() {
        if (PP.IS_KAKAO.is()) {
            super.onBackPressed();
        }

    }

    @SuppressLint("CheckResult")
    private void checkMsgId(String msgId) {

        DisposableObserver<AuthenticationData> benefitDataDisposableObserver
                = ServiceGenerator.changeApiBaseUrl(Authentication.class, NetConst.URL_AUTH)
                .checkMsgid(new MsgIdRequest(msgId))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<AuthenticationData>() {
                    @Override
                    public void onNext(AuthenticationData authenticationData) {
                        Log.d("authenticationData.toString()" + authenticationData.toString());
                        if (authenticationData.status.code.equals("200")) {
                            PP.IS_KAKAO.set(true);
                            loginComplete();
                        } else {
//                            setVisibility(R.id.textInputLayout4, false);
                            Toast.makeText(mContext, "인증 코드가 맞지 않습니다. 인증 부탁드립니다.", Toast.LENGTH_SHORT).show();
                            setText(R.id.login, "인증요청");
                            PP.IS_KAKAO.set(false);
                            PP.MSG_ID.set("");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("e : " + e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }


    private void checkAuthCode(String msgId, String authCode) {
        Log.d("getAuthenticationCode");
        DisposableObserver<AuthenticationData> benefitDataDisposableObserver
                = ServiceGenerator.changeApiBaseUrl(Authentication.class, NetConst.URL_AUTH)
                .checkAuthCode(new MsgIdRequest(authCode, msgId))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<AuthenticationData>() {
                    @Override
                    public void onNext(AuthenticationData authenticationData) {
                        Log.d("authenticationData.toString()" + authenticationData.toString());
                        if (authenticationData.status.code.equals("200")) {
                            setVisibility(R.id.textInputLayout4, false);
                            PP.IS_KAKAO.set(true);
                            loginComplete();
                        } else {
                            Toast.makeText(mContext, authenticationData.status.message + "\t인증번호 확인 부탁합니다.", Toast.LENGTH_SHORT).show();
                            setText(R.id.login_auth_code, "");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("e : " + e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }


    private void getAuthenticationCode(String phoneNumber, String userId) {
        DisposableObserver<AuthenticationData> benefitDataDisposableObserver
                = ServiceGenerator.changeApiBaseUrl(Authentication.class, NetConst.URL_AUTH)
                .getAuthenticationCode(new AuthenticationRequest(phoneNumber, userId))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<AuthenticationData>() {
                    @Override
                    public void onNext(AuthenticationData authenticationData) {
                        Log.d("getAuthenticationCode : " + authenticationData.toString());
                        if (authenticationData.data != null) {
                            Log.d("authenticationData : " + authenticationData.data.msgid);
                            PP.MSG_ID.set(authenticationData.data.msgid);
                            setVisibility(R.id.textInputLayout4, true);
                        } else {
                            setText(R.id.login_auth_code, "");
                            Toast.makeText(mContext, "재 인증 요청 하세요.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("e : " + e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }


    private void loginComplete() {
        Log.d("loginComplete ~~~~~~~~~~~~~~~~~");
        OH.c().notifyObservers(OH.TYPE.LOGIN_SUCCESS);
        finish();
    }

    private CompoundButton.OnCheckedChangeListener onAutoLoginCheckedChangeListener = (buttonView, isChecked) -> PP.autoLogin.set(isChecked);

    private CompoundButton.OnCheckedChangeListener onSaveLoginIdCheckedChangeListener = (buttonView, isChecked) -> PP.saveLoginId.set(isChecked);

    private void checkVersion() {
        showProgress();
        VersionManager.getInstance(this).loadMarketVersion("bodyfriend.app@gmail.com", "body1234", getPackageName(), versionListener);
    }

    private VersionManager.OnVersionListener versionListener = new VersionManager.OnVersionListener() {

        @Override
        public void onLoadSuccess(String version) {
            hideProgress();
            final String sCurrentVersion = VersionManager.getInstance(mContext).getClientVersion();
            float fMarketVersion = Float.valueOf(version);
            float fCurVersion = Float.valueOf(sCurrentVersion);

            if (fMarketVersion > fCurVersion) { // 마켓 버전이 현재 버전보다 높으면 팝업 띄움.
                showUpdateDialog();
            } else {
                autoLogin();
            }
        }

        @Override
        public void onLoadFailed() {
            hideProgress();
            autoLogin();
        }
    };

    private void showUpdateDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("구글 스토어에 현재 버전보다 최신 버전이 존재합니다.\n업데이트 하시겠습니까?");
        builder.setPositiveButton("업데이트", updateListener);
        builder.setNegativeButton("종료", negativeListener);
        builder.setCancelable(false);

        final AlertDialog dlg = builder.create();
        dlg.show();

    }

    private void autoLogin() {
        if (PP.autoLogin.is()) {
            attempLogin();
        }
    }

    /**
     * 업데이트
     */
    private android.content.DialogInterface.OnClickListener updateListener = new android.content.DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            marketLaunch.setData(Uri.parse(String.format("market://details?id=%s", getPackageName())));
            startActivity(marketLaunch);
        }
    };
    /**
     * 취소
     */
    private android.content.DialogInterface.OnClickListener negativeListener = new android.content.DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            exit();
        }
    };

    private View.OnClickListener onClickLoginFormListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideKeyboard(v);
        }
    };
}