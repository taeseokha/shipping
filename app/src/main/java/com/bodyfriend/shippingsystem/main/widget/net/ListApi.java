package com.bodyfriend.shippingsystem.main.widget.net;

import com.bodyfriend.shippingsystem.main.shipping_list.net.ShippingList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by 이주영 on 2017-05-23.
 */

public interface ListApi {


    @FormUrlEncoded
    @POST(NET_API.APP_RECEIVE_LIST_JSON)
    Call<ShippingList> getReceiveList(
            @Field("s_datetype") int s_datetype
            , @Field("dateStart") String m_sdate
            , @Field("dateEnd") String m_edate
            , @Field("s_oneself") String m_order_type
            , @Field("s_producttype") String producttype
    );

}
