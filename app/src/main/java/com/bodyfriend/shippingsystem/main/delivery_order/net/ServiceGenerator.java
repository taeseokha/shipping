package com.bodyfriend.shippingsystem.main.delivery_order.net;

import android.support.annotation.NonNull;

import com.bodyfriend.shippingsystem.BuildConfig;
import com.bodyfriend.shippingsystem.base.NetConst;
import com.bodyfriend.shippingsystem.main.login.Auth;


import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Taeseok on 2018-03-5.
 */

public class ServiceGenerator {

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(NetConst.HOST_WAREHOUSE)
                    .client(getRequestHeader())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();


    public static <S> S createService(
            Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }


    @NonNull
    private static Response addHeader(Interceptor.Chain chain) throws IOException {
        try {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("Cookie", String.format("JSESSIONID=%s", Auth.getSid()))
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        } catch (SocketTimeoutException exception) {
            exception.printStackTrace();

        }
        return chain.proceed(chain.request());
    }

    private static OkHttpClient getRequestHeader() {
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            return new OkHttpClient.Builder()
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .addInterceptor(ServiceGenerator::addHeader)
                    .addInterceptor(interceptor)
                    .build();
        } else {
            return new OkHttpClient.Builder()
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .addInterceptor(ServiceGenerator::addHeader)

                    .build();
        }
    }
}
