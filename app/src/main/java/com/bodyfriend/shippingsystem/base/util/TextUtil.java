package com.bodyfriend.shippingsystem.base.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.DecimalFormat;

/**
 * Created by 이주영 on 2017-01-10.
 */
public class TextUtil {
    private static TextUtil ourInstance = new TextUtil();

    public static TextUtil getInstance() {
        return ourInstance;
    }

    private TextUtil() {
    }

    /**
     * EditText에 Comma를 붙여준다.
     */
    public void setWon(final EditText editText) {

        // 세자리로 끊어서 쉼표 보여주고, 소숫점 셋째짜리까지 보여준다.
        final DecimalFormat df = new DecimalFormat("###,###.####");
        // 값 셋팅시, StackOverFlow를 막기 위해서, 바뀐 변수를 저장해준다.

        // 숫자가 바뀔때마다, 새로 셋팅을 해주어야 하므로, ChangeListener를 단다.
        editText.addTextChangedListener(new TextWatcher() {
            String result = "";

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    String pureText = s.toString();
                    if (!pureText.equals(result)) {     // StackOverflow를 막기위해,
                        if (pureText.isEmpty()) return;

                        result = df.format(Long.parseLong(pureText.replaceAll(",", "").trim()));   // 에딧텍스트의 값을 변환하여, result에 저장.
                        editText.setText(result);    // 결과 텍스트 셋팅.
                        editText.setSelection(result.length());     // 커서를 제일 끝으로 보냄.
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
