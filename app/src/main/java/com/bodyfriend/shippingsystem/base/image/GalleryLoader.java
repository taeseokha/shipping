package com.bodyfriend.shippingsystem.base.image;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import com.bodyfriend.shippingsystem.base.log.Log;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;

/**
 * &lt;uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"
 * android:maxSdkVersion="18" />
 * <p>
 * <pre>
 * &#064;Override
 * public void onActivityResult(int requestCode, int resultCode, Intent data) {
 * mGalleryLoader.onActivityResult(requestCode, resultCode, data);
 * super.onActivityResult(requestCode, resultCode, data);
 * }
 *
 *
 * @used
 * private GalleryLoader mGalleryLoader;
 * private OnClickListener onPhotoClickListener = new OnClickListener() {
 * &#064;Override
 * public void onClick(View v) {
 * Log.l();
 * Activity activity = SProfile.this;
 * mGalleryLoader = new GalleryLoader(activity);
 * mGalleryLoader.startCameraCrop(400, 400);
 * }
 * };
 * </pre>
 */
public class GalleryLoader {
    public static interface OnGalleryLoader {
        public void onLoaded(GalleryLoader galleryLoader, Uri uri);
    }

    public static final int REQUEST_CAMERA = 901;
    public static final int REQUEST_GALLERY = 902;

    public static final int REQUEST_CAMERA_CROP = 911;
    public static final int REQUEST_GALLERY_CROP = 912;
    public static final int REQUEST_CROP = 913;

    private WeakReference<Activity> mActivity;
    private Fragment mFragment;
    private int w = 480;
    private int h = 800;

    private Uri mTempPathfile;
    private Uri mResultPathfile;

    private File storageDir;

    public GalleryLoader(Activity activity) {
        mActivity = new WeakReference<Activity>(activity);
    }

    public GalleryLoader(Fragment fragment) {
        this(fragment.getActivity());
        mFragment = fragment;
    }

    private OnGalleryLoader onGalleryLoader;

    public void setOnGalleryLoaderListener(OnGalleryLoader onGalleryLoader) {
        this.onGalleryLoader = onGalleryLoader;
    }

    public void getImageCrop(final int w, final int h) {
        final Activity activity = mActivity.get();
        if (activity == null)
            return;
        DialogInterface.OnClickListener onItemClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {//((AlertDialog)dialog).getListView().getItemAtPosition(which).toString().equals("카메라")
                    startCameraCrop(w, h);
                }
                if (which == 1) {//((AlertDialog)dialog).getListView().getItemAtPosition(which).toString().equals("앨범")
                    startGalleryCrop(w, h);
                }
            }
        };

        new AlertDialog.Builder(activity)//
                .setItems(new String[]{"카메라", "앨범"}, onItemClick)//
                .show();
    }

    public void getImage() {
        final Activity activity = mActivity.get();
        if (activity == null)
            return;

//		startCamera();
        DialogInterface.OnClickListener onItemClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {//((AlertDialog)dialog).getListView().getItemAtPosition(which).toString().equals("카메라")
                    startCamera();
                }
                if (which == 1) {//((AlertDialog)dialog).getListView().getItemAtPosition(which).toString().equals("앨범")
                    startGallery();
                }
            }
        };

        new AlertDialog.Builder(activity)//
                .setItems(new String[]{"카메라", "앨범"}, onItemClick)//
                .show();
    }

    private void startActivityForResult(Intent intent, int requestCode) {
        final Activity activity = mActivity.get();
        final Fragment fragment = mFragment;
        if (activity != null) {
            if (fragment != null)
                activity.startActivityFromFragment(fragment, intent, requestCode);
            else
                activity.startActivityForResult(intent, requestCode);
        }
    }

    public void startCameraCrop(int w, int h) {
        this.w = w;
        this.h = h;

        mTempPathfile = getTempUri();
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE)//
                .putExtra(MediaStore.EXTRA_OUTPUT, mTempPathfile)//
                ;

        startActivityForResult(intent, REQUEST_CAMERA_CROP);

    }

    public void startCamera2() {
        mResultPathfile = getTempUri();

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mResultPathfile);

        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public void startCamera() {
        mResultPathfile = getTempUri();

        Intent cameraInent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File imageFile = storageDir;
        Uri photoURI = FileProvider.getUriForFile(mActivity.get(), mActivity.get().getApplicationContext().getPackageName() + ".provider", imageFile);

        List<ResolveInfo> resolvedIntentActivities = mActivity.get().getPackageManager().queryIntentActivities(cameraInent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
            String packageName = resolvedIntentInfo.activityInfo.packageName;
            mActivity.get().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }

        cameraInent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(cameraInent, REQUEST_CAMERA);

    }

    public void startGalleryCrop(int w, int h) {
        this.w = w;
        this.h = h;
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);

        startActivityForResult(intent, REQUEST_GALLERY_CROP);
    }

    public void startGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);

        startActivityForResult(intent, REQUEST_GALLERY);
    }

    public void startCrop(Uri pathfile, int w, int h) {
        this.w = w;
        this.h = h;

        mResultPathfile = getTempUri();
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(pathfile, "image/*");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mResultPathfile);
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", w);
        intent.putExtra("aspectY", h);
        intent.putExtra("outputX", w);
        intent.putExtra("outputY", h);
        try {
            startActivityForResult(intent, REQUEST_CROP);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * &lt;uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"
     * android:maxSdkVersion="18" />
     */
    public Uri getTempUri() {
        try {
            storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            if (!storageDir.exists())
                storageDir.mkdirs();
            return Uri.fromFile(File.createTempFile("GalleryLoader", ".jpg", storageDir));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
//		Log.l(requestCode, resultCode, data);

        Log.d("GalleryLoader > onActivityResult");

        switch (requestCode) {
            case GalleryLoader.REQUEST_CAMERA_CROP:
            case GalleryLoader.REQUEST_GALLERY_CROP:
            case GalleryLoader.REQUEST_CROP:
            case GalleryLoader.REQUEST_CAMERA:
            case GalleryLoader.REQUEST_GALLERY:
                break;
            default:
                return false;
        }

        final Activity activity = mActivity.get();
        if (activity == null)
            return true;

        if (resultCode != Activity.RESULT_OK)
            return true;

        switch (requestCode) {
            case GalleryLoader.REQUEST_CAMERA_CROP:
                startCrop(mTempPathfile, w, h);
                break;
            case GalleryLoader.REQUEST_GALLERY_CROP:
                startCrop(data.getData(), w, h);
                break;

            case GalleryLoader.REQUEST_CROP:
            case GalleryLoader.REQUEST_CAMERA:
                Log.d("GalleryLoader > onActivityResult > REQUEST_CAMERA");
                if (mTempPathfile != null) {
                    File f = new File(mTempPathfile.getEncodedSchemeSpecificPart());
                    if (f.exists()) {
                        f.delete();
                    }
                    mTempPathfile = null;
                }

                if (onGalleryLoader != null)
                    onGalleryLoader.onLoaded(this, mResultPathfile);
                break;
            case GalleryLoader.REQUEST_GALLERY:
                if (mTempPathfile != null) {
                    File f = new File(mTempPathfile.getEncodedSchemeSpecificPart());
                    if (f.exists()) {
                        f.delete();
                    }
                    mTempPathfile = null;
                }

                if (onGalleryLoader != null)
                    onGalleryLoader.onLoaded(this, data.getData());

                break;

        }
        return true;
    }
}