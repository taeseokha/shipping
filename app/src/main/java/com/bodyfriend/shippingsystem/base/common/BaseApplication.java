package com.bodyfriend.shippingsystem.base.common;

import android.app.Application;
import android.content.Context;

import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.util.FU;

import java.lang.Thread.UncaughtExceptionHandler;

public abstract class BaseApplication extends Application implements UncaughtExceptionHandler {

    private UncaughtExceptionHandler dueHandler = Thread.getDefaultUncaughtExceptionHandler();

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        Thread.setDefaultUncaughtExceptionHandler(this);
        FU.CREATE(base);
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        Log.flog(getApplicationContext(), Log._DUMP_throwable(ex));
        dueHandler.uncaughtException(thread, ex);
    }
}
