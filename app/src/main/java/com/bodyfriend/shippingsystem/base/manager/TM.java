package com.bodyfriend.shippingsystem.base.manager;

import android.content.Context;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneNumberUtils;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import java.util.List;

/**
 * &lt;uses-permission
 * android:name="android.permission.READ_PHONE_STATE"></uses-permission>
 */

/**
 * @author djrain
 *
 */
public class TM {

	public static CellLocation getCellLocation(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getCellLocation();
	}

	public static String getKRFormat(String phoneNumber) {
		try {
			phoneNumber = PhoneNumberUtils.stripSeparators(phoneNumber.replace("+82", "0"));
		} catch (NullPointerException e) {
		}
		return phoneNumber;
	}

	public static String getLine1Number(Context context) {
		String line1Number = "";
		try {
			TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			line1Number = tm.getLine1Number();
		} catch (Exception e) {
		}

		return getKRFormat(line1Number);
	}

	/**
	 * 대부분 삼성폰은 지원하지 않는다고 함
	 * 
	 * @param context
	 * 
	 * @return
	 */
	public static List<NeighboringCellInfo> getNeighboringCellInfo(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getNeighboringCellInfo();
	}
	/**
	 * NetworkOperator 국가 + 이통사
	 * @param context
	 * @return
	 */
	public static String getNetworkOperator(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getNetworkOperator();
	}
	/**
	 * NetworkOperator 국가
	 * @param context
	 * @return
	 */
	public static String getNetworkOperatorMCC(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getNetworkOperator().substring(0, 3);
	}

	/**
	 * NetworkOperator  이통사
	 * @param context
	 * @return
	 */
	public static String getNetworkOperatorMNC(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getNetworkOperator().substring(0, 3);
	}

	public static enum NetworkOperator {
		NONE(""), SKT("45005"), LG("45006"), KT("45008");
		public static NetworkOperator find(String networkOperatorCode) {
			for (NetworkOperator code : values()) {
				if (code.equalsNetworkOperator(networkOperatorCode)) {
					return code;
				}
			}
			return NONE;
		}

		private String mNetworkOperatorCode;

		NetworkOperator(String networkOperator) {
			mNetworkOperatorCode = networkOperator;
		}

		private boolean equalsNetworkOperator(String networkOperatorCode) {
			return this.mNetworkOperatorCode.equals(networkOperatorCode);
		}
	}

	public static void getSimInfo(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String imsi = tm.getSubscriberId();
		String imei = tm.getDeviceId();
	}
	public static boolean isRoaming(Context context) {
		TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		if (mTelephonyManager.isNetworkRoaming()) {
			return true;
		}
		return false;
	}

	/**
	 * hide
	 * | PhoneStateListener.LISTEN_DATA_CONNECTION_REAL_TIME_INFO// 0x00002000;
	 * | PhoneStateListener.LISTEN_OEM_HOOK_RAW_EVENT // 0x00008000;
	 * | PhoneStateListener.LISTEN_OTASP_CHANGED // 0x00000200;
	 * | PhoneStateListener.LISTEN_PRECISE_CALL_STATE // 0x00000800;
	 * | PhoneStateListener.LISTEN_PRECISE_DATA_CONNECTION_STATE // 0x00001000;
	 * | PhoneStateListener.LISTEN_SIGNAL_STRENGTH // 0x00000002;
	 * | PhoneStateListener.LISTEN_VOLTE_STATE // 0x00004000;
	 */
	public static void TelephonyManagerListenAll(Context context, PhoneStateListener phoneStateListener) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		tm.listen(phoneStateListener//
				, PhoneStateListener.LISTEN_CALL_FORWARDING_INDICATOR     // 0x00000008;
						| PhoneStateListener.LISTEN_CALL_STATE                    // 0x00000020;
						| PhoneStateListener.LISTEN_CELL_INFO                     // 0x00000400;
						| PhoneStateListener.LISTEN_CELL_LOCATION                 // 0x00000010;
						| PhoneStateListener.LISTEN_DATA_ACTIVITY                 // 0x00000080;
						| PhoneStateListener.LISTEN_DATA_CONNECTION_STATE         // 0x00000040;
						| PhoneStateListener.LISTEN_MESSAGE_WAITING_INDICATOR     // 0x00000004;
						| PhoneStateListener.LISTEN_SERVICE_STATE                 // 0x00000001;
						| PhoneStateListener.LISTEN_SIGNAL_STRENGTHS              // 0x00000100;
		);
	}

}