package com.bodyfriend.shippingsystem.base.util;

import android.text.Editable;
import android.text.TextWatcher;

import junit.framework.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Sting Util
 * 
 */
public class SU {

	public static final String BK = "●";

	public static String STARMARK(int length) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++)
			sb.append(BK);
		return sb.toString();
	}
	public static String STARMARK(String text) {
		return (text == null) ? "" : STARMARK(text.length());
	}
	public static class LimitByteTextWatcher implements TextWatcher {
		private int bytelength;
		private String format;
		private Runnable what;

		public LimitByteTextWatcher(int bytelength, String incode_format) {
			this.bytelength = bytelength;
			this.format = incode_format;
		}

		public LimitByteTextWatcher(int bytelength, String incode_format, Runnable what) {
			this(bytelength, incode_format);
			this.what = what;
		}

		public void afterTextChanged(Editable s) {
			try {
				byte[] byteArray = s.toString().getBytes(format);
				int count = byteArray.length;
				if (count > bytelength) {
					int utfcount = new String(byteArray, 0, bytelength, format).length();
					s.delete(utfcount, s.length());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (what != null)
				what.run();
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
	};

	/**
	 * 유효 비밀번호 확인
	 * 
	 * @return
	 */
	public static boolean validPass(String password) {
		return Pattern.compile("[a-zA-Z]").matcher(password).find();
	}

	/**
	 * 하나은행 사용 전화번호 포멧
	 * 0505 010 011 016 017 018 019 번호에 한하여 문자를 보낼수 있다
	 */
	public static String regularExpressionPhoneNo = "^(0(?:505|70|10|11|16|17|18|19))(\\d{3}|\\d{4})(\\d{4})$";
	public static String phoneNumberFormater(String phoneNo) {
		try {
			return phoneNo.replaceAll(regularExpressionPhoneNo, "$1-$2-$3");
		} catch (Exception e) {
			return phoneNo;
		}
	}

	public static final String getString(String strValue, String regularExpression, int group) {
		Pattern pattern = Pattern.compile(regularExpression);
		Matcher matcher = pattern.matcher(strValue);
		if (matcher.find()) {
			Assert.assertTrue(matcher.groupCount() >= group);
			return matcher.group(group);
		}
		return null;
	}

	//"KSC5601"
	public static int lengthByte(String string, String format) {
		try {
			return string.toString().getBytes(format).length;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;

	}

	public static String limitByte(String string, int length, String format) {
		try {
			byte[] byteArray = string.toString().getBytes(format);
			int count = byteArray.length;
			if (count > length) {
				int utfcount = new String(byteArray, 0, length, format).length();
				string = string.substring(0, utfcount);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return string;
	}

	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
		} finally {
		}

		return sb.toString();
	}

	public static boolean isEmpty(CharSequence str) {
		return str == null || str.length() == 0;
	}

	public static String onlyNumber(String value) {
		return value.replaceAll("//D", "");
	}

	public static boolean equals(String l, String r) {
		if ((l == null || l.trim().length() == 0) && (r == null || r.trim().length() == 0))
			return true;

		return l != null && l.equals(r);
	}

	public static String format(String no, String regularExpression, String devider) {
		try {
			String noadj = no.replaceAll("//D", "");
			Pattern pattern = Pattern.compile(regularExpression);
			Matcher matcher = pattern.matcher(noadj);
			if (noadj == null || !matcher.matches())
				return no;

			int N = matcher.groupCount();
			StringBuilder sb = new StringBuilder();
			for (int i = 1; i < N; i++) {
				sb.append(matcher.group(i));
				sb.append(devider);
			}
			sb.append(matcher.group(N));
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return no;
		}
	}
}