package com.bodyfriend.shippingsystem.base.net;

import android.net.Uri;

import com.android.volley.Request.Method;
import com.bodyfriend.shippingsystem.base.log.Log;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class NetEnty {
    //req
    boolean _DEBUG_OUT_HEADER = true;
    boolean _DEBUG_OUT1 = true;
    boolean _DEBUG_OUT2 = true;

    boolean _DEBUG_IN_HEADER = false;
    boolean _DEBUG_IN1 = true;
    boolean _DEBUG_IN2 = true;
    boolean _DEBUG_IN3 = true;

    protected final int method;
    protected String url;

    public NetEnty(int method) {
        this.method = method;
    }

    //req-post
    protected byte[] body;
    private String contentType;
    protected Map<String, Object> params = new HashMap<String, Object>();

    //header
    Map<String, String> headers = new HashMap<String, String>();

    //res
    protected boolean success;
    protected String errorMessage;
    protected String response;

    protected void parse(String json) {
        this.response = json;
    }

    protected void error(Exception error) {
        this.errorMessage = error.getMessage();
    }

    private static final String UTF_8 = "UTF-8";

    public Object getQueryParameter(String key) {
        if (method == Method.GET)
            return Uri.parse(url).getQueryParameter(key);
        else
            return params.get(key);
    }

    protected Map<String, String> getHeaders() {
        return headers;
    }

    public byte[] getBody() {
        if (body == null) {
            setBody();
        }
        return body;
    }

    String getBodyContentType() {
        return contentType;
    }

    public String getResponse() {
        return response;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    protected final void showLog(boolean isShow) {
        _DEBUG_OUT_HEADER = isShow;
        _DEBUG_OUT1 = isShow;
        _DEBUG_OUT2 = isShow;

        _DEBUG_IN_HEADER = isShow;
        _DEBUG_IN1 = isShow;
        _DEBUG_IN2 = isShow;
        _DEBUG_IN3 = isShow;
    }

    private void setBody() {
        if (isStringParams())
            setParametersBody();
        else
            setMultipartBody();
    }

    private boolean isStringParams() {
        for (Object obj : params.values()) {
            if (obj instanceof File /*|| obj instanceof ContentBody*/)
                return false;
        }
        return true;
    }

    private void setParametersBody() {
        Map<String, Object> stringParams = this.params;

        StringBuilder encodedParams = new StringBuilder();
        try {
            HashMap<String, String[]> hashMap = null; // 배열방식으로 송신 하기위한 hash map 이다.
            for (Entry<String, ? extends Object> entry : stringParams.entrySet()) {
                String key = entry.getKey();
                final Object value = entry.getValue();
                if (value == null)
                    continue;

                try {
                    if (key.contains("[") && key.contains("]")) {
                        // 배열 방식인경우 값을 어레이리스트에 담아서 해쉬맵에 담는다.
                        String simpleKey = key.substring(0, key.indexOf("["));
                        // 와 정규식 썼네? 내가 이렇게 잘 해놨던가? ㅋㅋ
                        int seq = Integer.valueOf(key.replaceAll("[^\\d]", ""));

                        if (hashMap == null) {
                            hashMap = new HashMap<>();
                        }
                        String[] array = hashMap.get(simpleKey);
                        if (array == null) {
                            // 문제가 있는 코드군.. 길이 제한이 100이라니
                            array = new String[100];
                        }
                        array[seq] = value.toString();

                        hashMap.put(simpleKey, array);
                    } else {
                        // 일반 방식인 경우 바로 담는다.
                        encodedParams.append(URLEncoder.encode(key, UTF_8));
                        encodedParams.append('=');
                        encodedParams.append(URLEncoder.encode(value.toString(), UTF_8));
                        encodedParams.append('&');
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // hashmap이 null이 아닌경우 배열방식이라고 판단하고 다시 값을 넣는다.
            if (hashMap != null) {
                for (String key : hashMap.keySet()) {
                    for (String value : hashMap.get(key)) {
                        if (value == null) break;
                        encodedParams.append(URLEncoder.encode(key, UTF_8));
                        encodedParams.append('=');
                        encodedParams.append(URLEncoder.encode(value, UTF_8));
                        encodedParams.append('&');
                    }
                }
            }

            Log.d("encodedParams.toString() > " + encodedParams.toString());
            this.body = encodedParams.toString().getBytes(UTF_8);
            this.contentType = "application/x-www-form-urlencoded; charset=" + UTF_8;
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("Encoding not supported: " + UTF_8, uee);
        }
    }

    private void setMultipartBody() {
    }

    private static final String LINE_FEED = "\r\n";

    public StringBuilder addFilePart(String fieldName, File uploadFile) throws IOException {
        StringBuilder encodedParams = new StringBuilder();
        String fileName = uploadFile.getName();
//        writer.append("--" + boundary).append(LINE_FEED);
        encodedParams.append("Content-Disposition: form-data; name=\"").append(fieldName).append("\"; filename=\"").append(fileName).append("\"").append(LINE_FEED);
        encodedParams.append("Content-Type: ").append(URLConnection.guessContentTypeFromName(fileName)).append(LINE_FEED);
        encodedParams.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
        encodedParams.append(LINE_FEED);
        return encodedParams;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "<" + (method == Method.POST ? "POST" : "GET") + ">" + url;
    }

}