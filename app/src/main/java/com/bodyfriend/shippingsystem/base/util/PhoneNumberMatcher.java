package com.bodyfriend.shippingsystem.base.util;

import android.text.TextUtils;

import java.util.regex.Pattern;

/**
 * Created by Taeseok on 2017-07-05.
 */

public class PhoneNumberMatcher {

    private static PhoneNumberMatcher instance = new PhoneNumberMatcher();

    private PhoneNumberMatcher() {
    }

    private static PhoneNumberMatcher getInstance() {
        return instance;
    }

    /**
     * 휴대폰, 안심 번호 체크
     *
     * @param cellPhone
     * @return sms 전송 가능여부
     */
    public static Boolean isCellPhoneNumber(String cellPhone) {
        // 일반 휴대폰 번호
        String cellPhone1 = "^01(?:0|1|[6-9])(?:\\d{3}|\\d{4})\\d{4}$";

        // 안심 번호
        String cellPhone2 = "050(?:\\d{3}|\\d{4})\\d{4}$";
        if (!TextUtils.isEmpty(cellPhone)) {
            return Pattern.matches(cellPhone1, cellPhone) || Pattern.matches(cellPhone2, cellPhone);
        }
        return false;
    }


    /**
     * 전화 번호를 노티 아이디로 변경 1
     *
     * @param cellPhone
     * @return notiId
     */
    public static int phoneNumber2NotiId(String cellPhone) {
        if (!TextUtils.isEmpty(cellPhone)) {
            String temp = cellPhone.replaceAll("[^\\d]", "");
            temp = temp.replaceAll("0", "");
//        if (temp.length() > 9) {
            if (temp.length() > 6) {
                return Integer.parseInt(temp.substring(temp.length() - 4, temp.length()));
            } else {
                return Integer.parseInt(temp);
            }
        } else {
            return 13123;
        }

    }

}
