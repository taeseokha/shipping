package com.bodyfriend.shippingsystem.base;

import com.android.volley.Request.Method;
import com.bodyfriend.shippingsystem.base.data.Pojo;
import com.bodyfriend.shippingsystem.base.log.Log;
import com.bodyfriend.shippingsystem.base.net.NetEnty;
import com.bodyfriend.shippingsystem.base.util.OH;
import com.bodyfriend.shippingsystem.base.util.SDF;
import com.bodyfriend.shippingsystem.main.login.Auth;
import com.bodyfriend.shippingsystem.main.login.net.login;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

import static com.bodyfriend.shippingsystem.base.log.Log.showLogCat;

public class BFEnty extends NetEnty {

    private String err;
    private String err_msg;
    private String session_msg;

    private boolean isShowProgress = true;

    protected void setEnableProgress() {
        isShowProgress = false;
    }

    private Gson gson;

    public BFEnty() {
        super(Method.POST);

        try {
            if (!(this instanceof login)) {
                getHeaders().put("Cookie", String.format("JSESSIONID=%s", Auth.getSid()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        gson = new GsonBuilder()//
                .registerTypeAdapter(Date.class, deserializer_date)//
                .registerTypeAdapter(boolean.class, deserializer_boolean)//
                .registerTypeAdapter(ArrayList.class, deserializer_stringarraylist)//
                .create();
    }

    protected void setParam(Object... key_value) {
        if (key_value.length % 2 == 1)
            throw new IllegalArgumentException("!!key value must pair");

        int N = (key_value.length) / 2;
        for (int i = 0; i < N; i++) {
            final String key = (String) key_value[i * 2];
            final Object value = key_value[i * 2 + 1];
            // Log.l(key, value);
            if (value == null) {
                params.put(key, null);
            } else if (value.getClass().isArray()) {
                final Object[] values = (Object[]) value;
                for (int j = 0; j < values.length; j++)
                    params.put(key + "[" + j + "]", values[j]);
            } else if (value.getClass().isAssignableFrom(ArrayList.class)) {
                final ArrayList<?> values = (ArrayList<?>) value;
                for (int j = 0; j < values.size(); j++)
                    params.put(key + "[" + j + "]", values.get(j));
            } else if (value instanceof Boolean) {
                params.put(key, (((Boolean) value) ? "Y" : "N"));
            } else {
                params.put(key, value);
            }
        }
    }

    @Override
    protected void parse(String json) {
        super.parse(json);

        if (isShowProgress) OH.c().notifyObservers(OH.TYPE.HIDE_PROGRESS);

        try {
            JSONObject jsonObject = new JSONObject(json);
            if (!jsonObject.isNull("resultCode")) {
                int resultCode = jsonObject.getInt("resultCode");
                if (resultCode == -1) {
                    String resultMsg = jsonObject.getString("resultMsg");
                    OH.TYPE oh = OH.TYPE.EXCEPTION;
                    oh.obj = resultMsg;
                    OH.c().notifyObservers(oh);
                    return;
                }
                if (resultCode == 309) {
                    OH.TYPE oh = OH.TYPE.SESSION_OUT;
                    OH.c().notifyObservers(oh);
                    return;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            Log.d("json -> " + json);
            showLogCat(json);

            // Log.l(Arrays.toString(getClass().getClasses()));
            final Class<?> cls = Class.forName(getClass().getName() + "$Data");
            final Field field = getClass().getField("data");
            field.set(this, gson.fromJson(json, cls));

            if (!NetConst.isReal()) {
                new Pojo(getClass(), json).gen().toLog();
            }
        } catch (Exception e) {
            Log.l(this, e.getMessage());
            new Pojo(getClass(), json).gen().toLog();
        }

        try {
            JSONObject jo = new JSONObject(json);
            err = jo.isNull("err") ? "N" : jo.getString("err");
            err_msg = jo.isNull("err_msg") ? "성공" : jo.getString("err_msg");
            success = !err.equals("Y");
            errorMessage = err_msg;

//            session_msg = jo.isNull("session_msg") ? "" : jo.getString("session_msg");

        } catch (JSONException e) {
            e.printStackTrace();
            error(e);
        }
        // Log.l(getClass().getSimpleName() + "<" + success + ">", errorMessage);
    }

    @Override
    protected void error(Exception error) {
        super.error(error);
        success = false;
    }

    private JsonDeserializer<Date> deserializer_date = new JsonDeserializer<Date>() {
        @Override
        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            if (json.isJsonNull() || json.getAsString().length() <= 0)
                return null;

            // Log.l(json.getAsString());
            try {
                return SDF.yyyymmddhhmmss_1.parseDate(json.getAsString());
            } catch (Exception e) {
                return null;
            }
        }
    };
    private JsonDeserializer<Boolean> deserializer_boolean = new JsonDeserializer<Boolean>() {
        public Boolean deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return !json.isJsonNull() && json.getAsString().equals("Y");
        }

    };
    private JsonDeserializer<ArrayList<String>> deserializer_stringarraylist = new JsonDeserializer<ArrayList<String>>() {
        public ArrayList<String> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            if (json.isJsonNull())
                return null;

            ArrayList<String> list = Lists.newArrayList();
            JsonArray ja = json.getAsJsonArray();
            for (JsonElement je : ja) {
                String spf_file = je.getAsJsonObject().get("spf_file").getAsString();
                // Log.l(spf_file);
                list.add(spf_file);
            }
            return list;
        }

    };

    public void setUrl(String url) {
        this.url = NetConst.host + url;

        // 중복 슬러시를 제거한다.
//        this.url = removeDoubleSlush(this.url);

        if (isShowProgress) OH.c().notifyObservers(OH.TYPE.SHOW_PROGRESS);
    }

    /**
     * 중복 슬러시를 제거한다.
     */
    private String removeDoubleSlush(String url) {
        String str2 = url.substring("http://".length());
        String str3 = str2.replace("//", "/");
        return "http://" + str3;
    }
}
